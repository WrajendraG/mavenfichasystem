package com.form.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.modal.ConfirmPasswordModel;

public class ConfirmPasswordFormValidation implements Validator{

	private Pattern pattern;
	private Matcher matcher;

	//private static final String PASSWORD_PATTERN = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}";
	private static final String PASSWORD_PATTERN = "(?=.{8,16})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=\\S)[A-Za-z0-9@#$%^&+=]+$";

	
	@Override
	public boolean supports(Class<?> clazz) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void validate(Object target, Errors errors) {
		// TODO Auto-generated method stub
		
		ConfirmPasswordModel confirmPasswordModel = (ConfirmPasswordModel) target;
		ValidationUtils.rejectIfEmpty(errors, "userName", "required.userName", "Not a valid activation request.");
		
		if(confirmPasswordModel.getUserName() != null && !confirmPasswordModel.getUserName().isEmpty()){
			ValidationUtils.rejectIfEmpty(errors, "verificationCode", "required.activationCode", "Not a valid Verification request.");
			if(confirmPasswordModel.getVerificationCode() != null && !confirmPasswordModel.getVerificationCode().isEmpty()){
				
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "resetPassword", "required.password", "Password is required.");
				
				// email validation in spring
				if (confirmPasswordModel.getResetPassword() != null && !confirmPasswordModel.getResetPassword().isEmpty()) {
					pattern = Pattern.compile(PASSWORD_PATTERN);
					matcher = pattern.matcher(confirmPasswordModel.getResetPassword());
					if (!matcher.matches()) {
						errors.rejectValue("resetPassword", "password.incorrect", "Invalid password pattern. Please provide password in the correct format.");
					}
				}
				
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "confirmResetPassword", "required.password", "Password is required.");
				
				if (confirmPasswordModel.getConfirmResetPassword() != null && !confirmPasswordModel.getConfirmResetPassword().isEmpty()) {
					if(!(confirmPasswordModel.getConfirmResetPassword().equals(confirmPasswordModel.getResetPassword())))
					{
						errors.rejectValue("confirmResetPassword", "password.incorrect", "Enter a correct Confirmation Password which should match with Password Field.");
					}
				}

			}
		}

	}
}

