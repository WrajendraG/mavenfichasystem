package com.form.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.modal.BusinessModal;
import com.modal.CasinoManager;

public class CasinoFormValidation implements Validator{
	
	private Pattern pattern;
	private Matcher matcher;

	private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	//private static final String PASSWORD_PATTERN = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}";
	private static final String PASSWORD_PATTERN = "(?=.{8,16})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=\\S)[A-Za-z0-9@#$%^&+=]+$";
	String ID_PATTERN = "[0-9]+";
	String STRING_PATTERN = "[a-zA-Z]+";
	String MOBILE_PATTERN = "[0-9]{6,12}";
	
	
	

	@Override
	public boolean supports(Class<?> clazz) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void validate(Object target, Errors errors) {
		// TODO Auto-generated method stub
		
		CasinoManager cm=(CasinoManager)target;
		
		ValidationUtils.rejectIfEmpty(errors, "firstname", "required.firstname", "First Name is required.");
		ValidationUtils.rejectIfEmpty(errors, "lastname", "required.lastname", "Last Name is required.");
		ValidationUtils.rejectIfEmpty(errors, "username", "required.lastname", "User Name is required.");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "required.email", "Email is required.");
		
		if (!(cm.getEmail() != null && cm.getEmail().isEmpty())) {
			pattern = Pattern.compile(EMAIL_PATTERN);
			matcher = pattern.matcher(cm.getEmail());
			if (!matcher.matches()) {
				errors.rejectValue("email", "email.incorrect", "Enter a correct email.");
			}
		}
		
		//ValidationUtils.rejectIfEmptyOrWhitespace(errors, "phone", "required.phone", "Phone is required.");
		if(cm.isUpdate()){
			// email validation in spring
			if (!(cm.getPassword() != null && cm.getPassword().isEmpty())) {
				pattern = Pattern.compile(PASSWORD_PATTERN);
				matcher = pattern.matcher(cm.getPassword());
				if (!matcher.matches()) {
					errors.rejectValue("password", "password.incorrect", "Invalid password pattern. Please provide password in the correct format.");
				}
				
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "conpassword", "required.password", "Password is required.");
			}
			
			if (!(cm.getConpassword() != null && cm.getConpassword().isEmpty())) {
				if(!(cm.getConpassword().equals(cm.getPassword())))
				{
					errors.rejectValue("conpassword", "password.incorrect", "Enter a correct Confirmation Password which should match with Password Field.");
				}
			}
		}else{
	        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "required.password", "Password is required.");
			
			// email validation in spring
			if (!(cm.getPassword() != null && cm.getPassword().isEmpty())) {
				pattern = Pattern.compile(PASSWORD_PATTERN);
				matcher = pattern.matcher(cm.getPassword());
				if (!matcher.matches()) {
					errors.rejectValue("password", "password.incorrect", "Invalid password pattern. Please provide password in the correct format.");
				}
			}
			
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "conpassword", "required.password", "Password is required.");
			
			
			if(!(cm.getPassword().equals(cm.password)))
			{
				errors.rejectValue("password", "password.incorrect", "Enter a correct Confirmation Password.");
			}
		}
		
		
		
		ValidationUtils.rejectIfEmpty(errors, "address", "required.address1", "Address is required.");
		ValidationUtils.rejectIfEmpty(errors, "postcode", "required.postcode", "Post code is required.");
		ValidationUtils.rejectIfEmpty(errors, "country", "required.country", "Country is required.");
		ValidationUtils.rejectIfEmpty(errors, "city", "required.city", "City is required.");
		
		//
	}
}
