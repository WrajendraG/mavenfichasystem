package com.form.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.modal.Currency;

public class CurrencyFormValidators implements Validator{

	@Override
	public boolean supports(Class<?> clazz) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void validate(Object target, Errors errors) {
		// TODO Auto-generated method stub
		Currency c=(Currency)target;
		
		ValidationUtils.rejectIfEmpty(errors, "name", "required.name", "Currency Name is required.");
	}

}
