package com.form.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.modal.PaybackModel;

public class PayBackFormValidation implements Validator{
	
	private Pattern pattern;
	private Matcher matcher;

	private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	//private static final String PASSWORD_PATTERN = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}";
	private static final String PASSWORD_PATTERN = "(?=.{8,16})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=\\S)[A-Za-z0-9@#$%^&+=]+$";
	String ID_PATTERN = "[0-9]+";
	String STRING_PATTERN = "[a-zA-Z]+";
	String MOBILE_PATTERN = "[0-9]{6,12}";

	@Override
	public boolean supports(Class<?> clazz) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void validate(Object target, Errors errors) {
		// TODO Auto-generated method stub
		System.out.println("inside validation form");
		PaybackModel paybackmodel=(PaybackModel)target;
		
		ValidationUtils.rejectIfEmpty(errors, "businessPartnerId", "required.business.partner", "Business Partner is required.");
		ValidationUtils.rejectIfEmpty(errors, "payBack", "required.pay.back", "Credit Payback is required.");
		ValidationUtils.rejectIfEmpty(errors, "currencyId", "required.currency", "Currency is required.");
		ValidationUtils.rejectIfEmpty(errors, "paymentId", "required.paymentId", "Payment type is required.");
		ValidationUtils.rejectIfEmpty(errors, "amountStr", "required.amount", "Amount is required.");
		
		
		if (paybackmodel.getAmountStr() != null && !paybackmodel.getAmountStr().isEmpty()) {
			Double amount;
			try {
				amount = Double.parseDouble(paybackmodel.getAmountStr());
				
/*				if (((amount == 0.0) || (amount == (-1d)))){
					errors.rejectValue("amountStr", "amountStr.incorrect", "Amount should be greater than 0");
				}*/
				
				if (amount == 0.0 || amount < 0.0){
					errors.rejectValue("amountStr", "amountStr.incorrect", "Amount should be greater than 0");
				}
			}
			catch (NumberFormatException ex) {
				errors.rejectValue("amountStr", "amountStr.incorrect", "Amount should be a numeric value");
			}
		// input string can not exceed that a limit
			if (paybackmodel.getAmountStr().toString().length() > 12) {
				errors.rejectValue("amountStr", "amountStr.exceed", "Amount should not contain more than 12 digits");
			}
		}
	}

}
