package com.util;

import java.util.HashMap;
import java.util.Map;

public class Constants {
	public static final Boolean isProduction = false;
	
	public static final Boolean runningForLocalHost = true;
	
	// betmais client
	public static final Boolean isClientBETMAIS = true;
	
	/** The Constant BETMAIS_SKIN. */
	public static final String BETMAIS_SKIN = "betmais";
	
	public static final Integer FICHA_REGISTRATION_SELF = 0;
	public static final Integer FICHA_REGISTRATION_CASINO_MANAGER = 1;
	
	public static final Integer DEPOSIT_AMOUNT_APPROVAL = 0;
	public static final Integer CREDIT_AMOUNT_APPROVAL = 1;
	
	public static final Integer STATUS_PENDING = 0;
	public static final Integer STATUS_APPROVED = 1;
	public static final Integer STATUS_REJECTED = 2;
	
	public static final Integer MAIN_AGENT = 0;
	public static final Integer SUB_AGENT = 1;
	
	public static final Integer CREDIT_PAYBACK = 2;
	
	public static final String MANDRILL_API_KEY = "RUZSzH-n5ByJSEnXCqHTZA";
	public static final String TEMPLATE_WELCOME_EMAIL = "01_casino_manager_ficha_welcomeEmail";
	public static final String TEMPLATE_ACTIVATION_EMAIL = "01_business_partner_ficha_accountActivation";
	public static final String TEMPLATE_FORGOT_EMAIL = "01_business_partner_ficha_forgotPassword";
	public static final String IP_ADDRESS_SERVER;
	public static final String IP_ADDRESS_SERVER_CASINO_MANAGER;
	
	static {
		
		if(runningForLocalHost){
			IP_ADDRESS_SERVER = "http://localhost:8080/BusinessPartner";
			IP_ADDRESS_SERVER_CASINO_MANAGER = "http://localhost:8080/CasinoManager";
		}else{
			if (isProduction) {
				IP_ADDRESS_SERVER = "https://bp.netfichas.com";
				IP_ADDRESS_SERVER_CASINO_MANAGER = "https://cm.netfichas.com";
			} else {
				IP_ADDRESS_SERVER = "https://staging-bp.netfichas.com";
				IP_ADDRESS_SERVER_CASINO_MANAGER = "https://staging-cm.netfichas.com";
			}
		}
	}
	
	public static class EmailTemplates {

		// Replacement Strings for the player used in the templates.
		// First Name of the player
		/*
		 * private static String FIRST_NAME_CONDITIONAL; private static String
		 * FIRST_NAME_1_CONDITIONAL; private static String
		 * LAST_NAME_CONDITIONAL;
		 */
		/*
		 * private static String ACTIVATION_LINK_CONDITIONAL; private static
		 * String PASSWORD_RESET_LINK_CONDITIONAL;
		 *//*
			 * private static String ENGLISH_CONDITIONAL; private static String
			 * GERMAN_CONDITIONAL;
			 */
		/** The setting map local. */
		/*
		 * private static String DATE_CONDITIONAL; private static String
		 * AMOUNT_CONDITIONAL; private static String
		 * PAYMENT_PROVIDER_CONDITIONAL;
		 */
		private static Map<String, Map<String, String>> settingMapLocal = new HashMap<String, Map<String, String>>();

		/** The skin setting map local. */
		private static Map<String, String> skinSettingMapLocal = new HashMap<String, String>();

		/** The Constant FIRST_NAME. */
		public static final String FIRST_NAME = "FIRSTNAME";
		
		/** The Constant USER_NAME. */
		public static final String USER_NAME = "USERNAME";

		/** The Constant FIRST_NAME_1. */
		public static final String FIRST_NAME_1 = "FIRST_NAME";

		/** The Constant LAST_NAME. */
		public static final String LAST_NAME = "LASTNAME";

		/** The Constant ACTIVATION_LINK. */
		public static final String ACTIVATION_LINK = "link";
		
		/** The Constant USER_PASSWORD. */
		public static final String USER_PASSWORD = "PASSWORD";

		/** The Constant PASSWORD_RESET_LINK. */
		public static final String PASSWORD_RESET_LINK = "PASSWORD_RESET_LINK";

		/** The Constant ENGLISH. */
		public static final String ENGLISH = ".en";
		
		/** The Constant PORTUGUESE. */
		public static final String PORTUGUESE = ".pt";

		/** The Constant SWEDISH. */
		public static final String SWEDISH = ".sv";

		/** The Constant NORWEGIAN. */
		public static final String NORWEGIAN = ".no";

		/** The Constant GERMAN. */
		public static final String GERMAN = ".de";

		/** The Constant DATE. */
		public static final String DATE = "DATE";

		/** The Constant AMOUNT. */
		public static final String AMOUNT = "AMOUNT";

		/** The Constant PAYMENT_PROVIDER. */
		public static final String PAYMENT_PROVIDER = "PAYMENTPROVIDER";

		/** The Constant LINK_ACTIVATION. */
		// Compost triggers
		public static final String LINK_ACTIVATION = "23171";

		/** The Constant LINK_AMOUNT. */
		public static final String LINK_AMOUNT = "23175";

		/** The Constant LINK_BET. */
		public static final String LINK_BET = "23177";

		/** The Constant LINK_DEPOSIT. */
		public static final String LINK_DEPOSIT = "23179";

		/** The Constant LINK_TIME. */
		public static final String LINK_TIME = "23180";

		/** The Constant LINK_LOSS. */
		public static final String LINK_LOSS = "23178";

		/** The Constant LINK_DATE. */
		public static final String LINK_DATE = "23181";

		/** The Constant LINK_EMAIL_MESSAGE_BODY. */
		public static final String LINK_EMAIL_MESSAGE_BODY = "23277";

		/** The Constant LINK_EMAIL_SUBJECT. */
		public static final String LINK_EMAIL_SUBJECT = "23278";

		/** The Constant LINK_EMAIL_ID. */
		public static final String LINK_EMAIL_ID = "23276";

		/** The Constant LINK_PAYMENT_PROVIDER. */
		public static final String LINK_PAYMENT_PROVIDER = "23182";

		/** The Constant TEMPLATE_ACC_NOT_VALIDATED. */
		public static final String TEMPLATE_ACC_NOT_VALIDATED = "TEMPLATE_ACC_NOT_VALIDATED";

		/** The Constant TEMPLATE_ACC_NOT_VALIDATED_DUP. */
		public static final String TEMPLATE_ACC_NOT_VALIDATED_DUP = "TEMPLATE_ACC_NOT_VALIDATED_DUP";

		/** The Constant TEMPLATE_ACC_ACTIVATION. */
		public static final String TEMPLATE_ACC_ACTIVATION = "TEMPLATE_ACC_ACTIVATION";

		/** The Constant TEMPLATE_ACCOUNT_VALIDATION. */
		public static final String TEMPLATE_ACCOUNT_VALIDATION = "TEMPLATE_ACCOUNT_VALIDATION";

		/** The Constant TEMPLATE_ACCOUNT_VALIDATION_DUPE. */
		public static final String TEMPLATE_ACCOUNT_VALIDATION_DUPE = "TEMPLATE_ACCOUNT_VALIDATION_DUPE";

		/** The Constant TEMPLATE_DEPOSIT_CONFIRMATION. */
		public static final String TEMPLATE_DEPOSIT_CONFIRMATION = "TEMPLATE_DEPOSIT_CONFIRMATION";

		/** The Constant TEMPLATE_FORGOT_PASSWORD. */
		public static final String TEMPLATE_FORGOT_PASSWORD = "TEMPLATE_FORGOT_PASSWORD";

		/** The Constant TEMPLATE_PROFILE_UPDATE. */
		public static final String TEMPLATE_PROFILE_UPDATE = "TEMPLATE_PROFILE_UPDATE";

		/** The Constant TEMPLATE_SET_LIMIT. */
		public static final String TEMPLATE_SET_LIMIT = "TEMPLATE_SET_LIMIT";

		/** The Constant TEMPLATE_PWD_SUCCESS_CHANGED. */
		public static final String TEMPLATE_PWD_SUCCESS_CHANGED = "TEMPLATE_PWD_SUCCESS_CHANGED";

		/** The Constant TEMPLATE_WELCOME_EMAIL. */
		public static final String TEMPLATE_WELCOME_EMAIL = "TEMPLATE_WELCOME_EMAIL";

		/** The Constant TEMPLATE_WITHDRAWAL_CONFIRMATION. */
		public static final String TEMPLATE_WITHDRAWAL_CONFIRMATION = "TEMPLATE_WITHDRAWAL_CONFIRMATION";

		/** The Constant TEMPLATE_WITHDRAWAL_PROCESSED. */
		public static final String TEMPLATE_WITHDRAWAL_PROCESSED = "TEMPLATE_WITHDRAWAL_PROCESSED";

		/** The Constant TEMPLATE_RISKY_COUNTRY. */
		public static final String TEMPLATE_RISKY_COUNTRY = "TEMPLATE_RISKY_COUNTRY";

		/** The Constant TEMPLATE_REFER_FRIEND. */
		public static final String TEMPLATE_REFER_FRIEND = "TEMPLATE_REFER_FRIEND";

		/** The Constant TEMPLATE_VERIFICATION_SMS. */
		public static final String TEMPLATE_VERIFICATION_SMS = "TEMPLATE_VERIFICATION_SMS";

		/** The Constant TEMPLATE_RESEND_VERIFICATION. */
		public static final String TEMPLATE_RESEND_VERIFICATION = "TEMPLATE_RESEND_VERIFICATION";

		/** The Constant TEMPLATE_CONTACT_US. */
		public static final String TEMPLATE_CONTACT_US = "TEMPLATE_CONTACT_US";

		public static final String LOSS_VALUE = "LOSS_VALUE";

		
		/** The mobile number. */
		// Add the SMS Constants for compost here
		public static String MOBILE_NUMBER = "mobileNumber";

		/** The verification code. */
		public static String VERIFICATION_CODE = "verificationCode";


		// Replacement Strings for the player used in the templates.
		/** The setting map. */
		// First Name of the player
		public static Map<String, Map<String, String>> settingMap = settingMapLocal;

		/** The skin setting map. */
		public static Map<String, String> skinSettingMap = skinSettingMapLocal;

	}

	
}


