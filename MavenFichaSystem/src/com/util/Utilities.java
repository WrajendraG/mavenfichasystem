package com.util;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import com.modal.Modelbean;

public class Utilities {

	public static boolean isUserLoggedIn(HttpServletRequest req, Model model, BindingResult result){
		HttpSession httpSession=req.getSession(false);
		if (httpSession == null) {
			model.addAttribute("go", new Modelbean());
			result.rejectValue("", "error.sessionInvalidate", "You must be logged in to access this page.");
			return false;
		}else{
			Modelbean casinoUser = (Modelbean) httpSession.getAttribute("casinoUser");
			if(casinoUser == null){
				model.addAttribute("go", new Modelbean());
				System.out.println("Username and password is Not there");
				return false;
			} else{
				return true;
			}
		}
	}
	
	public static List<Integer> convertCommaSeperatedStringToListInteger(String commaSeperatedStr) {
		String strArray[] = commaSeperatedStr.split(",");
		List<Integer> items = new ArrayList<Integer>();
		for (String splitStr : strArray) {
			items.add(Integer.parseInt(splitStr));
		}
		return items;
	}
}
