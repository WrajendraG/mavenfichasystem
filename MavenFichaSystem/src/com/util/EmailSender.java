/*
 * Copyright @2015 Alteatec Pvt Ltd.
 */
package com.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.microtripit.mandrillapp.lutung.MandrillApi;
import com.microtripit.mandrillapp.lutung.model.MandrillApiError;
import com.microtripit.mandrillapp.lutung.view.MandrillMessage;
import com.microtripit.mandrillapp.lutung.view.MandrillMessage.MergeVarBucket;
import com.microtripit.mandrillapp.lutung.view.MandrillMessage.Recipient;
import com.microtripit.mandrillapp.lutung.view.MandrillMessageStatus;
import com.modal.User;

// TODO: Auto-generated Javadoc
/**
 * Email Sender is a common base class for sending all emails using Mandrill
 * templates. This class has one method per template which calls a common method
 * sendTemplatedEmail.
 * <p>
 * sendTemplatedMethod resolves the template to be used from the parameters
 * passed and prepares the message accordingly.
 * 
 */

public class EmailSender {

	/** The Constant log. */
	private static final Logger log = LoggerFactory
			.getLogger(EmailSender.class);

	/** The mandrill api. */
	private final MandrillApi mandrillApi;

	/**
	 * Instantiates a new email sender.
	 *
	 * @param mandrillApi
	 *            the mandrill api
	 */

	public EmailSender() {
		
		this.mandrillApi = new MandrillApi(Constants.MANDRILL_API_KEY);
	}

	/**
	 * This is a method specific to the mail for sending the activation link to
	 * the player upon successful registration.
	 * 
	 * @param user
	 *            the User object containing all the information about the
	 *            player.
	 * @param serverPath
	 *            the url of the skin on which the player is currently
	 *            registering from.
	 * @param map
	 *            Map containing extra information about the player.
	 * @see User
	 * @see ITemplate
	 * @see Template
	 */
	public void sendEmailWithLink(User user, String serverPath,
			HashMap<String, Object> map) {
		// TODO: Add multi language support -- Add Conditions per language and
		// Add templates accordingly.
		System.out.println("servrepath" + serverPath);
		EmailSender sender = new EmailSender();
		ITemplate template = new Template("" + Constants.EmailTemplates.PORTUGUESE, user);
		try {
			sender.sendTemplatedEmail(template, user, serverPath, map);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * This is a method specific to the mail that notifies the player that his
	 * registration has failed because he is either coming from a country that
	 * is either prohibited or his registration ip is black listed.
	 * 
	 * @param user
	 *            the User object containing all the information about the
	 *            player.
	 * @param serverPath
	 *            the url of the skin on which the player is currently
	 *            registering from.
	 * @param map
	 *            Map containing extra information about the player.
	 * @see User
	 * @see ITemplate
	 * @see Template
	 */
/*	public void sendEmailWithoutLink(User user, String serverPath,
			HashMap<String, Object> map) {

		// TODO: Add multi language support -- Add Conditions per language and
		// Add templates accordingly.
		EmailSender sender = new EmailSender(mandrillApi);
		ITemplate template = new Template(Constants.EmailTemplates.settingMap
				.get(Utilities.getSkinNameFromServerPath(serverPath)).get(
						"TEMPLATE_RISKY_COUNTRY") 
												 * Constants.EmailTemplates.
												 * TEMPLATE_RISKY_COUNTRY
												 
				+ Constants.EmailTemplates.ENGLISH, user);
		try {
			sender.sendTemplatedEmail(template, user, serverPath, map);
		} catch (MandrillApiError | Exception e) {
			e.printStackTrace();
		}
	}*/

	/**
	 * This is a method specific to the mail that sends a welcome note to the
	 * player when he activates his account and logs in for the first time.
	 * 
	 * @param user
	 *            the User object containing al the information about the
	 *            player.
	 * @param serverPath
	 *            the url of the skin on which the player is currently
	 *            registering from.
	 * @param map
	 *            Map containing extra information about the player.
	 * @see User
	 * @see ITemplate
	 * @see Template
	 */
	public void welcomeEmail(User user, String serverPath,
			HashMap<String, Object> map) {
		// TODO: Add multi language support -- Add Conditions per language and
		// Add templates accordingly.
		EmailSender sender = new EmailSender();
		ITemplate template = new Template(Constants.TEMPLATE_WELCOME_EMAIL + Constants.EmailTemplates.PORTUGUESE, user);
		try {
			sender.sendTemplatedEmail(template, user, serverPath, map);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void verificationEmail(User user, String serverPath,
			HashMap<String, Object> map) {
		// TODO Auto-generated method stub
		EmailSender sender = new EmailSender();
		ITemplate template = new Template(Constants.TEMPLATE_ACTIVATION_EMAIL + Constants.EmailTemplates.PORTUGUESE, user);
		try {
			sender.sendTemplatedEmail(template, user, serverPath, map);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	/**
	 * This is a method specific to the mail that sends a welcome note to the
	 * player when he activates his account and logs in for the first time.
	 * 
	 * @param user
	 *            the User object containing al the information about the
	 *            player.
	 * @param serverPath
	 *            the url of the skin on which the player is currently
	 *            registering from.
	 * @param map
	 *            Map containing extra information about the player.
	 * @see User
	 * @see ITemplate
	 * @see Template
	 */
	public void activationEmail(User user, String serverPath,
			HashMap<String, Object> map) {
		// TODO: Add multi language support -- Add Conditions per language and
		// Add templates accordingly.
		EmailSender sender = new EmailSender();
		ITemplate template = new Template(Constants.TEMPLATE_ACTIVATION_EMAIL + Constants.EmailTemplates.PORTUGUESE, user);
		try {
			sender.sendTemplatedEmail(template, user, serverPath, map);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	

	public void forgotPasswordEmail(User user, String serverPath,
			HashMap<String, Object> map) {
		// TODO: Add multi language support -- Add Conditions per language and
		// Add templates accordingly.
		EmailSender sender = new EmailSender();
		ITemplate template = new Template(Constants.TEMPLATE_FORGOT_EMAIL + Constants.EmailTemplates.PORTUGUESE, user);
		try {
			sender.sendTemplatedEmail(template, user, serverPath, map);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * This is a method sends an email to the player notifying him that his
	 * password has been successfully changed and updated.
	 * 
	 * @param user
	 *            the User object containing all the information about the
	 *            player.
	 * @param serverPath
	 *            the url of the skin on which the player is currently
	 *            registering from.
	 * @param map
	 *            Map containing extra information about the player.
	 * @see User
	 * @see ITemplate
	 * @see Template
	 */
/*	public void passwordUpdateEmail(User user, String serverPath,
			HashMap<String, Object> map) {
		// TODO: Add multi language support -- Add Conditions per language and
		// Add templates accordingly.
		EmailSender sender = new EmailSender(mandrillApi);
		ITemplate template = new Template(Constants.EmailTemplates.settingMap
				.get(Utilities.getSkinNameFromServerPath(serverPath)).get(
						"TEMPLATE_PWD_SUCCESS_CHANGED") 
														 * Constants.EmailTemplates
														 * .
														 * TEMPLATE_PWD_SUCCESS_CHANGED
														 
				+ Constants.EmailTemplates.ENGLISH, user);
		try {
			sender.sendTemplatedEmail(template, user, serverPath, map);
		} catch (MandrillApiError | Exception e) {
			e.printStackTrace();
		}
	}*/

	/**
	 * This is a method that sends an email with a link that redirects the
	 * player to a page where he can reset his password. This page asks the
	 * player to set in a new password.
	 * 
	 * @param user
	 *            the User object containing al the information about the
	 *            player.
	 * @param serverPath
	 *            the url of the skin on which the player is currently
	 *            registering from.
	 * @param map
	 *            Map containing extra information about the player.
	 * @see User
	 */
/*	public void forgotPasswordActivationLink(User user, String serverPath,
			HashMap<String, Object> map) {
		// TODO: Add multi language support -- Add Conditions per language and
		// Add templates accordingly.
		EmailSender sender = new EmailSender(mandrillApi);
		ITemplate template = new Template(Constants.EmailTemplates.settingMap
				.get(Utilities.getSkinNameFromServerPath(serverPath)).get(
						"TEMPLATE_FORGOT_PASSWORD") 
													 * Constants.EmailTemplates.
													 * TEMPLATE_FORGOT_PASSWORD
													 
				+ Constants.EmailTemplates.ENGLISH, user);
		try {
			sender.sendTemplatedEmail(template, user, serverPath, map);
		} catch (MandrillApiError | Exception e) {
			e.printStackTrace();
		}
	}*/

	/**
	 * This is a method that sends an email to player upon successful process of
	 * his withdrawal request.
	 *
	 * @param user
	 *            the User object containing al the information about the
	 *            player.
	 * @param serverPath
	 *            the url of the skin on which the player is currently
	 *            registering from.
	 * @param map
	 *            Map containing extra information about the player.
	 * @see User
	 */
/*	public void withdrawProcess(User user, String serverPath,
			HashMap<String, Object> map) {
		// TODO: Add multi language support -- Add Conditions per language and
		// Add templates accordingly.
		EmailSender sender = new EmailSender(mandrillApi);
		ITemplate template = new Template(Constants.EmailTemplates.settingMap
				.get(Utilities.getSkinNameFromServerPath(serverPath)).get(
						"TEMPLATE_WITHDRAWAL_PROCESSED")
														 * Constants.EmailTemplates
														 * .
														 * TEMPLATE_WITHDRAWAL_PROCESSED
														 
				+ Constants.EmailTemplates.ENGLISH, user);
		try {
			sender.sendTemplatedEmail(template, user, serverPath, map);
		} catch (MandrillApiError | Exception e) {
			e.printStackTrace();
		}
	}*/

	/**
	 * This is a method that sends an email to player upon successful referral.
	 * 
	 * @param user
	 *            the User object containing all the information about the
	 *            player.
	 * @param serverPath
	 *            the url of the skin on which the player is currently
	 *            registering from.
	 * @param map
	 *            Map containing extra information about the player.
	 * @see User
	 */
/*	public void referFriendEmail(User user, String serverPath,
			HashMap<String, Object> map) {
		// TODO: Add multi language support -- Add Conditions per language and
		// Add templates accordingly.
		EmailSender sender = new EmailSender(mandrillApi);
		ITemplate template = new Template(Constants.EmailTemplates.settingMap
				.get(Utilities.getSkinNameFromServerPath(serverPath)).get(
						"TEMPLATE_REFER_FRIEND")
												 * Constants.EmailTemplates.
												 * TEMPLATE_REFER_FRIEND
												 
				+ Constants.EmailTemplates.ENGLISH, user);
		try {
			sender.sendTemplatedEmail(template, user, serverPath, map);
		} catch (MandrillApiError | Exception e) {
			e.printStackTrace();
		}
	}*/

	
/*	public void playerLossLimit(User user, String serverPath,
			HashMap<String, Object> map) {
		// TODO: Add multi language support -- Add Conditions per language and
		// Add templates accordingly.
		EmailSender sender = new EmailSender(mandrillApi);
		ITemplate template = new Template(Constants.EmailTemplates.settingMap
				.get(Utilities.getSkinNameFromServerPath(serverPath)).get(
						"TEMPLATE_LOSSES_LIMIT_INCREASE_CONFIRMATION")
				+ Constants.EmailTemplates.ENGLISH, user);
		try {
			sender.sendTemplatedEmail(template, user, serverPath, map);
		} catch (MandrillApiError | Exception e) {
			e.printStackTrace();
		}
	}*/
	/**
	 * This is a method that sends an email to player upon successful update of
	 * profile.
	 * 
	 * @param user
	 *            the User object containing all the information about the
	 *            player.
	 * @param serverPath
	 *            the url of the skin on which the player is currently
	 *            registering from.
	 * @param map
	 *            Map containing extra information about the player.
	 * @see User
	 */
/*	public void profileUpdateEmail(User user, String serverPath,
			HashMap<String, Object> map) {
		// TODO: Add multi language support -- Add Conditions per language and
		// Add templates accordingly.
		EmailSender sender = new EmailSender(mandrillApi);
		ITemplate template = new Template(Constants.EmailTemplates.settingMap
				.get(Utilities.getSkinNameFromServerPath(serverPath)).get(
						"TEMPLATE_PROFILE_UPDATE")
												 * Constants.EmailTemplates.
												 * TEMPLATE_PROFILE_UPDATE
												 
				+ Constants.EmailTemplates.ENGLISH, user);
		try {
			sender.sendTemplatedEmail(template, user, serverPath, map);
		} catch (MandrillApiError | Exception e) {
			e.printStackTrace();
		}
	}*/

	/**
	 * This is a method that sends an email to player upon successful deposit.
	 * 
	 * @param user
	 *            the User object containing all the information about the
	 *            player.
	 * @param serverPath
	 *            the url of the skin on which the player is currently
	 *            registering from.
	 * @param map
	 *            Map containing extra information about the player.
	 * @see User
	 */
	/*public void depositConfirmation(User user, String serverPath,
			HashMap<String, Object> map) {
		// TODO: Add multi language support -- Add Conditions per language and
		// Add templates accordingly.
		EmailSender sender = new EmailSender(mandrillApi);
		ITemplate template = new Template(Constants.EmailTemplates.settingMap
				.get(Utilities.getSkinNameFromServerPath(serverPath)).get(
						"TEMPLATE_DEPOSIT_CONFIRMATION")
														 * Constants.EmailTemplates
														 * .
														 * TEMPLATE_DEPOSIT_CONFIRMATION
														 
				+ Constants.EmailTemplates.ENGLISH, user);
		try {
			sender.sendTemplatedEmail(template, user, serverPath, map);
		} catch (MandrillApiError | Exception e) {
			e.printStackTrace();
		}
	}
*/
	/**
	 * This is a common method that uses the lutung.jar and the madrill template
	 * after it has been computed (prepareMessage).
	 *
	 * @param template
	 *            the template object Template class that registering from.
	 * @param user
	 *            the User object containing all the information about the
	 *            player.
	 * @param serverUrl
	 *            the url of the skin on which the player is currently
	 *            registering from.
	 * @param map
	 *            Map containing extra information about the player.
	 * @throws MandrillApiError
	 *             the mandrill api error
	 * @throws Exception
	 *             the exception
	 * @see User
	 * @see ITemplate
	 * @see Template
	 * @see {@link MandrillMessage}
	 * @see {@link MandrillMessageStatus}
	 */
	public void sendTemplatedEmail(ITemplate template, User user,
			String serverUrl, HashMap<String, Object> map)
			throws MandrillApiError, Exception {

		MandrillMessage msg = prepareMessage(user, template, serverUrl, map);
		Map<String, String> templateContent = Maps.newHashMap();

		try {

			MandrillMessageStatus[] status = mandrillApi.messages()
					.sendTemplate(template.getTemplateName(), templateContent,
							msg, false);

			System.out.println(status[0].getStatus());
			System.out.println(status[0].getRejectReason());
		} catch (MandrillApiError e) {
			e.printStackTrace();
			System.out.println(e.getMandrillErrorAsJson());
		}
	}

	/**
	 * This is a method is a common method that prepares the Mandrill message
	 * that using the template created on www.mandrill.com on your account.
	 * <p>
	 * This is a case based(conditional) method that creates the message
	 * according to the template sent. This method merges the variables that
	 * need to be dynamically replaced in the template using setMergeVar.
	 *
	 * @param user
	 *            the User object containing all the information about the
	 *            player.
	 * @param template
	 *            the template object Template class that registering from.
	 * @param serverPath
	 *            the url of the skin on which the player is currently
	 *            registering from.
	 * @param map
	 *            Map containing extra information about the player.
	 * @return {@link MandrillMessage}
	 * @throws Exception
	 *             the exception
	 * @see User
	 * @see {@link MergeVarBucket}
	 * @see Recipient
	 */
	public MandrillMessage prepareMessage(User user, ITemplate template,
			String serverPath, HashMap<String, Object> map) throws Exception {
		MandrillMessage msg = new MandrillMessage();
		List<Recipient> recipients = Lists.newLinkedList();
		List<MergeVarBucket> mergeVars = Lists.newLinkedList();
		Recipient rec = getRecipient(user);
		recipients.add(rec);
		template.setUser(user);
		// Construct the Message
		// Prepare the message As per the template ~~~ Replace the values
		// per template. Add more conditions as and when templates are added.

/*		if (template.getTemplateName().contains(
				Constants.EmailTemplates.settingMap.get(
						Utilities.getSkinNameFromServerPath(serverPath)).get(
						"TEMPLATE_ACC_ACTIVATION"))) {
			String activationUrl = serverPath;
			if (Constants.isClientBet90 || Constants.isClientPopulotto
					|| Constants.isClientRIOTA || Constants.isClientBETMAIS || Constants.isClientBahis90) {
				activationUrl += "/activation/" + user.getActivationCode();
				activationUrl += "/" + user.getEmail();

			}
			System.out.println("activation URl" + activationUrl);
			template.setMergeVar(Constants.EmailTemplates.FIRST_NAME,
					user.getName());
			template.setMergeVar(Constants.EmailTemplates.ACTIVATION_LINK,
					activationUrl);
		} 
		 * else if (template.getTemplateName().contains(
		 * Constants.EmailTemplates.TEMPLATE_ACC_NOT_VALIDATED)) { } else if
		 * (template.getTemplateName().contains(
		 * Constants.EmailTemplates.TEMPLATE_ACC_NOT_VALIDATED_DUP)) { } else if
		 * (template.getTemplateName().contains(
		 * Constants.EmailTemplates.TEMPLATE_ACCOUNT_VALIDATION)) { } else if
		 * (template.getTemplateName().contains(
		 * Constants.EmailTemplates.TEMPLATE_ACCOUNT_VALIDATION_DUPE)) { }
		 else if (template.getTemplateName().contains(
				Constants.EmailTemplates.settingMap.get(
						Utilities.getSkinNameFromServerPath(serverPath)).get(
						"TEMPLATE_DEPOSIT_CONFIRMATION"))) {
			template.setMergeVar(Constants.EmailTemplates.FIRST_NAME,
					user.getName());
			template.setMergeVar(Constants.EmailTemplates.AMOUNT,
					map.get("amount").toString());
		} else if (template.getTemplateName().contains(
				Constants.EmailTemplates.settingMap.get(
						Utilities.getSkinNameFromServerPath(serverPath)).get(
						"TEMPLATE_FORGOT_PASSWORD"))) {
			String resetPwdUrl = "";
			resetPwdUrl = user.getForgotPasswordActivationCode();
			if (Constants.isClientAmatic || Constants.isClientBet90
					|| Constants.isClientPopulotto || Constants.isClientDemo
					|| Constants.isClientGRANDX || Constants.isClientRIOTA
					|| Constants.isClientBETMAIS || Constants.isClientBahis90) {
				resetPwdUrl = serverPath + "/resetPassword/"
						+ user.getForgotPasswordActivationCode();
			} else {
				resetPwdUrl = user.getForgotPasswordActivationCode();
			}
			template.setMergeVar(Constants.EmailTemplates.FIRST_NAME_1,
					user.getName());
			template.setMergeVar(Constants.EmailTemplates.PASSWORD_RESET_LINK,
					resetPwdUrl);
		} else if (template.getTemplateName().contains(
				Constants.EmailTemplates.settingMap.get(
						Utilities.getSkinNameFromServerPath(serverPath)).get(
						"TEMPLATE_PROFILE_UPDATE"))) {
			template.setMergeVar(Constants.EmailTemplates.FIRST_NAME,
					user.getName());
		} else if (template.getTemplateName().contains(
				Constants.EmailTemplates.settingMap.get(
						Utilities.getSkinNameFromServerPath(serverPath)).get(
						"TEMPLATE_PWD_SUCCESS_CHANGED"))) {
			template.setMergeVar(Constants.EmailTemplates.FIRST_NAME,
					user.getName());
		}
		 * else if (template.getTemplateName().contains(
		 * Constants.EmailTemplates.TEMPLATE_SET_LIMIT)) { }
		 else*/
		  
		  if (template.getTemplateName().contains(Constants.TEMPLATE_ACTIVATION_EMAIL)) {
			 
			 String activationUrl = Constants.IP_ADDRESS_SERVER;
				if (Constants.isClientBETMAIS) {
					activationUrl += "/activateBusinessPartner.html?userName=" +user.getUsername() + "&activationCode=" + user.getActivationCode();
				}
				System.out.println("activation URl" + activationUrl);

			 
			template.setMergeVar(Constants.EmailTemplates.USER_NAME, user.getUsername());
			template.setMergeVar( Constants.EmailTemplates.ACTIVATION_LINK, activationUrl);
		
		  }else if(template.getTemplateName().contains(Constants.TEMPLATE_WELCOME_EMAIL)){ 
			template.setMergeVar(Constants.EmailTemplates.USER_NAME, user.getUsername());
			template.setMergeVar( Constants.EmailTemplates.USER_PASSWORD, user.getPassword());
			
		}else if (template.getTemplateName().contains(Constants.TEMPLATE_FORGOT_EMAIL)) {
			 
			 String activationUrl = Constants.IP_ADDRESS_SERVER_CASINO_MANAGER;
				if (Constants.isClientBETMAIS) {
					activationUrl += "/activateCasinoManager.html?userName=" +user.getUsername() + "&verificationCode=" + user.getActivationCode();
				}
				System.out.println("activation URl" + activationUrl);

			 
			template.setMergeVar(Constants.EmailTemplates.USER_NAME, user.getUsername());
			template.setMergeVar( Constants.EmailTemplates.ACTIVATION_LINK, activationUrl);
		}/*
		 * else if (template.getTemplateName().contains(
		 * Constants.EmailTemplates.TEMPLATE_WITHDRAWAL_CONFIRMATION)) { }
		 *//*else if (template.getTemplateName().contains(
				Constants.EmailTemplates.settingMap.get(
						Utilities.getSkinNameFromServerPath(serverPath)).get(
						"TEMPLATE_WITHDRAWAL_PROCESSED"))) {
			template.setMergeVar(Constants.EmailTemplates.FIRST_NAME,
					user.getName());
			template.setMergeVar(Constants.EmailTemplates.DATE, map.get("date")
					.toString());
			template.setMergeVar(Constants.EmailTemplates.AMOUNT,
					map.get("amount").toString());
			template.setMergeVar(Constants.EmailTemplates.PAYMENT_PROVIDER, map
					.get("provider").toString());
		} else if (template.getTemplateName().contains(
				Constants.EmailTemplates.settingMap.get(
						Utilities.getSkinNameFromServerPath(serverPath)).get(
						"TEMPLATE_RISKY_COUNTRY"))) {
			template.setMergeVar(Constants.EmailTemplates.FIRST_NAME,
					user.getName());
		} else if (template.getTemplateName().contains(
				Constants.EmailTemplates.settingMap.get(
						Utilities.getSkinNameFromServerPath(serverPath)).get(
						"TEMPLATE_REFER_FRIEND"))) {
			template.setMergeVar(Constants.EmailTemplates.FIRST_NAME,
					user.getName());
		}
		else if (template.getTemplateName().contains(
				Constants.EmailTemplates.settingMap.get(
						Utilities.getSkinNameFromServerPath(serverPath)).get(
						"TEMPLATE_LOSSES_LIMIT_INCREASE_CONFIRMATION"))) {
			template.setMergeVar(Constants.EmailTemplates.FIRST_NAME,
					user.getName());
			template.setMergeVar(Constants.EmailTemplates.LOSS_VALUE, user.getLossValue().toString());
		}
		else if (template.getTemplateName().contains(
				Constants.EmailTemplates.settingMap.get(
						Utilities.getSkinNameFromServerPath(serverPath)).get(
						"TEMPLATE_INCREASE_LIMIT_REQUEST"))) {
			template.setMergeVar(Constants.EmailTemplates.FIRST_NAME,
					user.getName());
			template.setMergeVar(Constants.EmailTemplates.LOSS_VALUE, user.getLossValue().toString());
		}
		else if (template.getTemplateName().contains(
				Constants.EmailTemplates.settingMap.get(
						Utilities.getSkinNameFromServerPath(serverPath)).get(
						"TEMPLATE_SET_LIMIT"))) {
			template.setMergeVar(Constants.EmailTemplates.FIRST_NAME,
					user.getName());
			template.setMergeVar(Constants.EmailTemplates.LOSS_VALUE, user.getLossValue().toString());
		}*/
		// Create a Merge variable bucket with all the variables to be replaced.
		MergeVarBucket mvb = template.getBucket();
		mergeVars.add(mvb);
		template.reset();
		msg.setTo(recipients);
		msg.setMergeVars(mergeVars);
		return msg;
	}

/*	public void emailForCoolingPeriod(User user, String serverPath,
			HashMap<String, Object> map) {
		// TODO: Add multi language support -- Add Conditions per language and
		// Add templates accordingly.
		EmailSender sender = new EmailSender(mandrillApi);
		ITemplate template = new Template(Constants.EmailTemplates.settingMap
				.get(Utilities.getSkinNameFromServerPath(serverPath)).get(
						"TEMPLATE_INCREASE_LIMIT_REQUEST")
												 * Constants.EmailTemplates.
												 * TEMPLATE_PROFILE_UPDATE
												 
				+ Constants.EmailTemplates.ENGLISH, user);
		try {
			sender.sendTemplatedEmail(template, user, serverPath, map);
		} catch (MandrillApiError | Exception e) {
			e.printStackTrace();
		}
	}*/
	
/*	public void emailForSetLimit(User user, String serverPath,
			HashMap<String, Object> map) {
		// TODO: Add multi language support -- Add Conditions per language and
		// Add templates accordingly.
		EmailSender sender = new EmailSender(mandrillApi);
		ITemplate template = new Template(Constants.EmailTemplates.settingMap
				.get(Utilities.getSkinNameFromServerPath(serverPath)).get(
						"TEMPLATE_SET_LIMIT")
												 * Constants.EmailTemplates.
												 * TEMPLATE_PROFILE_UPDATE
												 
				+ Constants.EmailTemplates.ENGLISH, user);
		try {
			sender.sendTemplatedEmail(template, user, serverPath, map);
		} catch (MandrillApiError | Exception e) {
			e.printStackTrace();
		}
	}*/

	
	/**
	 * This is a method specific to the mail that sends a welcome note to the
	 * player when he activates his account and logs in for the first time.
	 * 
	 * @param user
	 *            the {@link User} object containing all the information about
	 *            the player.
	 * @return recipient the {@link Recipient} object with the email and name of
	 *         the player set.
	 * @see User
	 * @see Recipient this is the Recipient object that is needed for creating
	 *      the MandrillMessage.
	 */
	public Recipient getRecipient(User user) {

		Recipient recipient = new Recipient();
		recipient.setEmail(user.getEmail());
		recipient.setName(user.getName());

		return recipient;
	}


}
