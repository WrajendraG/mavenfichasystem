/*
 * Copyright @2015 Alteatec Pvt Ltd.
 */
package com.util;

import com.microtripit.mandrillapp.lutung.view.MandrillMessage.MergeVarBucket;
import com.modal.User;


// TODO: Auto-generated Javadoc
/**
 * The Interface ITemplate.
 */
public interface ITemplate {
	
	/**
	 * Gets the bucket.
	 *
	 * @return the bucket
	 * @throws Exception the exception
	 */
	public MergeVarBucket getBucket() throws Exception;
	
	/**
	 * Gets the global vars.
	 *
	 * @return the global vars
	 */
	public void getGlobalVars();
	
	/**
	 * Gets the template name.
	 *
	 * @return the template name
	 */
	public String getTemplateName();
	
	/**
	 * Sets the user.
	 *
	 * @param user the new user
	 */
	public void setUser(User user);
	
	/**
	 * Reset.
	 */
	public void reset();
	
	/**
	 * Sets the global var.
	 *
	 * @param name the name
	 * @param value the value
	 */
	public void setGlobalVar(String name, String value);
	
	/**
	 * Sets the merge var.
	 *
	 * @param name the name
	 * @param value the value
	 */
	public void setMergeVar(String name, String value);

}
