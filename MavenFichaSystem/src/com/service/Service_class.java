package com.service;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

import org.apache.commons.codec.binary.Hex;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.dao.Dao_Interface;
import com.modal.AdminEmailModel;
import com.modal.BalancesModel;
import com.modal.BusinessModal;
import com.modal.CasinoManager;
import com.modal.ConfirmPasswordModel;
import com.modal.Currency;
import com.modal.ForgotPassword;
import com.modal.PaybackModel;
import com.modal.Payment;
import com.modal.User;
import com.util.Constants;
import com.util.EmailSender;
import com.util.Utilities;

public class Service_class implements Service_Interface {
	@Autowired
	Dao_Interface daoi;

	/** The email sender. */
	@Autowired
	private EmailSender emailSender;

	@Autowired
	private Random random;

	// *************************currency
	// operations********************************************
	@Override
	@Transactional
	public List<String> showcurrency(Currency cur) {

		return daoi.showcurrency(cur);

	}

	@Override
	@Transactional
	public List<String> getCurrencyValues(Currency cur, Integer offSet, Integer maxResults) {
		return daoi.getCurrencyValues(cur, offSet, maxResults);
	}

	@Override
	@Transactional
	public List<String> showallcurrency(int id) {

		return daoi.showallcurrencyvalues(id);
		// return currencyallvalues;
	}

	@Override
	@Transactional
	public String insert(Currency cur, String currencyname) {

		return daoi.insert(cur, currencyname);

	}

	@Override
	@Transactional
	public String update(Currency cur, String currencyname) {

		return daoi.update(cur, currencyname);

	}

	@Override
	@Transactional
	public String delete(Currency cur) {

		return daoi.delete(cur);
	}

	@Override
	@Transactional
	public String deleteCurrencyValuesId(int id) {

		return daoi.deleteCurrencyValuesId(id);
	}

	@Override
	@Transactional
	public List<String> currencyFromCurrencyName(String currencyName) {

		return daoi.currencyFromCurrencyName(currencyName);
	}

	// ***************************************payment
	// operations********************************************

	@Override
	@Transactional
	public List<String> showdpaymentvalues(Payment p) {

		return daoi.showpaymentvalues(p);
	}

	@Override
	@Transactional
	public String insertpayment(Payment p, String paymentname, String paymentmethod) {

		return daoi.insertpayment(p, paymentname, paymentmethod);

	}

	@Override
	@Transactional
	public String updatepayment(Payment p, String paymentname, String paymentmethod) {

		return daoi.updatepayment(p, paymentname, paymentmethod);

	}

	@Override
	@Transactional
	public String deletepayment(Payment p) {

		return daoi.deletepayment(p);

	}

	@Override
	@Transactional
	public List<String> showdpaymentvalues(int id) {

		return daoi.paymentallvalues(id);

	}

	@Override
	@Transactional
	public String deletePaymentValuesId(int id) {

		return daoi.deletePaymentValuesId(id);
	}

	// ****************************************Casino Manager
	// operations********************************************

	@Override
	@Transactional
	public List<String> showcasino(CasinoManager cm) {

		return daoi.showcasino(cm);

	}

	@Override
	@Transactional
	public String insertcasino(CasinoManager casinoManagerModel, String casinousername) {
		
		String passwordForWelcomeEmail = casinoManagerModel.getPassword();
		
		byte[] salt = new byte[64];
		random.nextBytes(salt);
		casinoManagerModel.setSalt(Hex.encodeHexString(salt));
		casinoManagerModel.setPassword(createPasswordHash(casinoManagerModel.getPassword(), salt));

		String result =  daoi.insertcasino(casinoManagerModel, casinousername);
		
		if ("success".equalsIgnoreCase(result)) {
			
			HashMap<String, Object> responseMap = new HashMap<String, Object>();
			
			User user = new User();
			user.setUsername(casinoManagerModel.getUsername());
			user.setPassword(passwordForWelcomeEmail);
			user.setEmail(casinoManagerModel.getEmail());

			EmailRunnable myRunnable = new EmailRunnable(user, Constants.IP_ADDRESS_SERVER, emailSender, "welcomeEmail", responseMap);
			Thread thread = new Thread(myRunnable);
			thread.start();

		}
		
		return result;

	}

	@Override
	@Transactional
	public String updatecasino(CasinoManager casinoManagerModel, String casinousername) {

		if (casinoManagerModel.getPassword() != null && !casinoManagerModel.getPassword().isEmpty()) {
			byte[] salt = new byte[64];
			random.nextBytes(salt);
			casinoManagerModel.setSalt(Hex.encodeHexString(salt));
			casinoManagerModel.setPassword(createPasswordHash(casinoManagerModel.getPassword(), salt));
		}

		return daoi.updatecasino(casinoManagerModel, casinousername);

	}

	@Override
	@Transactional
	public String deletecasino(CasinoManager cm) {

		return daoi.deletecasino(cm);
	}

	@Override
	@Transactional
	public List<String> showcasinoallvalues(int id) {

		return daoi.showall(id);

	}

	// ***************************************Businesss
	// operation*********************************************
	@Override
	@Transactional
	public String insertbusiness(BusinessModal businessPartnerModel, String username) {

		byte[] salt = new byte[64];
		random.nextBytes(salt);
		businessPartnerModel.setSalt(Hex.encodeHexString(salt));
		businessPartnerModel.setPassword(createPasswordHash(businessPartnerModel.getPassword(), salt));

		businessPartnerModel.setRegistrationSource(Constants.FICHA_REGISTRATION_CASINO_MANAGER);
		businessPartnerModel.setActivationCode(UUID.randomUUID().toString());
		businessPartnerModel.setActivatedStatus(false);
		
		if(businessPartnerModel.getAgent() == true){
			businessPartnerModel.setAgentType(Constants.MAIN_AGENT);
		}

		String result = daoi.insertbusiness(businessPartnerModel, username);

		if ("success".equalsIgnoreCase(result) && businessPartnerModel.getStatus() == Constants.STATUS_APPROVED) {

			List<HashMap> businessPartnerListForActivation = daoi.getBusinessPartnersForActivation(businessPartnerModel.getId().toString());

			HashMap<String, Object> responseMap = new HashMap<String, Object>();
			for (HashMap businessPartnerForActivation : businessPartnerListForActivation) {

				User user = new User();
				user.setUsername((String) businessPartnerForActivation.get("business_username"));
				user.setEmail((String) businessPartnerForActivation.get("business_email"));
				user.setActivationCode((String) businessPartnerForActivation.get("bp_activation_code"));

				EmailRunnable myRunnable = new EmailRunnable(user, Constants.IP_ADDRESS_SERVER, emailSender, "activationEmail", responseMap);
				Thread thread = new Thread(myRunnable);
				thread.start();

			}
		}

		return result;
	}

	@Override
	@Transactional
	public String updatebusiness(BusinessModal businessPartnerModel, String username) {
		
		if (businessPartnerModel.getPassword() != null && !businessPartnerModel.getPassword().isEmpty()) {
			byte[] salt = new byte[64];
			random.nextBytes(salt);
			businessPartnerModel.setSalt(Hex.encodeHexString(salt));
			businessPartnerModel.setPassword(createPasswordHash(businessPartnerModel.getPassword(), salt));
		}

		String result = daoi.updatebusiness(businessPartnerModel, username);

		if ("success".equalsIgnoreCase(result) && businessPartnerModel.getActivatedStatus() == false && businessPartnerModel.getStatus() == Constants.STATUS_APPROVED) {

			List<HashMap> businessPartnerListForActivation = daoi.getBusinessPartnersForActivation(businessPartnerModel.getId().toString());

			HashMap<String, Object> responseMap = new HashMap<String, Object>();
			for (HashMap businessPartnerForActivation : businessPartnerListForActivation) {

				User user = new User();
				user.setUsername((String) businessPartnerForActivation.get("business_username"));
				user.setEmail((String) businessPartnerForActivation.get("business_email"));
				user.setActivationCode((String) businessPartnerForActivation.get("bp_activation_code"));

				EmailRunnable myRunnable = new EmailRunnable(user, Constants.IP_ADDRESS_SERVER, emailSender, "activationEmail", responseMap);
				Thread thread = new Thread(myRunnable);
				thread.start();

			}
		}

		return result;
	}

	@Override
	@Transactional
	public String deletebusiness(BusinessModal b1) {

		String s1 = daoi.deletebusiness(b1);
		return s1;
	}

	@Override
	@Transactional
	public List<String> showdbusiness(BusinessModal b1) {

		
		return daoi.showbusinessvalues(b1); 
	}

	@Override
	@Transactional
	public List<String> businessallvalues(int id) {

		 
		return daoi.showbusinessallvalues(id);
	}

	@Override
	@Transactional
	public List<String> getcasinoid(String uname) {

		
		return daoi.getcasinoid(uname);
	}

	@Override
	@Transactional
	public String deletebusinessValuesId(int id) {

		return daoi.deletebusinessValuesId(id);
	}

	@Override
	@Transactional
	public List<String> checkBusinessPartnerFromusername(String businessUserName, Integer casinoManagerId) {

		return daoi.checkBusinessPartnerFromusername(businessUserName, casinoManagerId);
	}

	// **********************************************************************************************************

	@Override
	@Transactional
	public List<String> showdepositto(BalancesModel d1) {

		
		return daoi.showdeposittocasino(d1); 
	}

	@Override
	@Transactional
	public List<String> getbusinessvalues() {

		 
		return daoi.getbusinesspartnervalues();
	}

	@Override
	@Transactional
	public List<String> getcurrencyvalues() {

		 
		return daoi.getdepositcurrency();
	}

	@Override
	@Transactional
	public List<String> getpaymentvalues() {

		 
		return daoi.getpaymentvalues();
	}

	@Override
	@Transactional
	public List<String> paymentFromPaymentTypeName(String paymentTypeName) {

		return daoi.paymentFromPaymentTypeName(paymentTypeName);
	}

	// ******************************************Deposit to casino
	// operation**********************
	@Override
	@Transactional
	public String insertdepositto(BalancesModel d1) {

		String s1 = daoi.insertdeposittocasino(d1);
		return s1;
	}

	@Override
	@Transactional
	public String updatedepositto(BalancesModel d1) {

		String s1 = daoi.updatedeposittocasino(d1);
		return s1;
	}

	@Override
	@Transactional
	public String deletedepositto(BalancesModel d1) {

		String s1 = daoi.deletedeposittocasino(d1);
		return s1;
	}

	@Override
	@Transactional
	public List<String> showdeposittocasinoallvalues(int id) {

		 
		return daoi.getallvalues(id);
	}

	@Override
	@Transactional
	public String deleteCasinoValues(int id) {

		String s1 = daoi.deleteCasinoValues(id);
		return s1;
	}

	@Override
	@Transactional
	public String deleteCasinoManagerId(int id) {

		return daoi.deleteCasinoManagerId(id);
	}

	@Override
	@Transactional
	public List<String> depositToCasinoFromUserNameValues(Integer casinoManagerId, String businessUserName) {

		return daoi.depositToCasinoFromUserNameValues(casinoManagerId, businessUserName);
	}

	// *************************dashing*****************************
	@Override
	@Transactional
	public List<String> pendingBusinessPartnersList(Integer casinoManagerId) {
		List<String> pendingBusinessPartnersList = new ArrayList<String>();
		pendingBusinessPartnersList = daoi.pendingBusinessPartnersList(casinoManagerId);
		return pendingBusinessPartnersList;
	}

	@Override
	@Transactional
	public List<Integer> loginList(String uname, String pass) {

		 
		return daoi.loginlist(uname, pass);
	}

	@Override
	@Transactional
	public Map<Integer, String> getBusinessPartnerIdValueMap() {
		return daoi.getBusinessPartnerIdValueMap();
	}

	@Override
	@Transactional
	public Map<Integer, String> getCurrencyIdValueMap() {
		return daoi.getCurrencyIdValueMap();
	}

	@Override
	@Transactional
	public Map<Integer, String> getPaymentTypeIdValueMap() {
		return daoi.getPaymentTypeIdValueMap();
	}

	@Override
	@Transactional
	public String aproveBusinessPartners(String businessPartnerToBeApproved, int status, Integer casinoManagerId) {

		String result = daoi.aproveBusinessPartners(businessPartnerToBeApproved, status, casinoManagerId);

		if ("success".equalsIgnoreCase(result)) {

			List<HashMap> businessPartnerListForActivation = daoi.getBusinessPartnersForActivation(businessPartnerToBeApproved);

			HashMap<String, Object> responseMap = new HashMap<String, Object>();
			for (HashMap businessPartnerForActivation : businessPartnerListForActivation) {

				User user = new User();
				user.setUsername((String) businessPartnerForActivation.get("business_username"));
				user.setEmail((String) businessPartnerForActivation.get("business_email"));
				user.setActivationCode((String) businessPartnerForActivation.get("bp_activation_code"));

				EmailRunnable myRunnable = new EmailRunnable(user, Constants.IP_ADDRESS_SERVER, emailSender, "activationEmail", responseMap);
				Thread thread = new Thread(myRunnable);
				thread.start();

			}
		}

		return result;
	}

	@Override
	@Transactional
	public List<String> pendingBalances(Integer casinoManagerId) {

		return daoi.pendingBalances(casinoManagerId);
	}

	@Override
	@Transactional
	public String aproveBalances(String balancesToBeApproved, Integer status, Integer depositType) {

		return daoi.approveBalances(balancesToBeApproved, status, depositType);
	}

	@Override
	@Transactional
	public boolean isCurrencyBeingUsed(Currency cur) {

		return daoi.isCurrencyBeingUsed(cur);
	}

	@Override
	@Transactional
	public List<String> casinoManagerValuesFromUsernames(String casinoManagerUserName) {

		return daoi.casinoManagerValuesFromUsernames(casinoManagerUserName);
	}

	@Override
	@Transactional
	public boolean isPaymentTypeBeingUsed(Payment paymentType) {
		return daoi.isPaymentTypeBeingUsed(paymentType);
	}

	@Override
	@Transactional
	public List<String> getCountryNameList() {

		List<String> countryNameList = new ArrayList<String>();

		countryNameList = daoi.getCountryNameList();
		return countryNameList;
	}

	@Override
	@Transactional
	public Map<String, String> getCountryPhoneCodeMap() {
		Map<String, String> countryNameList = new HashMap<String, String>();

		countryNameList = daoi.getCountryPhoneCodeMap();
		return countryNameList;
	}

	@Override
	@Transactional
	public List<String> getApprovedAmountsList(Integer casinoManagerId) {
		List<String> approvedAmountsList = new ArrayList<String>();
		approvedAmountsList = daoi.getApprovedAmountsList(casinoManagerId);
		return approvedAmountsList;
	}

	@Override
	@Transactional
	public List<String> getApprovedCreditsList(Integer casinoManagerId) {
		List<String> approvedCreditsList = new ArrayList<String>();
		approvedCreditsList = daoi.getApprovedCreditsList(casinoManagerId);
		return approvedCreditsList;
	}

	@Override
	@Transactional
	public List<String> getApprovedAmountListbyUser(
			String userNameApprovedAmount) {
		// TODO Auto-generated method stub
		return daoi.getApprovedAmountListbyUser(userNameApprovedAmount);
	}
	
	
	
	@Override
	@Transactional
	public List<String> getApprovedCreditListbyUser(
			String userNameApprovedCredits) {
		// TODO Auto-generated method stub
		return daoi.getApprovedCreditListbyUser(userNameApprovedCredits);
	}
	
	
	@Override
	@Transactional
	public List<String> getBusinessPartnerIdInformationDisplay(int businessPartnerId) {
		// TODO Auto-generated method stub
		return daoi.getBusinessPartnerIdInformationDisplay(businessPartnerId);
	}
	
	@Override
	@Transactional
	public List<String> getbusinessPartnerIdPersonalInformation(
			int businessPartnerId) {
		// TODO Auto-generated method stub
		return daoi.getbusinessPartnerIdPersonalInformation(businessPartnerId);
	}
	
	
	@Override
	@Transactional
	public List<String> getPlayers(String username) {
		
		// TODO Auto-generated method stub
		return daoi.getPlayer(username);
	}

	@Override
	@Transactional
	public List<String> getPlayersForBusinessPartner(String username) {
		// TODO Auto-generated method stub
		return daoi.getPlayerForBusinessPartner(username);
	}
	
	@Override
	@Transactional
	public List<String> getPlayersForAgents(String username) {
		// TODO Auto-generated method stub
		return daoi.getPlayersForAgents(username);
	}
	
	@Override
	@Transactional
	public String insertpayback(PaybackModel paybackmodel) {
		// TODO Auto-generated method stub
		paybackmodel.setCreatedOn(new Date());
		paybackmodel.setUpdatedOn(new Date());
		return daoi.insertPayBack(paybackmodel);
	}
	
	@Override
	@Transactional
	public List<String> getPayBackValues(String username) {
		// TODO Auto-generated method stub
		return daoi.getPayBackValues(username);
	}
	

	@Override
	@Transactional
	public boolean validateCurrencyForPayBack(Integer businessPartnerId, Integer currencyIdUsedforPayBack) {
		// TODO Auto-generated method stub
		return daoi.validateCurrencyForPayBack(businessPartnerId, currencyIdUsedforPayBack);
	}
	
	@Override
	@Transactional
	public boolean validatePayBackAmount(PaybackModel payBackModel) {
		// TODO Auto-generated method stub
		return daoi.validatePayBackAmount(payBackModel);
	}

	@Override
	@Transactional
	public String checkUserName(
			ForgotPassword forgotPassword) {
		// TODO Auto-generated method stub
		
		String result;
		CasinoManager casinoManager=new CasinoManager(); ;
		String s1 = daoi.checkUserName(forgotPassword);
		System.out.println("email = "+s1);
		if(s1.equals("Fail"))
		{
		 result=""+s1;
		}
		else
		{
			
			casinoManager.setCasinoVerification(UUID.randomUUID().toString());
			List<Integer>getCasinoManagerId=daoi.updateActivationCode(forgotPassword,casinoManager);
			
			List<HashMap> casinoManagerVerificationDetails = daoi.getCasinoManagerForV(getCasinoManagerId.get(0));

			HashMap<String, Object> responseMap = new HashMap<String, Object>();
			for (HashMap casinoManagerVerificationDetailsMap : casinoManagerVerificationDetails) {
				User user = new User();
				user.setUsername((String) casinoManagerVerificationDetailsMap.get("casino_username"));
				user.setEmail((String) casinoManagerVerificationDetailsMap.get("casino_email"));
				user.setActivationCode((String) casinoManagerVerificationDetailsMap.get("casino_verification"));

				EmailRunnable myRunnable = new EmailRunnable(user, Constants.IP_ADDRESS_SERVER, emailSender, "forgotPasswordVerificationEmail", responseMap);
				Thread thread = new Thread(myRunnable);
				thread.start();
			
		}
		
		}
	return s1;
	}
	
	/**
	 * Creates the password hash.
	 *
	 * @param password
	 *            the password
	 * @param salt
	 *            the salt
	 * @return the string
	 */
	private String createPasswordHash(String password, byte[] salt) {
		byte[] hash;
		try {
			MessageDigest digest = MessageDigest.getInstance("SHA-512");
			digest.update(salt);
			digest.update(password.getBytes());
			hash = digest.digest();
		} catch (NoSuchAlgorithmException e) {
			System.out.println(e);
			System.out.println("Error generating secure password");
			throw new UnsupportedOperationException(e);
		}
		return Hex.encodeHexString(hash);
	}

	@Override
	@Transactional
	public String forgotPasswordOnActivationAction(
			ConfirmPasswordModel confirmPasswordModel) {
		// TODO Auto-generated method stub
		byte[] salt = new byte[64];
		random.nextBytes(salt);
		confirmPasswordModel.setSalt(Hex.encodeHexString(salt));
		confirmPasswordModel.setResetPassword(createPasswordHash(confirmPasswordModel.getResetPassword(), salt));
		String result =  daoi.forgotPasswordOnActivationAction(confirmPasswordModel);
		return result;
		
	}

	@Override
	@Transactional
	public String setAdminEmail(AdminEmailModel adminemail) {
		// TODO Auto-generated method stub
		return daoi.setAdminEmail(adminemail);
	}

}

class EmailRunnable implements Runnable {
	private User user;
	private String serverPath;
	private EmailSender emailSender;
	private String identifier;
	private HashMap<String, Object> map;

	public EmailRunnable(User user, String serverPath, EmailSender emailSender, String identifier, HashMap<String, Object> map) {
		this.user = user;
		this.serverPath = serverPath;
		this.emailSender = emailSender;
		this.identifier = identifier;
		this.map = map;
	}

	public void run() {
		if (identifier.contains("welcomeEmail")) {
			emailSender.welcomeEmail(user, serverPath, map);
		}  else if (identifier.contains("activationEmail")) {
			emailSender.activationEmail(user, serverPath, map);
		} else if (identifier.contains("forgotPasswordVerificationEmail")) {
			emailSender.verificationEmail(user, serverPath, map);
		}
	}
}