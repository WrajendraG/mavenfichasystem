package com.controller;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.form.validator.AdminEmailValidation;
import com.form.validator.BusinessFormValidation;
import com.form.validator.CasinoFormValidation;
import com.form.validator.ConfirmPasswordFormValidation;
import com.form.validator.CurrencyFormValidators;
import com.form.validator.DeposittoCasinoValidations;
import com.form.validator.ForgotPasswordFormValidation;
import com.form.validator.PayBackFormValidation;
import com.form.validator.PaymentFormValidations;
import com.modal.AdminEmailModel;
import com.modal.BalancesModel;
import com.modal.BusinessModal;
import com.modal.CasinoManager;
import com.modal.ConfirmPasswordModel;
import com.modal.Currency;
import com.modal.DepositWithdrawl;
import com.modal.ForgotPassword;
import com.modal.Modelbean;
import com.modal.NewPartner;
import com.modal.PaybackModel;
import com.modal.Payment;
import com.service.Service_Interface;
import com.util.Constants;
import com.util.Utilities;

@Controller
@EnableWebMvc
public class First {

	@Autowired
	Service_Interface servint;
	
	@Autowired
	private MessageSource messageSource;

	@RequestMapping(value = "/login")
	public ModelAndView login(Model model) {
		ModelAndView loginModelAndView = new ModelAndView("Login");
		model.addAttribute("go", new Modelbean());
		return loginModelAndView;
	}
	

	/*@RequestMapping(value="/newPartner")
	public ModelAndView newPartner(@ModelAttribute("newPartner") NewPartner newpartner,Model model,HttpServletRequest req, BindingResult result)
	{
		ModelAndView loginModelView = new ModelAndView("Login");
		boolean isUserLoggedIn = Utilities.isUserLoggedIn(req, model, result);
		if(isUserLoggedIn == false){
			return loginModelView;
		}
		return new ModelAndView("newPartner");
	}
	
	
	@RequestMapping(value="/depositWithdrawl")
	public ModelAndView depositWithdrawl(@ModelAttribute("depositWithdrawl") DepositWithdrawl depositwithdrawl,Model model,HttpServletRequest req, BindingResult result)
	{
		ModelAndView loginModelView = new ModelAndView("Login");
		boolean isUserLoggedIn = Utilities.isUserLoggedIn(req, model, result);
		if(isUserLoggedIn == false){
			return loginModelView;
		}
		return new ModelAndView("depositWithdrawl");
	}

	@RequestMapping(value="/creditStatus")
	public ModelAndView creditWithdrwal(Model model,HttpServletRequest req, BindingResult result)
	{
		ModelAndView loginModelView = new ModelAndView("Login");
		boolean isUserLoggedIn = Utilities.isUserLoggedIn(req, model, result);
		if(isUserLoggedIn == false){
			return loginModelView;
		}
		return new ModelAndView("creditStatus");
	}
	
*/	
	
	@RequestMapping(value = "/casinoManagerInsert")
	public ModelAndView casinoManagerInsert(@ModelAttribute("ncasino") CasinoManager cm, Model model, BindingResult result, HttpServletRequest req)
	{
		ModelAndView loginModelView = new ModelAndView("Login");
		boolean isUserLoggedIn = Utilities.isUserLoggedIn(req, model, result);
		if(isUserLoggedIn == false){
			return loginModelView;
		}
		HttpSession httpSession = req.getSession(false);
		List<String> countryNameList =(List<String>) httpSession.getAttribute("CountryNameList");
		model.addAttribute("countryList", countryNameList);
		
		Map<String, String> countryPhoneCodeList =(Map<String, String>) httpSession.getAttribute("CountryPhoneCodeList");
		model.addAttribute("countryPhoneCodeList", countryPhoneCodeList);
		
		Integer casinoManagerId = (Integer) req.getSession().getAttribute("casinoManagerId");
		model.addAttribute("casinoManagerId", casinoManagerId);
		return new ModelAndView("casinoManagerInsert");
	}
	
	
	@RequestMapping(value = "/casinoManagerUpdate")
	public ModelAndView casinoManagerUpdate(@ModelAttribute("ncasino") CasinoManager cm, Model model, BindingResult result, HttpServletRequest req)
	{
		ModelAndView loginModelView = new ModelAndView("Login");
		boolean isUserLoggedIn = Utilities.isUserLoggedIn(req, model, result);
		if(isUserLoggedIn == false){
			return loginModelView;
		}
		HttpSession httpSession = req.getSession(false);
		List<String> countryNameList =(List<String>) httpSession.getAttribute("CountryNameList");
		model.addAttribute("countryList", countryNameList);
		
		Map<String, String> countryPhoneCodeList =(Map<String, String>) httpSession.getAttribute("CountryPhoneCodeList");
		model.addAttribute("countryPhoneCodeList", countryPhoneCodeList);
		
		Integer casinoManagerId = (Integer) req.getSession().getAttribute("casinoManagerId");
		model.addAttribute("casinoManagerId", casinoManagerId);
		return new ModelAndView("casinoManagerUpdate");
	}
	
	@RequestMapping(value="/insertCasinoManager")
	public ModelAndView insertCasinoManager(@ModelAttribute("ncasino") CasinoManager cm, Model model, BindingResult result, HttpServletRequest req)
	{
		ModelAndView loginModelView = new ModelAndView("Login");
		boolean isUserLoggedIn = Utilities.isUserLoggedIn(req, model, result);
		if(isUserLoggedIn == false){
			return loginModelView;
		}
		
		List<String> casinoManagerValuesFromUsernames = new ArrayList<String>();
		PagedListHolder pagedListHolder = new PagedListHolder(casinoManagerValuesFromUsernames);
		int page = ServletRequestUtils.getIntParameter(req, "p", 0);
		pagedListHolder.setPage(page);
		int pageSize = 10;
		pagedListHolder.setPageSize(pageSize);
		model.addAttribute("pagedListHolder", pagedListHolder);
		
		HttpSession httpSession = req.getSession(false);
		String casinomanagerusername = cm.getUsername();
		
		List<String> countryNameList =(List<String>) httpSession.getAttribute("CountryNameList");
		model.addAttribute("countryList", countryNameList);
		
		Map<String, String> countryPhoneCodeList =(Map<String, String>) httpSession.getAttribute("CountryPhoneCodeList");
		model.addAttribute("countryPhoneCodeList", countryPhoneCodeList);
		
		CasinoFormValidation casinoform = new CasinoFormValidation();
		casinoform.validate(cm, result);
		
		if (result.hasErrors()) {

			httpSession = req.getSession();
			httpSession.setAttribute("isValidationErrorPresentCasinoManager", "Y");
			return new ModelAndView("casionmanger");
		} else {
			httpSession = req.getSession();
			httpSession.setAttribute("isValidationErrorPresentCasinoManager", "N");
			
			String password = cm.getPassword();
			String passwordConfirmation = cm.getConpassword();
			
			String s1 = servint.insertcasino(cm, casinomanagerusername);
			if (s1.equalsIgnoreCase("Success")) {
				// System.out.println(s1);
				casinoManagerValuesFromUsernames = servint.casinoManagerValuesFromUsernames("");
				pagedListHolder.setSource(casinoManagerValuesFromUsernames);
				model.addAttribute("message", "Inserted Successfully");
				return new ModelAndView("casionmanger");
			} else {
				// System.out.println(s1);
				httpSession.setAttribute("isValidationErrorPresentCasinoManager", "Y");
				cm.setPassword(password);
				cm.setPassword(passwordConfirmation);
				model.addAttribute("message", messageSource.getMessage("error.user.name.already.exists", null, RequestContextUtils.getLocale(req)));
				return new ModelAndView("casionmanger");
			}

		}

	}
	
	@RequestMapping(value="/updateCasinoManager")
	public ModelAndView updateCasinoManager(@ModelAttribute("ncasino") CasinoManager cm, Model model, BindingResult result, HttpServletRequest req)
	{
		
		ModelAndView loginModelView = new ModelAndView("Login");
		boolean isUserLoggedIn = Utilities.isUserLoggedIn(req, model, result);
		if(isUserLoggedIn == false){
			return loginModelView;
		}
		
		List<String> casinoManagerValuesFromUsernames = new ArrayList<String>();
		PagedListHolder pagedListHolder = new PagedListHolder(casinoManagerValuesFromUsernames);
		int page = ServletRequestUtils.getIntParameter(req, "p", 0);
		pagedListHolder.setPage(page);
		int pageSize = 10;
		pagedListHolder.setPageSize(pageSize);
		model.addAttribute("pagedListHolder", pagedListHolder);
		
		HttpSession httpSession = req.getSession(false);
		
		List<String> countryNameList =(List<String>) httpSession.getAttribute("CountryNameList");
		model.addAttribute("countryList", countryNameList);
		
		Map<String, String> countryPhoneCodeList =(Map<String, String>) httpSession.getAttribute("CountryPhoneCodeList");
		model.addAttribute("countryPhoneCodeList", countryPhoneCodeList);
		
		cm.setIsUpdate(true);
		CasinoFormValidation casinoform = new CasinoFormValidation();
		casinoform.validate(cm, result);
		
		if (result.hasErrors()) {
			httpSession.setAttribute("isValidationErrorForCasinoManagerUpdate", "Y");
			return new ModelAndView("casionmanger");
		} else{
			httpSession.setAttribute("isValidationErrorForCasinoManagerUpdate", "N");
	
			String password = cm.getPassword();
			String passwordConfirmation = cm.getConpassword();
			
			String casinomanagerusername = cm.getUsername();
			String s1 = servint.updatecasino(cm, casinomanagerusername);
			if (s1.equalsIgnoreCase("Success")) {
				// System.out.println(s1);
				casinoManagerValuesFromUsernames = servint.casinoManagerValuesFromUsernames("");
				pagedListHolder.setSource(casinoManagerValuesFromUsernames);
				model.addAttribute("message", "Updated Successfully");
				return new ModelAndView("casionmanger");
			} else {
				// System.out.println(s1);
				httpSession.setAttribute("isValidationErrorForCasinoManagerUpdate", "Y");
				cm.setPassword(password);
				cm.setPassword(passwordConfirmation);
				model.addAttribute("message", messageSource.getMessage("error.user.name.already.exists", null, RequestContextUtils.getLocale(req)));
				return new ModelAndView("casionmanger");
			}
		}
	}
	
	
	
	@RequestMapping(value="/currencyInsert")
	public ModelAndView currencyInsert(@ModelAttribute("curr") Currency cur,Model model,HttpServletRequest req, BindingResult result)
	{
		ModelAndView loginModelView = new ModelAndView("Login");
		boolean isUserLoggedIn = Utilities.isUserLoggedIn(req, model, result);
		if(isUserLoggedIn == false){
			return loginModelView;
		}
		return new ModelAndView("currencyInsert");
	}
	
	@RequestMapping(value="/currencyUpdate")
	public ModelAndView currencyUpdate(@ModelAttribute("curr") Currency cur,Model model,HttpServletRequest req, BindingResult result)
	{
		ModelAndView loginModelView = new ModelAndView("Login");
		boolean isUserLoggedIn = Utilities.isUserLoggedIn(req, model, result);
		if(isUserLoggedIn == false){
			return loginModelView;
		}
		return new ModelAndView("currencyUpdate");
	}
	
	//Inserting Currency Value
	@RequestMapping(value="/insertCurrency")
	public ModelAndView insertCurrency(@ModelAttribute("curr") Currency cur,Model model,HttpServletRequest req, BindingResult result)
	{
		ModelAndView loginModelView = new ModelAndView("Login");
		boolean isUserLoggedIn = Utilities.isUserLoggedIn(req, model, result);
		if(isUserLoggedIn == false){
			return loginModelView;
		}
		
		PagedListHolder pagedListHolder = new PagedListHolder(new ArrayList<String>());
		int page = ServletRequestUtils.getIntParameter(req, "p", 0);
		pagedListHolder.setPage(page);
		int pageSize = 10;
		pagedListHolder.setPageSize(pageSize);
		model.addAttribute("pagedListHolder", pagedListHolder);
		
		CurrencyFormValidators c = new CurrencyFormValidators();

		c.validate(cur, result);
		HttpSession httpSession = req.getSession(false);
		if (result.hasErrors()) {
			httpSession.setAttribute("isValidationErrorPresentCurrency", "Y");
			return new ModelAndView("currencyInsert");
		} else {
			httpSession.setAttribute("isValidationErrorPresentCurrency", "N");
			String currencyname = cur.getName();
			String s1 = servint.insert(cur, currencyname);
			List<String> currencyvalues = servint.currencyFromCurrencyName("");
			pagedListHolder.setSource(currencyvalues);
			if (s1.equalsIgnoreCase("Success")) {
				return new ModelAndView("currency");
			} else {
				return new ModelAndView("currency");
			}
		}

		
	}
	
	//Updating Currency Value
	@RequestMapping(value="/updateCurrency")
	public ModelAndView updateCurrency(@ModelAttribute("curr") Currency cur,Model model,HttpServletRequest req, BindingResult result)
	{
		ModelAndView loginModelView = new ModelAndView("Login");
		boolean isUserLoggedIn = Utilities.isUserLoggedIn(req, model, result);
		if(isUserLoggedIn == false){
			return loginModelView;
		}
		
		PagedListHolder pagedListHolder = new PagedListHolder(new ArrayList<String>());
		int page = ServletRequestUtils.getIntParameter(req, "p", 0);
		pagedListHolder.setPage(page);
		int pageSize = 10;
		pagedListHolder.setPageSize(pageSize);
		model.addAttribute("pagedListHolder", pagedListHolder);
		
		CurrencyFormValidators c = new CurrencyFormValidators();

		c.validate(cur, result);
		HttpSession httpSession = req.getSession(false);
		if (result.hasErrors()) {
			httpSession.setAttribute("isValidationErrorPresentCurrencyUpdate", "Y");
			return new ModelAndView("currencyUpdate");
		} 
		else {
			httpSession.setAttribute("isValidationErrorPresentCurrencyUpdate", "N");
		
			String currencyname = cur.getName();
			String s1 = servint.update(cur, currencyname);
			List<String> currencyvalues = servint.currencyFromCurrencyName("");
			pagedListHolder.setSource(currencyvalues);
			if (s1.equalsIgnoreCase("Success")) {
				
				return new ModelAndView("currency");
			} else {
				
				return new ModelAndView("currency");
			}
		
		}
		
		

	}
	
	
	
	
	
	@RequestMapping(value = "/paymentInsert")
	public ModelAndView paymentInsert(@ModelAttribute("paym") Payment p, Model model,HttpServletRequest req, BindingResult result)
	{   
		ModelAndView loginModelView = new ModelAndView("Login");
		boolean isUserLoggedIn = Utilities.isUserLoggedIn(req, model, result);
		if(isUserLoggedIn == false){
			return loginModelView;
		}
		return new ModelAndView("paymentInsert");
	}
	
	@RequestMapping(value="/paymentUpdate")
	public ModelAndView paymentUpdate(@ModelAttribute("paym") Payment p, Model model,HttpServletRequest req, BindingResult result)
	{
		ModelAndView loginModelView = new ModelAndView("Login");
		boolean isUserLoggedIn = Utilities.isUserLoggedIn(req, model, result);
		if(isUserLoggedIn == false){
			return loginModelView;
		}
		return new ModelAndView("paymentUpdate");
	}
	
	
	@RequestMapping(value="/insertPayment")
	public ModelAndView insertPayment(@ModelAttribute("paym") Payment p, Model model, BindingResult result, HttpServletRequest req)
	{
		ModelAndView loginModelView = new ModelAndView("Login");
		boolean isUserLoggedIn = Utilities.isUserLoggedIn(req, model, result);
		if(isUserLoggedIn == false){
			return loginModelView;
		}
		
		PagedListHolder pagedListHolder = new PagedListHolder(new ArrayList<String>());
		int page = ServletRequestUtils.getIntParameter(req, "p", 0);
		pagedListHolder.setPage(page);
		int pageSize = 10;
		pagedListHolder.setPageSize(pageSize);
		model.addAttribute("pagedListHolder", pagedListHolder);
		
		PaymentFormValidations paymentvalids = new PaymentFormValidations();

		paymentvalids.validate(p, result);
		HttpSession httpSession = req.getSession(false);
		if (result.hasErrors()) {

			httpSession.setAttribute("isValidationErrorPresentPayment", "Y");
			return new ModelAndView("paymentInsert");
		}else {

			httpSession.setAttribute("isValidationErrorPresentPayment", "N");
			String paymentname = p.getName();
			String paymentmethod = p.getMethod12();
			String s1 = servint.insertpayment(p, paymentname, paymentmethod);
			if (s1.equalsIgnoreCase("Success")) {
				// System.out.println(s1);
				List<String> paymentFromPaymentTypeName = servint.paymentFromPaymentTypeName("");
				pagedListHolder.setSource(paymentFromPaymentTypeName);
				model.addAttribute("message", "Inserted Successfully");
				return new ModelAndView("payment");
			} else {
				// System.out.println(s1);
				model.addAttribute("message", "Payment type name already exists.");
				return new ModelAndView("payment");
			}
		}
	}
	
	@RequestMapping(value="/updatePayment")
	public ModelAndView updatePayment(@ModelAttribute("paym") Payment p, Model model, BindingResult result, HttpServletRequest req)
	{
		ModelAndView loginModelView = new ModelAndView("Login");
		boolean isUserLoggedIn = Utilities.isUserLoggedIn(req, model, result);
		if(isUserLoggedIn == false){
			return loginModelView;
		}
		
		PagedListHolder pagedListHolder = new PagedListHolder(new ArrayList<String>());
		int page = ServletRequestUtils.getIntParameter(req, "p", 0);
		pagedListHolder.setPage(page);
		int pageSize = 10;
		pagedListHolder.setPageSize(pageSize);
		model.addAttribute("pagedListHolder", pagedListHolder);
		PaymentFormValidations paymentvalids = new PaymentFormValidations();

		paymentvalids.validate(p, result);
		HttpSession httpSession = req.getSession(false);
		if (result.hasErrors()) {
			httpSession.setAttribute("isValidationErrorPresentPaymentUpdate", "Y");
			return new ModelAndView("paymentUpdate");
		}
		else {
			httpSession.setAttribute("isValidationErrorPresentPaymentUpdate", "N");
			String paymentname = p.getName();
			String paymentmethod = p.getMethod12();
			String s1 = servint.updatepayment(p, paymentname, paymentmethod);
			List<String> paymentFromPaymentTypeName = servint.paymentFromPaymentTypeName("");
			pagedListHolder.setSource(paymentFromPaymentTypeName);
			if (s1.equalsIgnoreCase("Success")) {
				// System.out.println(s1);
				model.addAttribute("message", "Updated Successfully");
				return new ModelAndView("payment");
			} else {
				// System.out.println(s1);
				model.addAttribute("message", "Payment type name already exists.");
				return new ModelAndView("payment");
			}
		
		}
		
		

		
	}
	
	
	
	
	@RequestMapping(value = "/businesspartner")
	public ModelAndView businessOperation(@ModelAttribute("business") BusinessModal b1, Model model, HttpServletRequest req, BindingResult result) {
		ModelAndView loginModelView = new ModelAndView("Login");
		boolean isUserLoggedIn = Utilities.isUserLoggedIn(req, model, result);
		if(isUserLoggedIn == false){
			return loginModelView;
		}
		HttpSession httpSession = req.getSession(false);
		
		Integer casinoManagerId = (Integer) req.getSession().getAttribute("casinoManagerId");
		List<String> businessPartnerList = servint.checkBusinessPartnerFromusername(b1.getBusinessUserName() == null ? "" : b1.getBusinessUserName(), casinoManagerId);

		PagedListHolder pagedListHolder = new PagedListHolder(businessPartnerList);
		int page = ServletRequestUtils.getIntParameter(req, "p", 0);
		pagedListHolder.setPage(page);
		int pageSize = 10;
		pagedListHolder.setPageSize(pageSize);
		model.addAttribute("pagedListHolder", pagedListHolder);
		model.addAttribute("searchKey", b1.getBusinessUserName());

		httpSession.setAttribute("isValidationErrorPresent", "N");
		httpSession.setAttribute("isValidationErrorPresentForUpdate", "N");
		
		Map<Integer, String> statusList = new LinkedHashMap<Integer, String>();
		statusList.put(0, "Pending");
		statusList.put(1, "Confirmed");
		statusList.put(2, "Rejected");
		model.addAttribute("array1", statusList);
		
		List<String> countryNameList =(List<String>) httpSession.getAttribute("CountryNameList");
		model.addAttribute("countryList", countryNameList);
		
		Map<String, String> countryPhoneCodeList =(Map<String, String>) httpSession.getAttribute("CountryPhoneCodeList");
		model.addAttribute("countryPhoneCodeList", countryPhoneCodeList);
		
		return new ModelAndView("businessPageListing");
	}
	
	
	
	
	//Showing businessPartnerInsert page form
	@RequestMapping(value="/businessPartnerInsert")
	public ModelAndView businessPartnerInsert(@ModelAttribute("business") BusinessModal businessPartnerModel, BindingResult result, Model model, HttpServletRequest req)
	{   
		
		ModelAndView loginModelView = new ModelAndView("Login");
		boolean isUserLoggedIn = Utilities.isUserLoggedIn(req, model, result);
		if(isUserLoggedIn == false){
			return loginModelView;
		}
        HttpSession httpSession = req.getSession(false);
		Integer casinoManagerId = (Integer) req.getSession().getAttribute("casinoManagerId");
		Map<Integer, String> statusList = new LinkedHashMap<Integer, String>();
		statusList.put(0, "Pending");
		statusList.put(1, "Confirmed");
		statusList.put(2, "Rejected");
		model.addAttribute("array1", statusList);
		
		List<String> countryNameList =(List<String>) httpSession.getAttribute("CountryNameList");
		model.addAttribute("countryList", countryNameList);
		
		Map<String, String> countryPhoneCodeList =(Map<String, String>) httpSession.getAttribute("CountryPhoneCodeList");
		model.addAttribute("countryPhoneCodeList", countryPhoneCodeList);
		
		return new ModelAndView("businessPartnerInsert");
	}
	
	//Showing businessPartnerUpdate page form
	@RequestMapping(value="/businessPartnerUpdate")
	public ModelAndView businessPartnerUpdate(@ModelAttribute("business") BusinessModal businessPartnerModel, BindingResult result, Model model, HttpServletRequest req)
	{
		//System.out.println("inside business partner update form");
		ModelAndView loginModelView = new ModelAndView("Login");
		boolean isUserLoggedIn = Utilities.isUserLoggedIn(req, model, result);
		if(isUserLoggedIn == false){
			return loginModelView;
		}
		HttpSession httpSession = req.getSession(false);
		Map<Integer, String> statusList = new LinkedHashMap<Integer, String>();
		statusList.put(0, "Pending");
		statusList.put(1, "Confirmed");
		statusList.put(2, "Rejected");
		model.addAttribute("array1", statusList);
		
		List<String> countryNameList =(List<String>) httpSession.getAttribute("CountryNameList");
		model.addAttribute("countryList", countryNameList);
		
		Map<String, String> countryPhoneCodeList =(Map<String, String>) httpSession.getAttribute("CountryPhoneCodeList");
		model.addAttribute("countryPhoneCodeList", countryPhoneCodeList);
		return new ModelAndView("businessPartnerUpdate");
	}
	//insertBusinessPartner
	
	@RequestMapping(value="/insertBusinessPartner")
	public ModelAndView insertBusinessPartner(@ModelAttribute("business") BusinessModal businessPartnerModel, BindingResult result, Model model, HttpServletRequest req)
	{
		ModelAndView loginModelView = new ModelAndView("Login");
		boolean isUserLoggedIn = Utilities.isUserLoggedIn(req, model, result);
		if(isUserLoggedIn == false){
			return loginModelView;
		}
		HttpSession httpSession = req.getSession(false);
		
		Integer casinoManagerId = (Integer) req.getSession().getAttribute("casinoManagerId");
		List<String> businessPartnerList = new ArrayList<String>();
		Map<Integer, String> statusList = new LinkedHashMap<Integer, String>();
		statusList.put(0, "Pending");
		statusList.put(1, "Confirmed");
		statusList.put(2, "Rejected");
		model.addAttribute("array1", statusList);
		
		List<String> countryNameList =(List<String>) httpSession.getAttribute("CountryNameList");
		model.addAttribute("countryList", countryNameList);
		
		Map<String, String> countryPhoneCodeList =(Map<String, String>) httpSession.getAttribute("CountryPhoneCodeList");
		model.addAttribute("countryPhoneCodeList", countryPhoneCodeList);
		
		BusinessFormValidation b1validation = new BusinessFormValidation();
		b1validation.validate(businessPartnerModel, result);
		
		if (result.hasErrors()) {

			httpSession.setAttribute("isValidationErrorPresent", "Y");
			return new ModelAndView("businessPartnerInsert");
		} else {
			httpSession.setAttribute("isValidationErrorPresent", "N");

			businessPartnerModel.setCasinoManagerId(casinoManagerId);
			
			String username = businessPartnerModel.getUsername();
			String password = businessPartnerModel.getPassword();
			String passwordConfirmation = businessPartnerModel.getConpassword();
			String s1 = servint.insertbusiness(businessPartnerModel, username);
             
			if (s1.equalsIgnoreCase("Success")) {
				
				PagedListHolder pagedListHolder = new PagedListHolder(businessPartnerList);
				int page = ServletRequestUtils.getIntParameter(req, "p", 0);
				pagedListHolder.setPage(page);
				int pageSize = 10;
				pagedListHolder.setPageSize(pageSize);
				model.addAttribute("pagedListHolder", pagedListHolder);
				businessPartnerList = servint.checkBusinessPartnerFromusername("", casinoManagerId);
				pagedListHolder.setSource(businessPartnerList);
				return new ModelAndView("businessPageListing");
			} else {
				httpSession.setAttribute("isValidationErrorPresent", "Y");
				businessPartnerModel.setPassword(password);
				businessPartnerModel.setPassword(passwordConfirmation);
				model.addAttribute("message", messageSource.getMessage("error.user.name.already.exists", null, RequestContextUtils.getLocale(req)));
				return new ModelAndView("businessPartnerInsert");
			}
		}

	}
	
	//Business Partner Update Operation
	//updateBusinessPartner
	@RequestMapping(value="/updateBusinessPartner")
	public ModelAndView updateBusinessPartner(@ModelAttribute("business") BusinessModal businessPartnerModel, BindingResult result, Model model, HttpServletRequest req)
	{
		//System.out.println("updateBusinessPartner operation ");
		ModelAndView loginModelView = new ModelAndView("Login");
		boolean isUserLoggedIn = Utilities.isUserLoggedIn(req, model, result);
		if(isUserLoggedIn == false){
			return loginModelView;
		}
		HttpSession httpSession = req.getSession(false);

		Integer casinoManagerId = (Integer) req.getSession().getAttribute("casinoManagerId");
		List<String> businessPartnerList = new ArrayList<String>();

		PagedListHolder pagedListHolder = new PagedListHolder(businessPartnerList);
		int page = ServletRequestUtils.getIntParameter(req, "p", 0);
		pagedListHolder.setPage(page);
		int pageSize = 10;
		pagedListHolder.setPageSize(pageSize);
		model.addAttribute("pagedListHolder", pagedListHolder);
		Map<Integer, String> statusList = new LinkedHashMap<Integer, String>();
		statusList.put(0, "Pending");
		statusList.put(1, "Confirmed");
		statusList.put(2, "Rejected");
		model.addAttribute("array1", statusList);
		List<String> countryNameList =(List<String>) httpSession.getAttribute("CountryNameList");
		model.addAttribute("countryList", countryNameList);
		Map<String, String> countryPhoneCodeList =(Map<String, String>) httpSession.getAttribute("CountryPhoneCodeList");
		model.addAttribute("countryPhoneCodeList", countryPhoneCodeList);
		businessPartnerModel.setIsUpdate(true);
		BusinessFormValidation b1validation = new BusinessFormValidation();
		b1validation.validate(businessPartnerModel, result);
		String s1 ="";
		String password = businessPartnerModel.getPassword();
		String passwordConfirmation = businessPartnerModel.getConpassword();
		if (result.hasErrors()) {
			httpSession.setAttribute("isValidationErrorPresentForUpdate", "Y");
			return new ModelAndView("businessPartnerUpdate");
		}else{
			httpSession.setAttribute("isValidationErrorPresentForUpdate", "N");
			String username = businessPartnerModel.getUsername();
			s1 = servint.updatebusiness(businessPartnerModel, username);
		}
		if (s1.equalsIgnoreCase("Success")) {
			businessPartnerList = servint.checkBusinessPartnerFromusername("", casinoManagerId);
			pagedListHolder.setSource(businessPartnerList);
			model.addAttribute("message", "Updated Successfully");
			return new ModelAndView("businessPageListing");
		} else {
			httpSession.setAttribute("isValidationErrorPresentForUpdate", "Y");
			businessPartnerModel.setPassword(password);
			businessPartnerModel.setPassword(passwordConfirmation);
			model.addAttribute("message", messageSource.getMessage("error.user.name.already.exists", null, RequestContextUtils.getLocale(req)));
			return new ModelAndView("businessPartnerUpdate");
		}

	}
	//Deposit to casino to new  insert form jsp
	
	@RequestMapping(value="/depositToCasinoInsert")
	public ModelAndView depositToCasinoInsert(@ModelAttribute("d1") BalancesModel d1, Model model,HttpServletRequest req)
	{
		HttpSession httpSession = req.getSession(false);
		Map<Integer, String> statusList = new LinkedHashMap<Integer, String>();
		statusList.put(0, "Pending");
		statusList.put(1, "Confirmed");
		statusList.put(2, "Rejected");
		Map<Integer, String> businessPartnerIdValueMap = servint.getBusinessPartnerIdValueMap();
		Map<Integer, String> currencyIdValueMap = servint.getCurrencyIdValueMap();
		Map<Integer, String> paymentTypeIdValueMap = servint.getPaymentTypeIdValueMap();
		Integer casinoManagerId = (Integer) req.getSession().getAttribute("casinoManagerId");
		model.addAttribute("depositebusinesspartner", businessPartnerIdValueMap);
		model.addAttribute("deposittostatus", statusList);
		model.addAttribute("depositcurrency", currencyIdValueMap);
		model.addAttribute("depositpaymentvalues", paymentTypeIdValueMap);
	    return new ModelAndView("depositToCasinoInsert");	
	}
	
	
	//Deposit to casino insert operation form jsp
	
	@RequestMapping(value="/insertDepositToCasino")
	public ModelAndView insertDepositToCasino(@ModelAttribute("d1") BalancesModel d1, Model model,HttpServletRequest req,BindingResult result)
	{
		ModelAndView loginModelView = new ModelAndView("Login");
		boolean isUserLoggedIn = Utilities.isUserLoggedIn(req, model, result);
		if(isUserLoggedIn == false){
			return loginModelView;
		}
		HttpSession httpSession = req.getSession(false);
		Integer casinoManagerId = (Integer) httpSession.getAttribute("casinoManagerId");
		Map<Integer, String> statusList = new LinkedHashMap<Integer, String>();
		statusList.put(0, "Pending");
		statusList.put(1, "Confirmed");
		statusList.put(2, "Rejected");
		Map<Integer, String> businessPartnerIdValueMap = servint.getBusinessPartnerIdValueMap();
		Map<Integer, String> currencyIdValueMap = servint.getCurrencyIdValueMap();
		Map<Integer, String> paymentTypeIdValueMap = servint.getPaymentTypeIdValueMap();
		model.addAttribute("depositebusinesspartner", businessPartnerIdValueMap);
		model.addAttribute("deposittostatus", statusList);
		model.addAttribute("depositcurrency", currencyIdValueMap);
		model.addAttribute("depositpaymentvalues", paymentTypeIdValueMap);

		DeposittoCasinoValidations deposit = new DeposittoCasinoValidations();
		deposit.validate(d1, result);

		if (result.hasErrors()) {

			httpSession = req.getSession();
			httpSession.setAttribute("isValidationErrorPresentDeposit", "Y");
			return new ModelAndView("depositToCasinoInsert");
		} else {
			
			httpSession = req.getSession();
			httpSession.setAttribute("isValidationErrorPresentDeposit", "N");
			d1.setCasinoManagerId(casinoManagerId);
			d1.setAmount(Double.parseDouble(d1.getAmountStr()));
			@SuppressWarnings("unused")
			String s1 = servint.insertdepositto(d1);
			//depositToCasinoFromUserNameValues = servint.depositToCasinoFromUserNameValues(casinoManagerId, "");
			List<String> depositToCasinoFromUserNameValues = servint.depositToCasinoFromUserNameValues(casinoManagerId, d1.getBusinessUserName() == null ? "" : d1.getBusinessUserName());
			PagedListHolder pagedListHolder = new PagedListHolder(depositToCasinoFromUserNameValues);
			int page = ServletRequestUtils.getIntParameter(req, "p", 0);
			pagedListHolder.setPage(page);
			int pageSize = 10;
			pagedListHolder.setPageSize(pageSize);
			model.addAttribute("pagedListHolder", pagedListHolder);
			return new ModelAndView("depositToCasinoListing");	
		
		}	
	}
	
	
	//Deposit to Casino update form jsp
	
	@RequestMapping(value="/depositToCasinoUpdate")
	public ModelAndView depositToCasinoUpdate(@ModelAttribute("d1") BalancesModel d1, Model model,HttpServletRequest req)
	{
		HttpSession httpSession = req.getSession(false);
		Map<Integer, String> statusList = new LinkedHashMap<Integer, String>();
		statusList.put(0, "Pending");
		statusList.put(1, "Confirmed");
		statusList.put(2, "Rejected");

		Map<Integer, String> businessPartnerIdValueMap = servint.getBusinessPartnerIdValueMap();
		Map<Integer, String> currencyIdValueMap = servint.getCurrencyIdValueMap();
		Map<Integer, String> paymentTypeIdValueMap = servint.getPaymentTypeIdValueMap();
		Integer casinoManagerId = (Integer) req.getSession().getAttribute("casinoManagerId");
		model.addAttribute("depositebusinesspartner", businessPartnerIdValueMap);
		model.addAttribute("deposittostatus", statusList);
		model.addAttribute("depositcurrency", currencyIdValueMap);
		model.addAttribute("depositpaymentvalues", paymentTypeIdValueMap);
		return new ModelAndView("updateDepositToCasino");
	}
	
	// Update operation in Deposit to Casino
	
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/updateDepositToCasino")
	public ModelAndView updateDepositToCasino(@ModelAttribute("d1") BalancesModel d1, Model model,HttpServletRequest req,BindingResult result)
	{
		ModelAndView loginModelView = new ModelAndView("Login");
		boolean isUserLoggedIn = Utilities.isUserLoggedIn(req, model, result);
		if(isUserLoggedIn == false){
			return loginModelView;
		}
		HttpSession httpSession = req.getSession(false);
		
		Integer casinoManagerId = (Integer) httpSession.getAttribute("casinoManagerId");
		Map<Integer, String> statusList = new LinkedHashMap<Integer, String>();
		statusList.put(0, "Pending");
		statusList.put(1, "Confirmed");
		statusList.put(2, "Rejected");
		Map<Integer, String> businessPartnerIdValueMap = servint.getBusinessPartnerIdValueMap();
		Map<Integer, String> currencyIdValueMap = servint.getCurrencyIdValueMap();
		Map<Integer, String> paymentTypeIdValueMap = servint.getPaymentTypeIdValueMap();
		model.addAttribute("depositebusinesspartner", businessPartnerIdValueMap);
		model.addAttribute("deposittostatus", statusList);
		model.addAttribute("depositcurrency", currencyIdValueMap);
		model.addAttribute("depositpaymentvalues", paymentTypeIdValueMap);
		DeposittoCasinoValidations deposit = new DeposittoCasinoValidations();
		deposit.validate(d1, result);
		if (result.hasErrors()) {
			httpSession.setAttribute("isValidationErrorPresentDepositForUpdate", "Y");
			return new ModelAndView("updateDepositToCasino");
		}else{
			httpSession = req.getSession();
			httpSession.setAttribute("isValidationErrorPresentDepositForUpdate", "N");
			
			d1.setCasinoManagerId(casinoManagerId);
			d1.setAmount(Double.parseDouble(d1.getAmountStr()));
			String s1 = servint.updatedepositto(d1);
			List<String> depositToCasinoFromUserNameValues = servint.depositToCasinoFromUserNameValues(casinoManagerId, d1.getBusinessUserName() == null ? "" : d1.getBusinessUserName());
			@SuppressWarnings("rawtypes")
			PagedListHolder pagedListHolder = new PagedListHolder(depositToCasinoFromUserNameValues);
			int page = ServletRequestUtils.getIntParameter(req, "p", 0);
			pagedListHolder.setPage(page);
			int pageSize = 10;
			pagedListHolder.setPageSize(pageSize);
			model.addAttribute("pagedListHolder", pagedListHolder);
			depositToCasinoFromUserNameValues = servint.depositToCasinoFromUserNameValues(casinoManagerId, "");
			pagedListHolder.setSource(depositToCasinoFromUserNameValues);
			model.addAttribute("message", "Updated Successfully");
			return new ModelAndView("depositToCasinoListing");
		}
	
	}

	
	 // displaying admin email page
	@RequestMapping(value="/adminEmail")
	public ModelAndView adminEmail(@ModelAttribute("adminEmailCommandName")AdminEmailModel adminemail,Model model,HttpServletRequest req,BindingResult result)
	{
		ModelAndView loginModelView = new ModelAndView("Login");
		boolean isUserLoggedIn = Utilities.isUserLoggedIn(req, model, result);
		if(isUserLoggedIn == false){
			return loginModelView;
		}
		return new ModelAndView("adminEmail");
	}
	
	//insertion operation on admin email
	@RequestMapping(value="/insertAdminEmail")
	public ModelAndView adminEmailInsert(@ModelAttribute("adminEmailCommandName")AdminEmailModel adminemail,Model model,HttpServletRequest req,BindingResult result)
	{
		ModelAndView loginModelView = new ModelAndView("Login");
		boolean isUserLoggedIn = Utilities.isUserLoggedIn(req, model, result);
		if(isUserLoggedIn == false){
			return loginModelView;
		}
		AdminEmailValidation admin = new AdminEmailValidation();
		admin.validate(adminemail, result);
		if (!result.hasErrors()) 
		{
			String s1 = servint.setAdminEmail(adminemail);
		}
		return new ModelAndView("adminEmail");
		
	}
	
	@RequestMapping(value = "/validate", params = "loginbutton")
	public ModelAndView validatLogin(@ModelAttribute("go") Modelbean m1, @RequestParam(value = "logout", required = false) String logout ,
			Model model, HttpServletRequest req, BindingResult result) {

		ModelAndView loginModelView = new ModelAndView("Login");

		List<Integer> Loginlist=new ArrayList<Integer>();
		Loginlist = servint.loginList(m1.getUsername(),m1.getPassword());
		

		if (Loginlist.isEmpty()) {
			result.rejectValue("", "error.invalidLogin", "Incorrect User Name or Password");
			//System.out.println("Username and password is Not there");
			m1.setUsername("");
			m1.setPassword("");
	    	HttpSession indexPageSession = req.getSession(true);
	    	indexPageSession.setAttribute("indexPage", "login");
	    	
			return loginModelView;
		} else {
			//System.out.println("inside if condition");
			HttpSession httpSession=req.getSession(true);
			httpSession.setAttribute("username",m1.getUsername());
			httpSession.setAttribute("casinoUser", m1);
			Integer casinoManagerId = (Integer) Loginlist.get(0);
			httpSession.setAttribute("casinoManagerId", casinoManagerId);
			
			List<String> countryNameList = servint.getCountryNameList();
			Map<String, String>  countryPhoneCodeList = servint.getCountryPhoneCodeMap();

			httpSession.setAttribute("CountryNameList", countryNameList);
			httpSession.setAttribute("CountryPhoneCodeList", countryPhoneCodeList);
			
			Map<Integer, String> depositTypeList = new LinkedHashMap<Integer, String>();
			depositTypeList.put(Constants.DEPOSIT_AMOUNT_APPROVAL, "Deposit");
			depositTypeList.put(Constants.CREDIT_AMOUNT_APPROVAL, "Credit");
			
			httpSession.setAttribute("depositTypeList", depositTypeList);
			
			//System.out.println("Username and password is there");
			return new ModelAndView("Welcome");
		}

	}

	// **********************************Businesss operation
	// ************************************************

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/business", method = RequestMethod.GET)
	@ResponseBody
	public List ajaxbusiness(@ModelAttribute("business") BusinessModal b1, Model model) {
		// System.out.println("ajax values");
		List<String> Businesslist = servint.showdbusiness(b1);
		// System.out.println("action class casinomanager list = "+currencyvalues);
		return Businesslist;
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/businessallvalues", method = RequestMethod.GET)
	@ResponseBody
	public List ajaxbusinessallvalues(@ModelAttribute("business") BusinessModal b1, Model model, int id) {
		// System.out.println("ajax values");
		// System.out.println("inside business insert operation");
		//System.out.println(" id values = " + id);
		List<String> businessallvalues = servint.businessallvalues(id);
		return businessallvalues;
	}

	
/*
	@RequestMapping(value = "/businessoper", params = "create")
	public ModelAndView businessoperationinsert(@ModelAttribute("business") BusinessModal businessPartnerModel, BindingResult result, Model model, HttpServletRequest req) {

		ModelAndView loginModelView = new ModelAndView("Login");
		boolean isUserLoggedIn = Utilities.isUserLoggedIn(req, model, result);
		if(isUserLoggedIn == false){
			return loginModelView;
		}
		HttpSession httpSession = req.getSession(false);
		
		Integer casinoManagerId = (Integer) req.getSession().getAttribute("casinoManagerId");
		List<String> businessPartnerList = new ArrayList<String>();

		PagedListHolder pagedListHolder = new PagedListHolder(businessPartnerList);
		int page = ServletRequestUtils.getIntParameter(req, "p", 0);
		pagedListHolder.setPage(page);
		int pageSize = 10;
		pagedListHolder.setPageSize(pageSize);
		model.addAttribute("pagedListHolder", pagedListHolder);
		
		Map<Integer, String> statusList = new LinkedHashMap<Integer, String>();
		statusList.put(0, "Pending");
		statusList.put(1, "Confirmed");
		statusList.put(2, "Rejected");
		model.addAttribute("array1", statusList);
		
		List<String> countryNameList =(List<String>) httpSession.getAttribute("CountryNameList");
		model.addAttribute("countryList", countryNameList);
		
		Map<String, String> countryPhoneCodeList =(Map<String, String>) httpSession.getAttribute("CountryPhoneCodeList");
		model.addAttribute("countryPhoneCodeList", countryPhoneCodeList);
		
		BusinessFormValidation b1validation = new BusinessFormValidation();
		b1validation.validate(businessPartnerModel, result);
		
		if (result.hasErrors()) {

			httpSession.setAttribute("isValidationErrorPresent", "Y");
			return new ModelAndView("business");
		} else {
			httpSession.setAttribute("isValidationErrorPresent", "N");

			businessPartnerModel.setCasinoManagerId(casinoManagerId);
			
			String username = businessPartnerModel.getUsername();
			
			String password = businessPartnerModel.getPassword();
			String passwordConfirmation = businessPartnerModel.getConpassword();
			
			String s1 = servint.insertbusiness(businessPartnerModel, username);

			if (s1.equalsIgnoreCase("Success")) {

				businessPartnerList = servint.checkBusinessPartnerFromusername("", casinoManagerId);
				pagedListHolder.setSource(businessPartnerList);
				return new ModelAndView("business");
			} else {
				httpSession.setAttribute("isValidationErrorPresent", "Y");
				businessPartnerModel.setPassword(password);
				businessPartnerModel.setPassword(passwordConfirmation);
				model.addAttribute("message", messageSource.getMessage("error.user.name.already.exists", null, RequestContextUtils.getLocale(req)));
				return new ModelAndView("business");
			}
		}
	}*/

/*	@RequestMapping(value = "/businessoper", params = "update")
	public ModelAndView businessoperationupdate(@ModelAttribute("business") BusinessModal b1, Model model, BindingResult result, HttpServletRequest req) {

		ModelAndView loginModelView = new ModelAndView("Login");
		boolean isUserLoggedIn = Utilities.isUserLoggedIn(req, model, result);
		if(isUserLoggedIn == false){
			return loginModelView;
		}
		
		HttpSession httpSession = req.getSession(false);

		Integer casinoManagerId = (Integer) req.getSession().getAttribute("casinoManagerId");
		List<String> businessPartnerList = new ArrayList<String>();

		PagedListHolder pagedListHolder = new PagedListHolder(businessPartnerList);
		int page = ServletRequestUtils.getIntParameter(req, "p", 0);
		pagedListHolder.setPage(page);
		int pageSize = 10;
		pagedListHolder.setPageSize(pageSize);
		model.addAttribute("pagedListHolder", pagedListHolder);
		
		Map<Integer, String> statusList = new LinkedHashMap<Integer, String>();
		statusList.put(0, "Pending");
		statusList.put(1, "Confirmed");
		statusList.put(2, "Rejected");
		model.addAttribute("array1", statusList);
		
		List<String> countryNameList =(List<String>) httpSession.getAttribute("CountryNameList");
		model.addAttribute("countryList", countryNameList);
		
		Map<String, String> countryPhoneCodeList =(Map<String, String>) httpSession.getAttribute("CountryPhoneCodeList");
		model.addAttribute("countryPhoneCodeList", countryPhoneCodeList);

		
		b1.setIsUpdate(true);
		BusinessFormValidation b1validation = new BusinessFormValidation();
		b1validation.validate(b1, result);
		String s1 ="";
		
		String password = b1.getPassword();
		String passwordConfirmation = b1.getConpassword();
		
		if (result.hasErrors()) {
			httpSession.setAttribute("isValidationErrorPresentForUpdate", "Y");
			return new ModelAndView("business");
		}else{
			httpSession.setAttribute("isValidationErrorPresentForUpdate", "N");
			String username = b1.getUsername();
			s1 = servint.updatebusiness(b1, username);
		}
		
		
		if (s1.equalsIgnoreCase("Success")) {
			businessPartnerList = servint.checkBusinessPartnerFromusername("", casinoManagerId);
			pagedListHolder.setSource(businessPartnerList);
			model.addAttribute("message", "Updated Successfully");
			return new ModelAndView("business");
		} else {
			httpSession.setAttribute("isValidationErrorPresentForUpdate", "Y");
			b1.setPassword(password);
			b1.setPassword(passwordConfirmation);
			model.addAttribute("message", messageSource.getMessage("error.user.name.already.exists", null, RequestContextUtils.getLocale(req)));
			return new ModelAndView("business");
		}
	}*/

	@RequestMapping(value = "/businessoper", params = "delete")
	public ModelAndView businessoperationdelete(@ModelAttribute("business") BusinessModal b1, Model model, BindingResult result, HttpServletRequest req) {

		ModelAndView loginModelView = new ModelAndView("Login");
		boolean isUserLoggedIn = Utilities.isUserLoggedIn(req, model, result);
		if(isUserLoggedIn == false){
			return loginModelView;
		}
		
		@SuppressWarnings("unused")
		String s1 = servint.deletebusiness(b1);

		Map<Integer, String> statusList = new LinkedHashMap<Integer, String>();
		statusList.put(0, "Pending");
		statusList.put(1, "Confirmed");
		statusList.put(2, "Rejected");
		model.addAttribute("array1", statusList);

		return new ModelAndView("business");
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/businessCasinoId", method = RequestMethod.GET)
	@ResponseBody
	public List BusinessCasinoIdvalues(@ModelAttribute("business") BusinessModal b1, Model model, HttpServletRequest req) {

		String usname = (String) req.getSession().getAttribute("username");

		List<String> businessCasinoId = servint.getcasinoid(usname);
		return businessCasinoId;
	}

	
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/checkBusinessPartnerFromUsername", method = RequestMethod.GET)
	@ResponseBody
	public List checkBusinessPartnerFromUsername(@ModelAttribute("business") BusinessModal b1, Model model, HttpServletRequest req, String businessUserName) {

		List <String>checkBusinessPartnerFromusername = new ArrayList<String>();
		HttpSession httpSession=req.getSession(false);
		Integer casinoManagerId = (Integer) req.getSession().getAttribute("casinoManagerId");
	
		checkBusinessPartnerFromusername=servint.checkBusinessPartnerFromusername(businessUserName,casinoManagerId);
	
		return checkBusinessPartnerFromusername;
	}
	
	
	
	
	
	// deletebusinessValuesId

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/deletebusinessValuesId", method = RequestMethod.GET)
	@ResponseBody
	public List deletebusinessValuesId(@ModelAttribute("d1") BalancesModel d1, Model model, int id) {
		String s1 = servint.deletebusinessValuesId(id);
		//System.out.println("Action class s1 value = " + s1);
		if (s1.equalsIgnoreCase("success")) {
			List<String> deletebusinessValuesId = new ArrayList<String>();
			deletebusinessValuesId.removeAll(deletebusinessValuesId);
			deletebusinessValuesId.add("Success");
			//System.out.println(deletebusinessValuesId);
			return deletebusinessValuesId;
		} else {
			return null;
		}
	}

	// **********************deposite to casino crude operations*************************

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/deposittocasinovalues", method = RequestMethod.GET)
	@ResponseBody
	public List ajaxdeposittocasino(@ModelAttribute("d1") BalancesModel d1, Model model) {
		// System.out.println("ajax values");
		List<String> deposittocasinovalues = servint.showdepositto(d1);
		// System.out.println("action class casinomanager list = "+currencyvalues);
		return deposittocasinovalues;
	}

	//

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/deposittocasinovaluesallvalues", method = RequestMethod.GET)
	@ResponseBody
	public List ajaxdeposittocasinoallvalues(@ModelAttribute("d1") BalancesModel d1, Model model, int id) {
		//System.out.println("ajax values id = " + id);
		List<String> depositpaymentallvalues = servint.showdeposittocasinoallvalues(id);
		// System.out.println("action class casinomanager list = "+currencyvalues);
		return depositpaymentallvalues;
	}
	
	
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/depositToCasinoFromUserNameValues", method = RequestMethod.GET)
	@ResponseBody
	public List depositToCasinoFromUserNameValues(String businessUserName,HttpServletRequest req) {
		
		HttpSession httpSession=req.getSession(false);
		Integer casinoManagerId = (Integer) httpSession.getAttribute("casinoManagerId");
		//System.out.println("Controller businessUserName = "+businessUserName);
		List<String> depositToCasinoFromUserNameValues = servint.depositToCasinoFromUserNameValues(casinoManagerId,businessUserName);
		 //System.out.println("action class depositToCasinoFromUserNameValues list = "+depositToCasinoFromUserNameValues);
		return depositToCasinoFromUserNameValues;
	}

	
	
	
	@RequestMapping(value = "/depositcasino")
	public ModelAndView Depositetocasino(@ModelAttribute("d1") BalancesModel d1, Model model, BindingResult result, HttpServletRequest req) {

		ModelAndView loginModelView = new ModelAndView("Login");
		boolean isUserLoggedIn = Utilities.isUserLoggedIn(req, model, result);
		if(isUserLoggedIn == false){
			return loginModelView;
		}
		
		HttpSession httpSession = req.getSession(false);
		Integer casinoManagerId = (Integer) httpSession.getAttribute("casinoManagerId");
		
		List<String> depositToCasinoFromUserNameValues = servint.depositToCasinoFromUserNameValues(casinoManagerId, d1.getBusinessUserName() == null ? "" : d1.getBusinessUserName());

		PagedListHolder pagedListHolder = new PagedListHolder(depositToCasinoFromUserNameValues);
		int page = ServletRequestUtils.getIntParameter(req, "p", 0);
		pagedListHolder.setPage(page);
		int pageSize = 10;
		pagedListHolder.setPageSize(pageSize);
		model.addAttribute("pagedListHolder", pagedListHolder);
		model.addAttribute("searchKey", d1.getBusinessUserName());
		httpSession.setAttribute("isValidationErrorPresentDeposit", "N");
		httpSession.setAttribute("isValidationErrorPresentDepositForUpdate", "N");

		Map<Integer, String> statusList = new LinkedHashMap<Integer, String>();
		statusList.put(0, "Pending");
		statusList.put(1, "Confirmed");
		statusList.put(2, "Rejected");

		Map<Integer, String> businessPartnerIdValueMap = servint.getBusinessPartnerIdValueMap();
		Map<Integer, String> currencyIdValueMap = servint.getCurrencyIdValueMap();
		Map<Integer, String> paymentTypeIdValueMap = servint.getPaymentTypeIdValueMap();

		model.addAttribute("depositebusinesspartner", businessPartnerIdValueMap);
		model.addAttribute("deposittostatus", statusList);
		model.addAttribute("depositcurrency", currencyIdValueMap);
		model.addAttribute("depositpaymentvalues", paymentTypeIdValueMap);
		return new ModelAndView("depositToCasinoPageListing");
	}

/*	@RequestMapping(value = "/deposittocasinooper", params = "logindeposit")
	public ModelAndView Depositetocasinooperation_insert(@ModelAttribute("d1") BalancesModel d1, Model model, BindingResult result, HttpSession httpSession,
			HttpServletRequest req) {
		
		ModelAndView loginModelView = new ModelAndView("Login");
		boolean isUserLoggedIn = Utilities.isUserLoggedIn(req, model, result);
		if(isUserLoggedIn == false){
			return loginModelView;
		}

		Integer casinoManagerId = (Integer) httpSession.getAttribute("casinoManagerId");
		
		List<String> depositToCasinoFromUserNameValues = new ArrayList<String>();

		PagedListHolder pagedListHolder = new PagedListHolder(depositToCasinoFromUserNameValues);
		int page = ServletRequestUtils.getIntParameter(req, "p", 0);
		pagedListHolder.setPage(page);
		int pageSize = 10;
		pagedListHolder.setPageSize(pageSize);
		model.addAttribute("pagedListHolder", pagedListHolder);
		
		Map<Integer, String> statusList = new LinkedHashMap<Integer, String>();
		statusList.put(0, "Pending");
		statusList.put(1, "Confirmed");
		statusList.put(2, "Rejected");

		Map<Integer, String> businessPartnerIdValueMap = servint.getBusinessPartnerIdValueMap();
		Map<Integer, String> currencyIdValueMap = servint.getCurrencyIdValueMap();
		Map<Integer, String> paymentTypeIdValueMap = servint.getPaymentTypeIdValueMap();

		model.addAttribute("depositebusinesspartner", businessPartnerIdValueMap);
		model.addAttribute("deposittostatus", statusList);
		model.addAttribute("depositcurrency", currencyIdValueMap);
		model.addAttribute("depositpaymentvalues", paymentTypeIdValueMap);

		DeposittoCasinoValidations deposit = new DeposittoCasinoValidations();
		deposit.validate(d1, result);

		if (result.hasErrors()) {

			httpSession = req.getSession();
			httpSession.setAttribute("isValidationErrorPresentDeposit", "Y");
			return new ModelAndView("deposite");
		} else {
			httpSession = req.getSession();
			httpSession.setAttribute("isValidationErrorPresentDeposit", "N");
			d1.setCasinoManagerId(casinoManagerId);
			d1.setAmount(Double.parseDouble(d1.getAmountStr()));
			@SuppressWarnings("unused")
			String s1 = servint.insertdepositto(d1);
			
			depositToCasinoFromUserNameValues = servint.depositToCasinoFromUserNameValues(casinoManagerId, "");
			pagedListHolder.setSource(depositToCasinoFromUserNameValues);
			return new ModelAndView("deposite");
		}
	}*/

	/*@RequestMapping(value = "/deposittocasinooper", params = "updatedeposit")
	public ModelAndView Updatetocasinooperation_insert(@ModelAttribute("d1") BalancesModel d1, Model model, BindingResult result, HttpServletRequest req) {
		
		ModelAndView loginModelView = new ModelAndView("Login");
		boolean isUserLoggedIn = Utilities.isUserLoggedIn(req, model, result);
		if(isUserLoggedIn == false){
			return loginModelView;
		}
		HttpSession httpSession = req.getSession(false);
		
		Integer casinoManagerId = (Integer) httpSession.getAttribute("casinoManagerId");
		List<String> depositToCasinoFromUserNameValues = new ArrayList<String>();

		PagedListHolder pagedListHolder = new PagedListHolder(depositToCasinoFromUserNameValues);
		int page = ServletRequestUtils.getIntParameter(req, "p", 0);
		pagedListHolder.setPage(page);
		int pageSize = 10;
		pagedListHolder.setPageSize(pageSize);
		model.addAttribute("pagedListHolder", pagedListHolder);
		
		Map<Integer, String> statusList = new LinkedHashMap<Integer, String>();
		statusList.put(0, "Pending");
		statusList.put(1, "Confirmed");
		statusList.put(2, "Rejected");

		Map<Integer, String> businessPartnerIdValueMap = servint.getBusinessPartnerIdValueMap();
		Map<Integer, String> currencyIdValueMap = servint.getCurrencyIdValueMap();
		Map<Integer, String> paymentTypeIdValueMap = servint.getPaymentTypeIdValueMap();

		model.addAttribute("depositebusinesspartner", businessPartnerIdValueMap);
		model.addAttribute("deposittostatus", statusList);
		model.addAttribute("depositcurrency", currencyIdValueMap);
		model.addAttribute("depositpaymentvalues", paymentTypeIdValueMap);
		
		DeposittoCasinoValidations deposit = new DeposittoCasinoValidations();
		deposit.validate(d1, result);

		if (result.hasErrors()) {
			httpSession.setAttribute("isValidationErrorPresentDepositForUpdate", "Y");
			return new ModelAndView("deposite");
		}else{
			httpSession.setAttribute("isValidationErrorPresentDepositForUpdate", "N");
			d1.setCasinoManagerId(casinoManagerId);
			d1.setAmount(Double.parseDouble(d1.getAmountStr()));
			String s1 = servint.updatedepositto(d1);
			
			depositToCasinoFromUserNameValues = servint.depositToCasinoFromUserNameValues(casinoManagerId, "");
			pagedListHolder.setSource(depositToCasinoFromUserNameValues);
			
			model.addAttribute("message", "Updated Successfully");
			return new ModelAndView("deposite");
		}

	}
*/
	@RequestMapping(value = "/deposittocasinooper", params = "deletedeposit")
	public ModelAndView deletetocasinooperation_insert(@ModelAttribute("d1") BalancesModel d1, Model model, BindingResult result, HttpServletRequest req) {
		
		ModelAndView loginModelView = new ModelAndView("Login");
		boolean isUserLoggedIn = Utilities.isUserLoggedIn(req, model, result);
		if(isUserLoggedIn == false){
			return loginModelView;
		}
		
		Map<Integer, String> statusList = new LinkedHashMap<Integer, String>();
		statusList.put(0, "Pending");
		statusList.put(1, "Confirmed");
		statusList.put(2, "Rejected");

		Map<Integer, String> businessPartnerIdValueMap = servint.getBusinessPartnerIdValueMap();
		Map<Integer, String> currencyIdValueMap = servint.getCurrencyIdValueMap();
		Map<Integer, String> paymentTypeIdValueMap = servint.getPaymentTypeIdValueMap();

		model.addAttribute("depositebusinesspartner", businessPartnerIdValueMap);
		model.addAttribute("deposittostatus", statusList);
		model.addAttribute("depositcurrency", currencyIdValueMap);
		model.addAttribute("depositpaymentvalues", paymentTypeIdValueMap);
		@SuppressWarnings("unused")
		String s1 = servint.deletedepositto(d1);
		return new ModelAndView("depositToCasinoPageListing");
	}

	@RequestMapping(value = "/menudashboard")
	public ModelAndView depositopertodashboard(HttpServletRequest req) {
		
		HttpSession httpSession = req.getSession(false);
		Map<Integer, String> depositTypeList = new LinkedHashMap<Integer, String>();
		depositTypeList.put(Constants.DEPOSIT_AMOUNT_APPROVAL, "Deposit");
		depositTypeList.put(Constants.CREDIT_AMOUNT_APPROVAL, "Credit");
		httpSession.setAttribute("depositTypeList", depositTypeList);
		
		return new ModelAndView("Welcome");
	}

	//

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/deleteCasinoValuesId", method = RequestMethod.GET)
	@ResponseBody
	public List deleteCasinoValuesId(@ModelAttribute("d1") BalancesModel d1, Model model, int id) {
		String s1 = servint.deleteCasinoValues(id);
		//System.out.println("Action class s1 value = " + s1);
		if (s1.equalsIgnoreCase("success")) {
			List<String> al = new ArrayList<String>();
			al.removeAll(al);
			al.add("Success");
			//System.out.println(al);
			return al;
		} else {
			return null;
		}
	}

	// *******************************casinomanager crude operation
	// *********************************

	@RequestMapping(value = "/casinomanager")
	public ModelAndView newcasinomanager(@ModelAttribute("ncasino") CasinoManager cm, Model model, BindingResult result, HttpServletRequest req) {
		
		ModelAndView loginModelView = new ModelAndView("Login");
		boolean isUserLoggedIn = Utilities.isUserLoggedIn(req, model, result);
		if(isUserLoggedIn == false){
			return loginModelView;
		}
		
		List<String> casinoManagerValuesFromUsernames = servint.casinoManagerValuesFromUsernames(cm.getUsername()== null?"":cm.getUsername());
		PagedListHolder pagedListHolder = new PagedListHolder(casinoManagerValuesFromUsernames);
		int page = ServletRequestUtils.getIntParameter(req, "p", 0);
		pagedListHolder.setPage(page);
		int pageSize = 10;
		pagedListHolder.setPageSize(pageSize);
		model.addAttribute("pagedListHolder", pagedListHolder);
		model.addAttribute("searchKey", cm.getUsername());
		
		HttpSession httpSession = req.getSession(false);
		httpSession.setAttribute("isValidationErrorPresentCasinoManager", "N");
		httpSession.setAttribute("isValidationErrorForCasinoManagerUpdate", "N");
		
		List<String> countryNameList =(List<String>) httpSession.getAttribute("CountryNameList");
		model.addAttribute("countryList", countryNameList);
		
		Map<String, String> countryPhoneCodeList =(Map<String, String>) httpSession.getAttribute("CountryPhoneCodeList");
		model.addAttribute("countryPhoneCodeList", countryPhoneCodeList);
		
		Integer casinoManagerId = (Integer) req.getSession().getAttribute("casinoManagerId");
		model.addAttribute("casinoManagerId", casinoManagerId);
		
		return new ModelAndView("casionmanger");
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/casinomangervalues", method = RequestMethod.GET)
	@ResponseBody
	public List ajax1(@ModelAttribute("ncasino") CasinoManager cm, Model model) {
		// System.out.println("ajax values");
		List<String> casinomanager = servint.showcasino(cm);
		// System.out.println("action class casinomanager list = " +
		// casinomanager);
		return casinomanager;
	}

	//casinoManagerValuesFromUsernames
	
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/casinoManagerValuesFromUsernames", method = RequestMethod.GET)
	@ResponseBody
	public List casinoManagerValuesFromUsernames(@ModelAttribute("ncasino") CasinoManager cm, Model model,String casinoManagerUserName) {
		// System.out.println("ajax values");
		List<String> casinoManagerValuesFromUsernames = servint.casinoManagerValuesFromUsernames(casinoManagerUserName);
		//System.out.println("controller casinoManagerValuesFromUsernames = "+casinoManagerValuesFromUsernames);
		return casinoManagerValuesFromUsernames;
	}
	
	

	/*@RequestMapping(value = "/newcasinomanager", params = "logincasinomanager")
	public ModelAndView casinoinsertoper(@ModelAttribute("ncasino") CasinoManager cm, Model model, BindingResult result, HttpServletRequest req) {
		
		ModelAndView loginModelView = new ModelAndView("Login");
		boolean isUserLoggedIn = Utilities.isUserLoggedIn(req, model, result);
		if(isUserLoggedIn == false){
			return loginModelView;
		}
		
		List<String> casinoManagerValuesFromUsernames = new ArrayList<String>();
		PagedListHolder pagedListHolder = new PagedListHolder(casinoManagerValuesFromUsernames);
		int page = ServletRequestUtils.getIntParameter(req, "p", 0);
		pagedListHolder.setPage(page);
		int pageSize = 10;
		pagedListHolder.setPageSize(pageSize);
		model.addAttribute("pagedListHolder", pagedListHolder);
		
		HttpSession httpSession = req.getSession(false);
		String casinomanagerusername = cm.getUsername();
		
		List<String> countryNameList =(List<String>) httpSession.getAttribute("CountryNameList");
		model.addAttribute("countryList", countryNameList);
		
		Map<String, String> countryPhoneCodeList =(Map<String, String>) httpSession.getAttribute("CountryPhoneCodeList");
		model.addAttribute("countryPhoneCodeList", countryPhoneCodeList);
		
		CasinoFormValidation casinoform = new CasinoFormValidation();
		casinoform.validate(cm, result);
		
		if (result.hasErrors()) {

			httpSession = req.getSession();
			httpSession.setAttribute("isValidationErrorPresentCasinoManager", "Y");
			return new ModelAndView("casionmanger");
		} else {
			httpSession = req.getSession();
			httpSession.setAttribute("isValidationErrorPresentCasinoManager", "N");
			
			String password = cm.getPassword();
			String passwordConfirmation = cm.getConpassword();
			
			String s1 = servint.insertcasino(cm, casinomanagerusername);
			if (s1.equalsIgnoreCase("Success")) {
				// System.out.println(s1);
				casinoManagerValuesFromUsernames = servint.casinoManagerValuesFromUsernames("");
				pagedListHolder.setSource(casinoManagerValuesFromUsernames);
				model.addAttribute("message", "Inserted Successfully");
				return new ModelAndView("casionmanger");
			} else {
				// System.out.println(s1);
				httpSession.setAttribute("isValidationErrorPresentCasinoManager", "Y");
				cm.setPassword(password);
				cm.setPassword(passwordConfirmation);
				model.addAttribute("message", messageSource.getMessage("error.user.name.already.exists", null, RequestContextUtils.getLocale(req)));
				return new ModelAndView("casionmanger");
			}

		}

	}

*/	/*@RequestMapping(value = "/newcasinomanager", params = "updatecasinomanager")
	public ModelAndView casinoupdateoper(@ModelAttribute("ncasino") CasinoManager cm, Model model, BindingResult result, HttpServletRequest req) {
		
		ModelAndView loginModelView = new ModelAndView("Login");
		boolean isUserLoggedIn = Utilities.isUserLoggedIn(req, model, result);
		if(isUserLoggedIn == false){
			return loginModelView;
		}
		
		List<String> casinoManagerValuesFromUsernames = new ArrayList<String>();
		PagedListHolder pagedListHolder = new PagedListHolder(casinoManagerValuesFromUsernames);
		int page = ServletRequestUtils.getIntParameter(req, "p", 0);
		pagedListHolder.setPage(page);
		int pageSize = 10;
		pagedListHolder.setPageSize(pageSize);
		model.addAttribute("pagedListHolder", pagedListHolder);
		
		HttpSession httpSession = req.getSession(false);
		
		List<String> countryNameList =(List<String>) httpSession.getAttribute("CountryNameList");
		model.addAttribute("countryList", countryNameList);
		
		Map<String, String> countryPhoneCodeList =(Map<String, String>) httpSession.getAttribute("CountryPhoneCodeList");
		model.addAttribute("countryPhoneCodeList", countryPhoneCodeList);
		
		cm.setIsUpdate(true);
		CasinoFormValidation casinoform = new CasinoFormValidation();
		casinoform.validate(cm, result);
		
		if (result.hasErrors()) {
			httpSession.setAttribute("isValidationErrorForCasinoManagerUpdate", "Y");
			return new ModelAndView("casionmanger");
		} else{
			httpSession.setAttribute("isValidationErrorForCasinoManagerUpdate", "N");
			// System.out.println("casino manager update operation");
			
			String password = cm.getPassword();
			String passwordConfirmation = cm.getConpassword();
			
			String casinomanagerusername = cm.getUsername();
			String s1 = servint.updatecasino(cm, casinomanagerusername);
			if (s1.equalsIgnoreCase("Success")) {
				// System.out.println(s1);
				casinoManagerValuesFromUsernames = servint.casinoManagerValuesFromUsernames("");
				pagedListHolder.setSource(casinoManagerValuesFromUsernames);
				model.addAttribute("message", "Updated Successfully");
				return new ModelAndView("casionmanger");
			} else {
				// System.out.println(s1);
				httpSession.setAttribute("isValidationErrorForCasinoManagerUpdate", "Y");
				cm.setPassword(password);
				cm.setPassword(passwordConfirmation);
				model.addAttribute("message", messageSource.getMessage("error.user.name.already.exists", null, RequestContextUtils.getLocale(req)));
				return new ModelAndView("casionmanger");
			}
		}
	}*/

	@RequestMapping(value = "/newcasinomanager", params = "deletecasinomanager")
	public ModelAndView casinodeleteoper(@ModelAttribute("ncasino") CasinoManager cm, Model model, BindingResult result, HttpServletRequest req) {
		
		ModelAndView loginModelView = new ModelAndView("Login");
		boolean isUserLoggedIn = Utilities.isUserLoggedIn(req, model, result);
		if(isUserLoggedIn == false){
			return loginModelView;
		}
		
		HttpSession httpSession = req.getSession(false);
		
		List<String> countryNameList =(List<String>) httpSession.getAttribute("CountryNameList");
		model.addAttribute("countryList", countryNameList);
		
		Map<String, String> countryPhoneCodeList =(Map<String, String>) httpSession.getAttribute("CountryPhoneCodeList");
		model.addAttribute("countryPhoneCodeList", countryPhoneCodeList);
		
		//System.out.println("casino manager delete operation");
		String s1 = servint.deletecasino(cm);
		return new ModelAndView("casionmanger");
	}

	//

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/casinoman", method = RequestMethod.GET)
	@ResponseBody
	public List ajax2(@ModelAttribute("ncasino") CasinoManager cm, Model model, int id) {
		// System.out.println("ajax values");
		//System.out.println("id value selected = " + id);
		List<String> casinomanagerallvalues = servint.showcasinoallvalues(id);
		//System.out.println("action class casinomanager list = " + casinomanagerallvalues);
		return casinomanagerallvalues;

	}

	//
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/deleteCasinoManagerId", method = RequestMethod.GET)
	@ResponseBody
	public List deleteCasinoManagerId(int id) {
		List<String> casinoManagerId = new ArrayList<String>();

		String s1 = servint.deleteCasinoManagerId(id);
		// System.out.println("action class casinomanager list = " +
		// casinomanagerallvalues);

		if (s1.equalsIgnoreCase("success")) {
			List<String> deleteCasinoManagerId = new ArrayList<String>();
			deleteCasinoManagerId.removeAll(deleteCasinoManagerId);
			deleteCasinoManagerId.add("Success");
			//System.out.println(deleteCasinoManagerId);
			return deleteCasinoManagerId;
		} else {
			return null;
		}

	}

	// ***********************payment all crudes
	// operaions*************************************

	@RequestMapping(value = "/payment")
	public ModelAndView payment1(@ModelAttribute("paym") Payment p, Model model, BindingResult result, HttpServletRequest req) {
		
		ModelAndView loginModelView = new ModelAndView("Login");
		boolean isUserLoggedIn = Utilities.isUserLoggedIn(req, model, result);
		if(isUserLoggedIn == false){
			return loginModelView;
		}
		
		List<String> paymentFromPaymentTypeName = servint.paymentFromPaymentTypeName(p.getName() == null?"":p.getName());
		PagedListHolder pagedListHolder = new PagedListHolder(paymentFromPaymentTypeName);
		int page = ServletRequestUtils.getIntParameter(req, "p", 0);
		pagedListHolder.setPage(page);
		int pageSize = 10;
		pagedListHolder.setPageSize(pageSize);
		model.addAttribute("pagedListHolder", pagedListHolder);
		model.addAttribute("searchKey", p.getName());
		
		HttpSession httpSession = req.getSession(false);
		httpSession.setAttribute("isValidationErrorPresentPayment", "N");
		httpSession.setAttribute("isValidationErrorPresentPaymentUpdate", "N");
		return new ModelAndView("payment");
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/paymentvalues", method = RequestMethod.GET)
	@ResponseBody
	public List aj(@ModelAttribute("paym") Payment p, Model model) {
		
		List<String> paymentvaluesl = servint.showdpaymentvalues(p);
		
		return paymentvaluesl;
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/paymentallvalues", method = RequestMethod.GET)
	@ResponseBody
	public List paymentallvalues(@ModelAttribute("paym") Payment p, Model model, int id) {
		// System.out.println("ajax values");

		List<String> paymentallvalues = servint.showdpaymentvalues(id);
	
		return paymentallvalues;
	}

	//paymentFromPaymentTypeName

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/paymentFromPaymentTypeName", method = RequestMethod.GET)
	@ResponseBody
	public List paymentFromPaymentTypeName(String paymentTypeName) {
		// System.out.println("ajax values");

		List<String> paymentFromPaymentTypeName = servint.paymentFromPaymentTypeName(paymentTypeName);
	
		return paymentFromPaymentTypeName;
	}
	
	
	
	
	/*@RequestMapping(value = "/paym", params = "Insert")
	public ModelAndView payment1insert(@ModelAttribute("paym") Payment p, Model model, BindingResult result, HttpServletRequest req) {

		ModelAndView loginModelView = new ModelAndView("Login");
		boolean isUserLoggedIn = Utilities.isUserLoggedIn(req, model, result);
		if(isUserLoggedIn == false){
			return loginModelView;
		}
		
		PagedListHolder pagedListHolder = new PagedListHolder(new ArrayList<String>());
		int page = ServletRequestUtils.getIntParameter(req, "p", 0);
		pagedListHolder.setPage(page);
		int pageSize = 10;
		pagedListHolder.setPageSize(pageSize);
		model.addAttribute("pagedListHolder", pagedListHolder);
		
		PaymentFormValidations paymentvalids = new PaymentFormValidations();

		paymentvalids.validate(p, result);
		HttpSession httpSession = req.getSession(false);
		if (result.hasErrors()) {

			httpSession.setAttribute("isValidationErrorPresentPayment", "Y");
			return new ModelAndView("payment");
		}else {

			httpSession.setAttribute("isValidationErrorPresentPayment", "N");
			String paymentname = p.getName();
			String paymentmethod = p.getMethod12();
			String s1 = servint.insertpayment(p, paymentname, paymentmethod);
			if (s1.equalsIgnoreCase("Success")) {
				// System.out.println(s1);
				List<String> paymentFromPaymentTypeName = servint.paymentFromPaymentTypeName("");
				pagedListHolder.setSource(paymentFromPaymentTypeName);
				model.addAttribute("message", "Inserted Successfully");
				return new ModelAndView("payment");
			} else {
				// System.out.println(s1);
				model.addAttribute("message", "Payment type name already exists.");
				return new ModelAndView("payment");
			}
		}
	}*/

	/*@RequestMapping(value = "/paym", params = "Update")
	public ModelAndView payment1updates(@ModelAttribute("paym") Payment p, Model model, BindingResult result, HttpServletRequest req) {

		ModelAndView loginModelView = new ModelAndView("Login");
		boolean isUserLoggedIn = Utilities.isUserLoggedIn(req, model, result);
		if(isUserLoggedIn == false){
			return loginModelView;
		}
		
		PagedListHolder pagedListHolder = new PagedListHolder(new ArrayList<String>());
		int page = ServletRequestUtils.getIntParameter(req, "p", 0);
		pagedListHolder.setPage(page);
		int pageSize = 10;
		pagedListHolder.setPageSize(pageSize);
		model.addAttribute("pagedListHolder", pagedListHolder);
		PaymentFormValidations paymentvalids = new PaymentFormValidations();

		paymentvalids.validate(p, result);
		HttpSession httpSession = req.getSession(false);
		if (result.hasErrors()) {
			httpSession.setAttribute("isValidationErrorPresentPaymentUpdate", "Y");
			return new ModelAndView("payment");
		}
		else {
			httpSession.setAttribute("isValidationErrorPresentPaymentUpdate", "N");
			String paymentname = p.getName();
			String paymentmethod = p.getMethod12();
			String s1 = servint.updatepayment(p, paymentname, paymentmethod);
			List<String> paymentFromPaymentTypeName = servint.paymentFromPaymentTypeName("");
			pagedListHolder.setSource(paymentFromPaymentTypeName);
			if (s1.equalsIgnoreCase("Success")) {
				// System.out.println(s1);
				model.addAttribute("message", "Updated Successfully");
				return new ModelAndView("payment");
			} else {
				// System.out.println(s1);
				model.addAttribute("message", "Payment type name already exists.");
				return new ModelAndView("payment");
			}
		
		}
		
		
	}
*/
	@RequestMapping(value = "/paym", params = "Delete")
	public ModelAndView payment1delete(@ModelAttribute("paym") Payment paymentType, Model model, BindingResult result, HttpServletRequest req) {
		
		ModelAndView loginModelView = new ModelAndView("Login");
		boolean isUserLoggedIn = Utilities.isUserLoggedIn(req, model, result);
		if(isUserLoggedIn == false){
			return loginModelView;
		}
		
		String s1 ="";
		boolean isPaymentTypeBeingUsed = servint.isPaymentTypeBeingUsed(paymentType);
		if(isPaymentTypeBeingUsed){
			s1 = servint.deletepayment(paymentType);
		}else{
			model.addAttribute("message", "Payment Type is already being used.");
		}
		return new ModelAndView("payment");
	}

	//

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/deletepaymentId", method = RequestMethod.GET)
	@ResponseBody
	public List deletePaymentValuesId(@ModelAttribute("paym") Payment paymentType, Model model, int id) {

		String s1 ="";
		boolean isPaymentTypeBeingUsed = servint.isPaymentTypeBeingUsed(paymentType);
		if(isPaymentTypeBeingUsed){
			s1 = servint.deletePaymentValuesId(paymentType.getId());
			
			if (s1.equalsIgnoreCase("success")) {
				List<String> deletePaymentValuesId = new ArrayList<String>();
				deletePaymentValuesId.removeAll(deletePaymentValuesId);
				deletePaymentValuesId.add("Success");
				//System.out.println(deletePaymentValuesId);
				return deletePaymentValuesId;
			} else {
				return null;
			}
			
		}else{
			model.addAttribute("message", "Payment Type is already being used.");
		}
		return null;
		
	}

	// ************************Currency all crude
	// operations***********************************

	@RequestMapping(value = "/currency")
	public ModelAndView currency(@ModelAttribute("curr") Currency cur, Model model, BindingResult result, HttpServletRequest req) {
		
		ModelAndView loginModelView = new ModelAndView("Login");
		boolean isUserLoggedIn = Utilities.isUserLoggedIn(req, model, result);
		if(isUserLoggedIn == false){
			return loginModelView;
		}
		
		HttpSession httpSession = req.getSession(false);
		httpSession.setAttribute("isValidationErrorPresentCurrency", "N");
		httpSession.setAttribute("isValidationErrorPresentCurrencyUpdate", "N");
		
		List<String> currencyvalues = servint.currencyFromCurrencyName(cur.getName() == null?"":cur.getName());
		
		PagedListHolder pagedListHolder = new PagedListHolder(currencyvalues);
		int page = ServletRequestUtils.getIntParameter(req, "p", 0);
		pagedListHolder.setPage(page);
		int pageSize = 10;
		pagedListHolder.setPageSize(pageSize);
		model.addAttribute("pagedListHolder", pagedListHolder);
		model.addAttribute("searchKey", cur.getName());
		
		return new ModelAndView("currency");
	}

/*	@RequestMapping(value = "/curropertion", params = "Insert")
	public ModelAndView currencyoperationinsert(@ModelAttribute("curr") Currency cur, Model model, BindingResult result, HttpServletRequest req) {
		// System.out.println("inside currency insert operation");

		ModelAndView loginModelView = new ModelAndView("Login");
		boolean isUserLoggedIn = Utilities.isUserLoggedIn(req, model, result);
		if(isUserLoggedIn == false){
			return loginModelView;
		}
		

		PagedListHolder pagedListHolder = new PagedListHolder(new ArrayList<String>());
		int page = ServletRequestUtils.getIntParameter(req, "p", 0);
		pagedListHolder.setPage(page);
		int pageSize = 10;
		pagedListHolder.setPageSize(pageSize);
		model.addAttribute("pagedListHolder", pagedListHolder);
		
		CurrencyFormValidators c = new CurrencyFormValidators();

		c.validate(cur, result);
		HttpSession httpSession = req.getSession(false);
		if (result.hasErrors()) {
			httpSession.setAttribute("isValidationErrorPresentCurrency", "Y");
			return new ModelAndView("currency");
		} else {
			httpSession.setAttribute("isValidationErrorPresentCurrency", "N");
			String currencyname = cur.getName();
			String s1 = servint.insert(cur, currencyname);
			List<String> currencyvalues = servint.currencyFromCurrencyName("");
			pagedListHolder.setSource(currencyvalues);
			if (s1.equalsIgnoreCase("Success")) {
				return new ModelAndView("currency");
			} else {
				return new ModelAndView("currency");
			}
		}
	}

	@RequestMapping(value = "/curropertion", params = "Update")
	public ModelAndView currencyoperationupdate(@ModelAttribute("curr") Currency cur, Model model, BindingResult result, HttpServletRequest req) {
		
		ModelAndView loginModelView = new ModelAndView("Login");
		boolean isUserLoggedIn = Utilities.isUserLoggedIn(req, model, result);
		if(isUserLoggedIn == false){
			return loginModelView;
		}
		
		PagedListHolder pagedListHolder = new PagedListHolder(new ArrayList<String>());
		int page = ServletRequestUtils.getIntParameter(req, "p", 0);
		pagedListHolder.setPage(page);
		int pageSize = 10;
		pagedListHolder.setPageSize(pageSize);
		model.addAttribute("pagedListHolder", pagedListHolder);
		
		CurrencyFormValidators c = new CurrencyFormValidators();

		c.validate(cur, result);
		HttpSession httpSession = req.getSession(false);
		if (result.hasErrors()) {
			httpSession.setAttribute("isValidationErrorPresentCurrencyUpdate", "Y");
			return new ModelAndView("currency");
		} 
		else {
			httpSession.setAttribute("isValidationErrorPresentCurrencyUpdate", "N");
		
			String currencyname = cur.getName();
			String s1 = servint.update(cur, currencyname);
			List<String> currencyvalues = servint.currencyFromCurrencyName("");
			pagedListHolder.setSource(currencyvalues);
			if (s1.equalsIgnoreCase("Success")) {
				
				return new ModelAndView("currency");
			} else {
				
				return new ModelAndView("currency");
			}
		
		}
		
		
		
	}*/

	@RequestMapping(value = "/curropertion", params = "Delete")
	public ModelAndView currencyoperationdelete(@ModelAttribute("curr") Currency cur, Model model, BindingResult result, HttpServletRequest req) {
		
		ModelAndView loginModelView = new ModelAndView("Login");
		boolean isUserLoggedIn = Utilities.isUserLoggedIn(req, model, result);
		if(isUserLoggedIn == false){
			return loginModelView;
		}
		
		String s1 ="";
		boolean isCurrencyBeingUsed = servint.isCurrencyBeingUsed(cur);
		if(isCurrencyBeingUsed){
			s1 = servint.delete(cur);
		}else{
			model.addAttribute("message", "Currency is already being used.");
		}
		
		return new ModelAndView("currency");
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/currencyvalues", method = RequestMethod.GET)
	@ResponseBody
	public List ajaxcurrency(@ModelAttribute("curr") Currency cur, Model model, HttpServletRequest request) {
		List<String> currencyvalues = servint.showcurrency(cur);
		
		PagedListHolder pagedListHolder = new PagedListHolder(currencyvalues);
		int page = ServletRequestUtils.getIntParameter(request, "p", 0);
		pagedListHolder.setPage(page);
		int pageSize = 10;
		pagedListHolder.setPageSize(pageSize);
		model.addAttribute("pagedListHolder", pagedListHolder);
		return pagedListHolder.getPageList();
	}
	
	
	//currencyFromCurrencyName

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/currencyFromCurrencyName", method = RequestMethod.GET)
	@ResponseBody
	public List currencyFromCurrencyName(String currencyName) {
		//System.out.println("currency value called");
		List<String> currencyFromCurrencyName = servint.currencyFromCurrencyName(currencyName);
		return currencyFromCurrencyName;
	}
	
	
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/deleteCurrencyId", method = RequestMethod.GET)
	@ResponseBody
	public List ajaxcurrencyallvalues(@ModelAttribute("curr") Currency cur, Model model, int id) {
		
		String s1 ="";
		boolean isCurrencyBeingUsed = servint.isCurrencyBeingUsed(cur);
		if(isCurrencyBeingUsed){
			s1 = servint.deleteCurrencyValuesId(id);
		}else{
			s1 = "Currency is already being used.";
		}
		model.addAttribute("message", s1);
		List<String> currencyId = new ArrayList<String>();
		//System.out.println("Action class s1 value = " + s1);
		if (s1.equalsIgnoreCase("success")) {
			currencyId.add("Success");
			return currencyId;
		} else {
			currencyId.add(s1);
			//System.out.println(currencyId);
			return currencyId;
		}
	}

	// deleteCurrencyValuesId

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/currencyallvalues", method = RequestMethod.GET)
	@ResponseBody
	public List deleteCurrencyValuesId(@ModelAttribute("curr") Currency cur, Model model, int id) {
		// System.out.println("ajax values");
		List<String> currencyallvalues = servint.showallcurrency(id);
		// System.out.println("action class currencyvalues list = "+
		// currencyallvalues);
		return currencyallvalues;
	}

	// ************************************

	// dashboard values
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/dashboard", method = RequestMethod.GET)
	@ResponseBody
	public List dashboardValues(HttpServletRequest req) {

		HttpSession httpSession =  req.getSession(false);
		Integer casinoManagerId = (Integer) httpSession.getAttribute("casinoManagerId");

		List<String> pendingbusinesspartner = servint.pendingBusinessPartnersList(casinoManagerId);
		return pendingbusinesspartner;
	}

	//

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/dashboardValuesUpdate", method = RequestMethod.GET)
	@ResponseBody
	public List dashboarUpdatesValues(String businessPartnerToBeApproved, String approved, HttpServletRequest req) {

		HttpSession httpSession =  req.getSession(false);
		Integer casinoManagerId = (Integer) httpSession.getAttribute("casinoManagerId");
		
		//System.out.println("length = " + businessPartnerToBeApproved + "  Status=== " + approved);
		List<String> approvedValues = new ArrayList<String>();
		int status = Integer.parseInt(approved);
		String s1 = servint.aproveBusinessPartners(businessPartnerToBeApproved, status, casinoManagerId);
		List<String> dashboard = new ArrayList<String>();
		dashboard.removeAll(dashboard);
		dashboard.add("Success");
		return approvedValues;
	}

	//

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/dashboardBalancesUpdate", method = RequestMethod.GET)
	@ResponseBody
	public List dashboarBalancesApproveValues(String balancesToBeApproved, String approved, String depositType) {

		//System.out.println("length = " + balancesToBeApproved + "  Status=== " + approved);
		List<String> approvedValues = new ArrayList<String>();
		int status = Integer.parseInt(approved);
		Integer depositTypeConfirmation = null;
		if(status == Constants.STATUS_APPROVED){
			if(depositType != null){
				depositTypeConfirmation = Integer.parseInt(depositType);
			}
		}

		String s1 = servint.aproveBalances(balancesToBeApproved, status, depositTypeConfirmation);
		List<String> balances = new ArrayList<String>();
		balances.removeAll(balances);
		balances.add("Success");
		return balances;
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/dashboardBalancesValue", method = RequestMethod.GET)
	@ResponseBody
	public List dashboarhBalancesValues(HttpServletRequest req) {

		HttpSession httpSession =  req.getSession(false);
		Integer casinoManagerId = (Integer) httpSession.getAttribute("casinoManagerId");

		List<String> pendingBalances = new ArrayList<String>();

		pendingBalances = servint.pendingBalances(casinoManagerId);
		return pendingBalances;
	}
	


	@RequestMapping(value = "/approvedAmounts")
	public ModelAndView approvedAmounts(HttpServletRequest req, String userNameApprovedAmount) {
		
		ModelAndView approvedAmountModelView = new ModelAndView("approvedAmountsList");
		//System.out.println("userNameApprovedAmount = "+userNameApprovedAmount);
		List<String> approvedAmountListbyUserName = servint.getApprovedAmountListbyUser(userNameApprovedAmount == null?"":userNameApprovedAmount);
		
		PagedListHolder pagedListHolder = new PagedListHolder(approvedAmountListbyUserName);
		int page = ServletRequestUtils.getIntParameter(req, "p", 0);
		pagedListHolder.setPage(page);
		int pageSize = 20;
		pagedListHolder.setPageSize(pageSize);
		approvedAmountModelView.addObject("pagedListHolder", pagedListHolder);
		approvedAmountModelView.addObject("searchKey", userNameApprovedAmount);

		return approvedAmountModelView;
	}
	
	@RequestMapping(value = "/approvedCredits")
	public ModelAndView approvedCredits(HttpServletRequest req, String userNameApprovedCredits) {
		
		ModelAndView approvedCreditModelView = new ModelAndView("approvedCreditsList");
		//System.out.println("userNameApprovedCredits = "+userNameApprovedCredits);
		List<String> approvedCreditListbyUserName = servint.getApprovedCreditListbyUser(userNameApprovedCredits == null?"":userNameApprovedCredits);
		
		PagedListHolder pagedListHolder = new PagedListHolder(approvedCreditListbyUserName);
		int page = ServletRequestUtils.getIntParameter(req, "p", 0);
		pagedListHolder.setPage(page);
		int pageSize = 20;
		pagedListHolder.setPageSize(pageSize);
		approvedCreditModelView.addObject("pagedListHolder", pagedListHolder);
		approvedCreditModelView.addObject("searchKey", userNameApprovedCredits);

		return approvedCreditModelView;
	}
	
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/approvedAmountsList", method = RequestMethod.GET)
	@ResponseBody
	public List getApprovedAmountsList(HttpServletRequest req) {

		HttpSession httpSession =  req.getSession(false);
		Integer casinoManagerId = (Integer) httpSession.getAttribute("casinoManagerId");

		List<String> approvedAmountsList = servint.getApprovedAmountsList(casinoManagerId);
		return approvedAmountsList;
	}
	
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/approvedCreditsList", method = RequestMethod.GET)
	@ResponseBody
	public List getApprovedCreditsList(HttpServletRequest req) {

		HttpSession httpSession =  req.getSession(false);
		Integer casinoManagerId = (Integer) httpSession.getAttribute("casinoManagerId");

		List<String> approvedCreditsList = servint.getApprovedCreditsList(casinoManagerId);
		return approvedCreditsList;
	}
	
	
	//checkUserNameApprovedAmount
	
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/checkUserNameApprovedAmount", method = RequestMethod.GET)
	@ResponseBody
	public List getApprovedAmountUsernameValues(HttpServletRequest req,String userNameApprovedAmount) {

		//System.out.println("userNameApprovedAmount = "+userNameApprovedAmount);
	  List<String>getApprovedAmountListbyUserName=servint.getApprovedAmountListbyUser(userNameApprovedAmount);
		return getApprovedAmountListbyUserName;
	}
	
	//
	

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/checkUserNameApprovedCredits", method = RequestMethod.GET)
	@ResponseBody
	public List getApprovedCreditUsernameValues(HttpServletRequest req,String userNameApprovedCredits) {

	  List<String>getApprovedCreditListbyUserName=servint.getApprovedCreditListbyUser(userNameApprovedCredits);
		return getApprovedCreditListbyUserName;
	}
	
//	
	@RequestMapping(value = "/BusinessPartnerIDView")
	public String BusinessPartnerIDView(Integer id, Model model, HttpServletRequest req) {
		
		HttpSession httpSession = req.getSession(false);

		List<String> businessPartnerInfo = new ArrayList<String>();
		businessPartnerInfo = servint.getBusinessPartnerIdInformationDisplay(id);
	
		PagedListHolder pagedListHolder = new PagedListHolder(businessPartnerInfo);
		int page = ServletRequestUtils.getIntParameter(req, "p", 0);
		pagedListHolder.setPage(page);
		int pageSize = 10;
		pagedListHolder.setPageSize(pageSize);
		model.addAttribute("pagedListHolder", pagedListHolder);
		
		return "BusinessPartnerIDView";
	}
	
	
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/businessPartnerIdInformationDisplay", method = RequestMethod.GET)
	@ResponseBody
	public List businessPartnerIdInformationDisplay(HttpServletRequest req , int businessPartnerId) {
        //System.out.println("Business PArtner Id = "+businessPartnerId);
	  List<String> getbusinessPartnerIdInformationDisplay = servint.getBusinessPartnerIdInformationDisplay(businessPartnerId);
		return getbusinessPartnerIdInformationDisplay;
	}


	//
	
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/businessPartnerIdPersonalInformation", method = RequestMethod.GET)
	@ResponseBody
	public List businessPartnerIdPersonalInformation(HttpServletRequest req , int businessPartnerId) {
        //System.out.println("Business PArtner Id = "+businessPartnerId);
	  List<String>businessPartnerIdPersonalInformation=servint.getbusinessPartnerIdPersonalInformation(businessPartnerId);
		return businessPartnerIdPersonalInformation;
	}
	
	//playersCasinoManager
	
	@RequestMapping(value = "/playersCasinoManager")
	public String players (Model model,HttpServletRequest req,String username) {
		HttpSession httpSession = req.getSession(false);
		if (httpSession == null) {
			return "Login";
		}
		
		List<String> playerList = new ArrayList<String>();
		playerList = servint.getPlayers(username==null ?"":username);
	   
		PagedListHolder pagedListHolder = new PagedListHolder(playerList);
		int page = ServletRequestUtils.getIntParameter(req, "p", 0);
		pagedListHolder.setPage(page);
		int pageSize = 10;
		pagedListHolder.setPageSize(pageSize);
		model.addAttribute("pagedListHolder", pagedListHolder);
		model.addAttribute("searchKey", username);
		return "Players";
	}

	//playersForBusinessPartner
	
	@RequestMapping(value = "/playersForBusinessPartner")
	public String playersForBusinessPartner (Model model,HttpServletRequest req,String username) {
		HttpSession httpSession = req.getSession(false);
		if (httpSession == null) {
			return "Login";
		}
		
		List<String> playerList = new ArrayList<String>();
		playerList = servint.getPlayersForBusinessPartner(username==null ?"":username);
	    //playerList = serviceinter.getPlayersByUserNameForAgents(player.getUsername() == null ? "" : player.getUsername(), businessPartnerId, agentType);	
		PagedListHolder pagedListHolder = new PagedListHolder(playerList);
		int page = ServletRequestUtils.getIntParameter(req, "p", 0);
		pagedListHolder.setPage(page);
		int pageSize = 10;
		pagedListHolder.setPageSize(pageSize);
		model.addAttribute("pagedListHolder", pagedListHolder);
		model.addAttribute("searchKey", username);
		return "PlayersForBusinessPartner";
	}
	
	//playersForAgents
	
	@RequestMapping(value = "/playersForAgents")
	public String playersForAgents (Model model,HttpServletRequest req,String username) {
		HttpSession httpSession = req.getSession(false);
		if (httpSession == null) {
			return "Login";
		}
		
		List<String> playerList = new ArrayList<String>();
		playerList = servint.getPlayersForAgents(username==null ?"":username);
	    //playerList = serviceinter.getPlayersByUserNameForAgents(player.getUsername() == null ? "" : player.getUsername(), businessPartnerId, agentType);	
		PagedListHolder pagedListHolder = new PagedListHolder(playerList);
		int page = ServletRequestUtils.getIntParameter(req, "p", 0);
		pagedListHolder.setPage(page);
		int pageSize = 10;
		pagedListHolder.setPageSize(pageSize);
		model.addAttribute("pagedListHolder", pagedListHolder);
		model.addAttribute("searchKey", username);
		return "PlayersForAgents";
	}
	
	@RequestMapping(value = "/payback")
	public String payBackPage(Model model,HttpServletRequest req,String username)
	{
		
		 HttpSession httpSession =  req.getSession(false);
		   
			//playerCreditInfo = serviceinter.displayPlayersCreditInfo(username == null ?"" : username,businessPartnerId);
		   List<String> payBackValues = new ArrayList<String>();
		   payBackValues = servint.getPayBackValues(username==null ?"":username);
			PagedListHolder pagedListHolder = new PagedListHolder(payBackValues);
			int page = ServletRequestUtils.getIntParameter(req, "p", 0);
			pagedListHolder.setPage(page);
			int pageSize = 10;
			pagedListHolder.setPageSize(pageSize);
			model.addAttribute("pagedListHolder", pagedListHolder);
			model.addAttribute("searchKey", username);
			
		return "payBackPageIndex";
	}
	
	@RequestMapping(value="/payBackOperations")
	public ModelAndView payBackOperations(@ModelAttribute("payBackCrudOperation") PaybackModel paybackmodel,HttpServletRequest req,Model model,String username)
	{
	   HttpSession httpSession =  req.getSession(false);
	   
		//playerCreditInfo = serviceinter.displayPlayersCreditInfo(username == null ?"" : username,businessPartnerId);
	   /*List<String> payBackValues = new ArrayList<String>();
	   payBackValues = servint.getPayBackValues(username==null ?"":username);
		PagedListHolder pagedListHolder = new PagedListHolder(payBackValues);
		int page = ServletRequestUtils.getIntParameter(req, "p", 0);
		pagedListHolder.setPage(page);
		int pageSize = 10;
		pagedListHolder.setPageSize(pageSize);
		model.addAttribute("pagedListHolder", pagedListHolder);
		model.addAttribute("searchKey", username);*/
	 
		Map<Integer, String> businessPartnerIdValueMap = servint.getBusinessPartnerIdValueMap();
		Map<Integer, String> currencyIdValueMap = servint.getCurrencyIdValueMap();
		Map<Integer, String> paymentTypeIdValueMap = servint.getPaymentTypeIdValueMap();
		model.addAttribute("depositebusinesspartner", businessPartnerIdValueMap);
		model.addAttribute("depositcurrency", currencyIdValueMap);
		model.addAttribute("depositpaymentvalues", paymentTypeIdValueMap);
		return new ModelAndView("payBackOperations");
	}
	
	
	@RequestMapping(value="/payBackCrudOperations",params = "insert")
	public ModelAndView payBackInsertOperations(@ModelAttribute("payBackCrudOperation") PaybackModel paybackmodel,HttpServletRequest req,Model model,BindingResult result)
	{
	    HttpSession httpSession =  req.getSession(false);
	    
	    List<String> payBackValues = new ArrayList<String>();

		PagedListHolder pagedListHolder = new PagedListHolder(payBackValues);
		int page = ServletRequestUtils.getIntParameter(req, "p", 0);
		pagedListHolder.setPage(page);
		int pageSize = 10;
		pagedListHolder.setPageSize(pageSize);
		model.addAttribute("pagedListHolder", pagedListHolder);
	    
		Map<Integer, String> businessPartnerIdValueMap = servint.getBusinessPartnerIdValueMap();
		Map<Integer, String> currencyIdValueMap = servint.getCurrencyIdValueMap();
		Map<Integer, String> paymentTypeIdValueMap = servint.getPaymentTypeIdValueMap();
		model.addAttribute("depositebusinesspartner", businessPartnerIdValueMap);
		model.addAttribute("depositcurrency", currencyIdValueMap);
		model.addAttribute("depositpaymentvalues", paymentTypeIdValueMap);
		
		PayBackFormValidation paybackvalidation = new PayBackFormValidation();
		paybackvalidation.validate(paybackmodel, result);
		if (result.hasErrors()) {
			return new ModelAndView("payBackOperations");
		}
		else
		{
			Integer casinoManagerId = (Integer) req.getSession().getAttribute("casinoManagerId");
			paybackmodel.setCasinoManagerId(casinoManagerId);
			paybackmodel.setAmount(Double.parseDouble(paybackmodel.getAmountStr()));
			
			boolean isValidCurrency = servint.validateCurrencyForPayBack(paybackmodel.getBusinessPartnerId(), paybackmodel.getCurrencyId());
			
			if (isValidCurrency){
				
				boolean isValidPayBackAmount = servint.validatePayBackAmount(paybackmodel);
				
				if (isValidPayBackAmount){
					String s1 = servint.insertpayback(paybackmodel);
					
					if(s1.equalsIgnoreCase("success")){
						   payBackValues = servint.getPayBackValues("");
						   pagedListHolder.setSource(payBackValues);
					}
					return new ModelAndView("payBackPageIndex");
				}else{
					model.addAttribute("message", "Business Partner does not have sufficient total amount/credit amount to pay back.");
					return new ModelAndView("payBackOperations");
				}

			}else{
				model.addAttribute("message", "Business Partner does not have selected currency account.");
				return new ModelAndView("payBackOperations");
			}

			
		}
		
	}
	
	
	@RequestMapping(value = "/forgotPasswordPage",params="forgotPassword")
	public ModelAndView forgotPassword(@ModelAttribute("casinoManagerCredential") ForgotPassword forgotPasswordCredentialsPage , Model model) {
		//System.out.println("inside ForgotPassword Page");
		return new ModelAndView("forgotPasswordPage");
	}
	
	
	@RequestMapping(value = "/casinoManagerCredential",params="forgotPassword")
	public ModelAndView forgotPasswordCredential(@ModelAttribute("casinoManagerCredential") ForgotPassword forgotPasswordCredentialsPage , Model model,BindingResult result) {
	
		ForgotPasswordFormValidation forgotPasswordFormValidation=new ForgotPasswordFormValidation();
		forgotPasswordFormValidation.validate(forgotPasswordCredentialsPage, result);
		if(result.hasErrors())
		{
				
			return new ModelAndView("forgotPasswordPage");
		}
		else
		{
			String s1 = servint.checkUserName(forgotPasswordCredentialsPage);
			//System.out.println("reyurn type = "+s1);
			if(s1.contains("Fail"))
			{
				model.addAttribute("message","Please give valid User Name");
			return new ModelAndView("forgotPasswordPage");
			}
			else
			{
				model.addAttribute("message","Your Verfication Code is send to your Registered Email");
				return new ModelAndView("forgotPasswordPage");
			}
	}
}
	
	//activateCasinoManager
	@RequestMapping(value = "/activateCasinoManager")
	public ModelAndView activateCasinoManager(@ModelAttribute("casinoManagerEmailVerification")ConfirmPasswordModel confirmPasswordModel)
	{
		return new ModelAndView("forgotPasswordEmailVerification");
	}
	
	
	//casinoManagerEmailVerification
	
	@RequestMapping(value = "/casinoManagerEmailVerification",params="reset")
	public ModelAndView activateCasinoManagerEmailVerification(@ModelAttribute("casinoManagerEmailVerification")ConfirmPasswordModel confirmPasswordModel, BindingResult result, HttpServletRequest req,Model model)
	{
	//	return new ModelAndView("forgotPasswordEmailVerification");
		
		HttpSession httpSession = req.getSession(false);
		if (httpSession == null) {
			return new ModelAndView("Login");
		}
		
		ConfirmPasswordFormValidation confirmPasswordFormValidation = new ConfirmPasswordFormValidation();
		confirmPasswordFormValidation.validate(confirmPasswordModel, result);
		
		if (result.hasErrors()) {
		
			return new ModelAndView("forgotPasswordEmailVerification");
		} else {

			String resultStr = servint.forgotPasswordOnActivationAction(confirmPasswordModel);

			if (resultStr.equalsIgnoreCase("Success")) {
				model.addAttribute("go", new Modelbean());
				//System.out.println("till here");
				return new ModelAndView("Login");
			} else {
				model.addAttribute("message", "Either activation code has been expired or User is already activated.");
				
			}
		}
		return new ModelAndView("forgotPasswordEmailVerification");
	}

	}
	
