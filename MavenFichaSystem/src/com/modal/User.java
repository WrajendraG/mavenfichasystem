/*
 * Copyright @2015 Alteatec Pvt Ltd.
 */
package com.modal;

// TODO: Auto-generated Javadoc
/**
 * The Class User.
 */
public class User {

	/** The id. */
	// currencyId is code like eur, usd etc and currencyCode is id like 1,2 etc.
	private String id;

	/** The name. */
	private String name;

	/** The name. */
	private String firstName;

	/** The name. */
	private String lastName;

	/** The email. */
	private String email;

	/** The phone no. */
	private String phoneNo;

	/** The username. */
	private String username;

	/** The hashed password. */
	private String hashedPassword;

	/** The salt. */
	private String salt;

	/** The status. */
	private Boolean status;

	/** The activation code. */
	private String activationCode;

	/** The player id. */
	private Integer playerId;

	/** The is risky. */
	private boolean isRisky;

	/** The skin id. */
	private Integer skinId;

	/** The login status. */
	private String loginStatus;

	/** The msg. */
	private String msg;

	private String password;

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	/** The forgot password activation code. */
	private String forgotPasswordActivationCode;

	/** The skin name. */
	private String skinName;

	/** The error code. */
	private Integer errorCode;

	/** The currency id. */
	private Integer currencyId;

	/** The currency code. */
	private String currencyCode;

	/** The language id. */
	private Integer languageId;

	/** The language code. */
	private String languageCode;

	/** The country id. */
	private Integer countryId;

	/** The country code. */
	private String countryCode;

	/** The error flag. */
	private Boolean errorFlag;

	/** The validation1. */
	private Boolean validation1;

	/** The validation2. */
	private Boolean validation2;

	/** The validation3. */
	private Boolean validation3;

	/** The validation4. */
	private Boolean validation4;

	/** The validation5. */
	private Boolean validation5;

	/** The validation6. */
	private Boolean validation6;

	/** The validation7. */
	private Boolean validation7;

	/** The validation8. */
	private Boolean validation8;

	/** The validation9. */
	private Boolean validation9;

	/** The validation10. */
	private Boolean validation10;

	// for betsonstruct
	private String authtoken;

	// pragmatic Player Id
	private Integer pragmaticPlayerId;

	// loss value
	private Double lossValue;

	public Double getLossValue() {
		return lossValue;
	}

	public void setLossValue(Double lossValue) {
		this.lossValue = lossValue;
	}

	// country Name
	private String countryName;
	// social Security Number
	private String socialSecurityNumber;

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getSocialSecurityNumber() {
		return socialSecurityNumber;
	}

	public void setSocialSecurityNumber(String socialSecurityNumber) {
		this.socialSecurityNumber = socialSecurityNumber;
	}

	/**
	 * @param pragmaticPlayerId
	 *            the pragmaticPlayerId to set
	 */
	public void setPragmaticPlayerId(Integer pragmaticPlayerId) {
		this.pragmaticPlayerId = pragmaticPlayerId;
	}

	/**
	 * @return the pragmaticPlayerId
	 */
	public Integer getPragmaticPlayerId() {
		return pragmaticPlayerId;
	}

	public void setAuthtoken(String authtoken) {
		this.authtoken = authtoken;
	}

	public String getAuthtoken() {
		return authtoken;
	}

	/**
	 * Gets the error code.
	 *
	 * @return the error code
	 */
	public Integer getErrorCode() {
		return errorCode;
	}

	/**
	 * Sets the error code.
	 *
	 * @param errorCode
	 *            the new error code
	 */
	public void setErrorCode(Integer errorCode) {
		this.errorCode = errorCode;
	}

	/**
	 * Gets the forgot password activation code.
	 *
	 * @return the forgot password activation code
	 */
	public String getForgotPasswordActivationCode() {
		return forgotPasswordActivationCode;
	}

	/**
	 * Sets the forgot password activation code.
	 *
	 * @param forgotPasswordActivationCode
	 *            the new forgot password activation code
	 */
	public void setForgotPasswordActivationCode(String forgotPasswordActivationCode) {
		this.forgotPasswordActivationCode = forgotPasswordActivationCode;
	}

	/**
	 * Gets the login status.
	 *
	 * @return the login status
	 */
	public String getLoginStatus() {
		return loginStatus;
	}

	/**
	 * Sets the login status.
	 *
	 * @param loginStatus
	 *            the new login status
	 */
	public void setLoginStatus(String loginStatus) {
		this.loginStatus = loginStatus;
	}

	/**
	 * Checks if is risky.
	 *
	 * @return true, if is risky
	 */
	public boolean isRisky() {
		return isRisky;
	}

	/**
	 * Gets the msg.
	 *
	 * @return the msg
	 */
	public String getMsg() {
		return msg;
	}

	/**
	 * Sets the msg.
	 *
	 * @param msg
	 *            the new msg
	 */
	public void setMsg(String msg) {
		this.msg = msg;
	}

	/**
	 * Sets the risky.
	 *
	 * @param isRisky
	 *            the new risky
	 */
	public void setRisky(boolean isRisky) {
		this.isRisky = isRisky;
	}

	/**
	 * Gets the player id.
	 *
	 * @return the player id
	 */
	public Integer getPlayerId() {
		return playerId;
	}

	/**
	 * Sets the player id.
	 *
	 * @param playerId
	 *            the new player id
	 */
	public void setPlayerId(Integer playerId) {
		this.playerId = playerId;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name
	 *            the name
	 * @return the user
	 */
	public User setName(String name) {
		this.name = name;
		return this;
	}

	/**
	 * Sets the first name.
	 *
	 * @param firstName
	 *            the first name
	 * @return the user
	 */
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * Sets the last name.
	 *
	 * @param lastName
	 *            the last name
	 * @return the user
	 */
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * Gets the email.
	 *
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Sets the email.
	 *
	 * @param email
	 *            the email
	 * @return the user
	 */
	public User setEmail(String email) {
		this.email = email;
		return this;
	}

	/**
	 * Gets the phoneNo.
	 *
	 * @return the phoneNo
	 */
	public String getPhoneNo() {
		return phoneNo;
	}

	/**
	 * Sets the phoneNo.
	 *
	 * @param phoneNo
	 *            the phoneNo
	 * @return the user
	 */
	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	/**
	 * Gets the username.
	 *
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * Sets the username.
	 *
	 * @param username
	 *            the username
	 * @return the user
	 */
	public User setUsername(String username) {
		this.username = username;
		return this;
	}

	/**
	 * Gets the hashed password.
	 *
	 * @return the hashed password
	 */
	public String getHashedPassword() {
		return hashedPassword;
	}

	/**
	 * Sets the hashed password.
	 *
	 * @param hashedPassword
	 *            the hashed password
	 * @return the user
	 */
	public User setHashedPassword(String hashedPassword) {
		this.hashedPassword = hashedPassword;
		return this;
	}

	/**
	 * Gets the salt.
	 *
	 * @return the salt
	 */
	public String getSalt() {
		return salt;
	}

	/**
	 * Sets the salt.
	 *
	 * @param salt
	 *            the salt
	 * @return the user
	 */
	public User setSalt(String salt) {
		this.salt = salt;
		return this;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public Boolean getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status
	 *            the status
	 * @return the user
	 */
	public User setStatus(Boolean status) {
		this.status = status;
		return this;
	}

	/**
	 * Gets the activation code.
	 *
	 * @return the activation code
	 */
	public String getActivationCode() {
		return activationCode;
	}

	/**
	 * Sets the activation code.
	 *
	 * @param activationCode
	 *            the activation code
	 * @return the user
	 */
	public User setActivationCode(String activationCode) {
		this.activationCode = activationCode;
		return this;
	}

	/**
	 * Gets the skin id.
	 *
	 * @return the skin id
	 */
	public Integer getSkinId() {
		return skinId;
	}

	/**
	 * Sets the skin id.
	 *
	 * @param skinId
	 *            the new skin id
	 */
	public void setSkinId(Integer skinId) {
		this.skinId = skinId;
	}

	/**
	 * Gets the skin name.
	 *
	 * @return the skin name
	 */
	public String getSkinName() {
		return skinName;
	}

	/**
	 * Sets the skin name.
	 *
	 * @param skinName
	 *            the new skin name
	 */
	public void setSkinName(String skinName) {
		this.skinName = skinName;
	}

	/**
	 * Gets the default currency.
	 *
	 * @return the default currency
	 */
	public Integer getDefaultCurrency() {
		return currencyId;
	}

	/**
	 * Sets the default currency.
	 *
	 * @param defaultCurrency
	 *            the new default currency
	 */
	public void setDefaultCurrency(Integer defaultCurrency) {
		this.currencyId = defaultCurrency;
	}

	/**
	 * Gets the language id.
	 *
	 * @return the language id
	 */
	public Integer getLanguageId() {
		return languageId;
	}

	/**
	 * Sets the language id.
	 *
	 * @param languageId
	 *            the new language id
	 */
	public void setLanguageId(Integer languageId) {
		this.languageId = languageId;
	}

	/**
	 * Gets the language code.
	 *
	 * @return the language code
	 */
	public String getLanguageCode() {
		return languageCode;
	}

	/**
	 * Sets the language code.
	 *
	 * @param languageCode
	 *            the new language code
	 */
	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	/**
	 * Gets the currency id.
	 *
	 * @return the currency id
	 */
	public Integer getCurrencyId() {
		return currencyId;
	}

	/**
	 * Sets the currency id.
	 *
	 * @param currencyId
	 *            the new currency id
	 */
	public void setCurrencyId(Integer currencyId) {
		this.currencyId = currencyId;
	}

	/**
	 * Gets the currency code.
	 *
	 * @return the currency code
	 */
	public String getCurrencyCode() {
		return currencyCode;
	}

	/**
	 * Sets the currency code.
	 *
	 * @param currencyCode
	 *            the new currency code
	 */
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	/**
	 * Gets the country id.
	 *
	 * @return the country id
	 */
	public Integer getCountryId() {
		return countryId;
	}

	/**
	 * Sets the country id.
	 *
	 * @param countryId
	 *            the new country id
	 */
	public void setCountryId(Integer countryId) {
		this.countryId = countryId;
	}

	/**
	 * Gets the country code.
	 *
	 * @return the country code
	 */
	public String getCountryCode() {
		return countryCode;
	}

	/**
	 * Sets the country code.
	 *
	 * @param countryCode
	 *            the new country code
	 */
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	/**
	 * Gets the error flag.
	 *
	 * @return the error flag
	 */
	public Boolean getErrorFlag() {
		return errorFlag;
	}

	/**
	 * Sets the error flag.
	 *
	 * @param errorFlag
	 *            the new error flag
	 */
	public void setErrorFlag(Boolean errorFlag) {
		this.errorFlag = errorFlag;
	}

	/**
	 * Gets the validation1.
	 *
	 * @return the validation1
	 */
	public Boolean getValidation1() {
		return validation1;
	}

	/**
	 * Sets the validation1.
	 *
	 * @param validation1
	 *            the new validation1
	 */
	public void setValidation1(Boolean validation1) {
		this.validation1 = validation1;
	}

	/**
	 * Gets the validation2.
	 *
	 * @return the validation2
	 */
	public Boolean getValidation2() {
		return validation2;
	}

	/**
	 * Sets the validation2.
	 *
	 * @param validation2
	 *            the new validation2
	 */
	public void setValidation2(Boolean validation2) {
		this.validation2 = validation2;
	}

	/**
	 * Gets the validation3.
	 *
	 * @return the validation3
	 */
	public Boolean getValidation3() {
		return validation3;
	}

	/**
	 * Sets the validation3.
	 *
	 * @param validation3
	 *            the new validation3
	 */
	public void setValidation3(Boolean validation3) {
		this.validation3 = validation3;
	}

	/**
	 * Gets the validation4.
	 *
	 * @return the validation4
	 */
	public Boolean getValidation4() {
		return validation4;
	}

	/**
	 * Sets the validation4.
	 *
	 * @param validation4
	 *            the new validation4
	 */
	public void setValidation4(Boolean validation4) {
		this.validation4 = validation4;
	}

	/**
	 * Gets the validation5.
	 *
	 * @return the validation5
	 */
	public Boolean getValidation5() {
		return validation5;
	}

	/**
	 * Sets the validation5.
	 *
	 * @param validation5
	 *            the new validation5
	 */
	public void setValidation5(Boolean validation5) {
		this.validation5 = validation5;
	}

	/**
	 * Gets the validation6.
	 *
	 * @return the validation6
	 */
	public Boolean getValidation6() {
		return validation6;
	}

	/**
	 * Sets the validation6.
	 *
	 * @param validation6
	 *            the new validation6
	 */
	public void setValidation6(Boolean validation6) {
		this.validation6 = validation6;
	}

	/**
	 * Gets the validation7.
	 *
	 * @return the validation7
	 */
	public Boolean getValidation7() {
		return validation7;
	}

	/**
	 * Sets the validation7.
	 *
	 * @param validation7
	 *            the new validation7
	 */
	public void setValidation7(Boolean validation7) {
		this.validation7 = validation7;
	}

	/**
	 * Gets the validation8.
	 *
	 * @return the validation8
	 */
	public Boolean getValidation8() {
		return validation8;
	}

	/**
	 * Sets the validation8.
	 *
	 * @param validation8
	 *            the new validation8
	 */
	public void setValidation8(Boolean validation8) {
		this.validation8 = validation8;
	}

	/**
	 * Gets the validation9.
	 *
	 * @return the validation9
	 */
	public Boolean getValidation9() {
		return validation9;
	}

	/**
	 * Sets the validation9.
	 *
	 * @param validation9
	 *            the new validation9
	 */
	public void setValidation9(Boolean validation9) {
		this.validation9 = validation9;
	}

	/**
	 * Gets the validation10.
	 *
	 * @return the validation10
	 */
	public Boolean getValidation10() {
		return validation10;
	}

	/**
	 * Sets the validation10.
	 *
	 * @param validation10
	 *            the new validation10
	 */
	public void setValidation10(Boolean validation10) {
		this.validation10 = validation10;
	}

}
