package com.modal;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "Business")
public class BusinessModal {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "business_id")
	public Integer id;
	@Column(name = "business_firstname")
	public String firstname;
	@Column(name = "business_lastname")
	public String lastname;
	@Column(name = "business_username")
	public String username;
	@Column(name = "business_email")
	public String email;
	@Column(name = "business_password")
	public String password;
	@Column(name = "business_salt")
	public String salt;
	@Column(name = "business_allowshared")
	public Boolean allowShared;
	@Column(name = "business_status")
	public Integer status;
	@Column(name = "business_phone")
	public String phone;
	@Column(name = "business_language")
	public String language;
	@Column(name = "business_sscnum")
	public String sscnum;
	@Column(name = "business_address")
	public String address;
	@Column(name = "business_country")
	public String country;
	@Column(name = "business_town")
	public String town;
	@Column(name = "business_postcode")
	public String postcode;
	@Column(name = "casino_manager_id")
	public Integer casinoManagerId;
	@Column(name = "registration_source")
	public Integer registrationSource;
	@Column(name = "bp_activation_code")
	public String activationCode;
	@Column(name = "bp_activated_status")
	public Boolean activatedStatus;
	@Column(name = "created_on")
	public Date createdOn;
	@Column(name = "updated_on")
	public Date updatedOn;
	@Column(name ="commission_percentage")
	public Double commissionPercentage;
	@Column(name="is_agent")
	public Boolean agent;
	@Column(name="agent_Type")
	public Integer agentType;
	@Column(name="affiliate_id")
	public String affiliateId;
	

	@Transient
	public String commissionPercentageStr;
	@Transient
	public String conpassword;
	@Transient
	public String businessUserName;
	@Transient
	public Boolean isUpdate = false;

	// getters and setters

	public String getBusinessUserName() {
		return businessUserName;
	}

	public void setBusinessUserName(String businessUserName) {
		this.businessUserName = businessUserName;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	public Integer getId() {
		return id;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getSscnum() {
		return sscnum;
	}

	public void setSscnum(String sscnum) {
		this.sscnum = sscnum;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getTown() {
		return town;
	}

	public void setTown(String town) {
		this.town = town;
	}

	public String getPostcode() {
		return postcode;
	}

	public Boolean getAllowShared() {
		// System.out.println("inside poja classes boolean");
		return allowShared;
	}

	public void setAllowShared(Boolean allowShared) {
		this.allowShared = allowShared;
		System.out.println(allowShared);
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
		System.out.println(postcode);
	}

	public Integer getCasinoManagerId() {
		return casinoManagerId;
	}

	public void setCasinoManagerId(Integer casinoManagerId) {
		this.casinoManagerId = casinoManagerId;
	}

	public Integer getRegistrationSource() {
		return registrationSource;
	}

	public void setRegistrationSource(Integer registrationSource) {
		this.registrationSource = registrationSource;
	}

	public String getActivationCode() {
		return activationCode;
	}

	public void setActivationCode(String activationCode) {
		this.activationCode = activationCode;
	}

	public Boolean getActivatedStatus() {
		return activatedStatus;
	}

	public void setActivatedStatus(Boolean activatedStatus) {
		this.activatedStatus = activatedStatus;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getConpassword() {
		return conpassword;
	}

	public void setConpassword(String conpassword) {
		this.conpassword = conpassword;
	}

	public Boolean isUpdate() {
		return isUpdate;
	}

	public void setIsUpdate(Boolean isUpdate) {
		this.isUpdate = isUpdate;
	}

	public Double getCommissionPercentage() {
		return commissionPercentage;
	}

	public void setCommissionPercentage(Double commissionPercentage) {
		this.commissionPercentage = commissionPercentage;
	}

	public String getCommissionPercentageStr() {
		return commissionPercentageStr;
	}

	public void setCommissionPercentageStr(String commissionPercentageStr) {
		this.commissionPercentageStr = commissionPercentageStr;
	}
	
	public Boolean getAgent() {
		return agent;
	}

	public void setAgent(Boolean agent) {
		this.agent = agent;
	}

	public Integer getAgentType() {
		return agentType;
	}

	public void setAgentType(Integer agentType) {
		this.agentType = agentType;
	}

	public String getAffiliateId() {
		return affiliateId;
	}

	public void setAffiliateId(String affiliateId) {
		this.affiliateId = affiliateId;
	}

}
