package com.modal;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="payback_credit")
public class PaybackModel {

	/**/
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="payback_credit_id")
	public Integer id;
  
    @Column(name="business_partner_id")
	public Integer businessPartnerId;
	@Column(name="casino_manager_id")
    public Integer casinoManagerId;
    @Column(name="currency")
    public Integer currencyId;
	@Column(name="payment")
    public Integer paymentId;
	@Column(name="amount")
    public Double amount;
	@Column(name="payback_type")
    public Integer payBack;
	@Column(name="created_on")
	public Date createdOn;
	@Column(name="updated_on")
	public Date updatedOn;
	
	@Transient
    public String amountStr;
    
    
	public Integer getBusinessPartnerId() {
		return businessPartnerId;
	}

	public Integer getCurrencyId() {
		return currencyId;
	}

	public Integer getPaymentId() {
		return paymentId;
	}

	public Double getAmount() {
		return amount;
	}

	public Integer getPayBack() {
		return payBack;
	}

	public void setBusinessPartnerId(Integer businessPartnerId) {
		this.businessPartnerId = businessPartnerId;
	}

	public void setCurrencyId(Integer currencyId) {
		this.currencyId = currencyId;
	}

	public void setPaymentId(Integer paymentId) {
		this.paymentId = paymentId;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public void setPayBack(Integer payBack) {
		this.payBack = payBack;
	}

	public String getAmountStr() {
		return amountStr;
	}

	public void setAmountStr(String amountStr) {
		this.amountStr = amountStr;
	}
	
	 public Integer getId() {
			return id;
		}

	public Integer getCasinoManagerId() {
			return casinoManagerId;
		}

	public void setId(Integer id) {
			this.id = id;
		}

	public void setCasinoManagerId(Integer casinoManagerId) {
			this.casinoManagerId = casinoManagerId;
		}
	public Date getCreatedOn() {
		return createdOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

}
