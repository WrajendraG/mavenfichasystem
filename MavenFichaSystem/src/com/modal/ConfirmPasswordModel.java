package com.modal;

public class ConfirmPasswordModel {
	private String userName;
	private String verificationCode;
	private String resetPassword;
	private String confirmResetPassword;
	private String salt;
	
	
	
	
	public String getSalt() {
		return salt;
	}
	public void setSalt(String salt) {
		this.salt = salt;
	}
	public String getUserName() {
		return userName;
	}
	public String getVerificationCode() {
		return verificationCode;
	}
	public String getResetPassword() {
		return resetPassword;
	}
	public String getConfirmResetPassword() {
		return confirmResetPassword;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public void setVerificationCode(String verificationCode) {
		this.verificationCode = verificationCode;
	}
	public void setResetPassword(String resetPassword) {
		this.resetPassword = resetPassword;
	}
	public void setConfirmResetPassword(String confirmResetPassword) {
		this.confirmResetPassword = confirmResetPassword;
	}


}
