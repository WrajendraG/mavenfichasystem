package com.modal;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "Casinomanager")
public class CasinoManager {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "casino_id")
	public Integer id;
	@Column(name = "casino_firstname")
	public String firstname;
	@Column(name = "casino_lastname")
	public String lastname;
	@Column(name = "casino_username")
	public String username;
	@Column(name = "casino_email")
	public String email;
	@Column(name = "casino_password")
	public String password;
	@Column(name = "casino_salt")
	public String salt;
	@Column(name = "casino_address")
	public String address;
	@Column(name = "casino_country")
	public String country;
	@Column(name = "casino_city")
	public String city;
	@Column(name = "casino_postcode")
	public String postcode;
	@Column(name = "created_on")
	public Date createdOn;
	@Column(name = "updated_on")
	public Date updatedOn;
	@Column(name="casino_verification")
	public String casinoVerification;
	@Transient
	public String conpassword;
	@Transient
	public Boolean isUpdate = false;

	// getters setter
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public String getConpassword() {
		return conpassword;
	}

	public void setConpassword(String conpassword) {
		this.conpassword = conpassword;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPostcode() {
		return postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	
	public Boolean isUpdate() {
		return isUpdate;
	}

	public void setIsUpdate(Boolean isUpdate) {
		this.isUpdate = isUpdate;
	}

	public String getCasinoVerification() {
		return casinoVerification;
	}

	public void setCasinoVerification(String casinoVerification) {
		this.casinoVerification = casinoVerification;
	}

}
