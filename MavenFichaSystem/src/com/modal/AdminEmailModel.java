package com.modal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="settings")
public class AdminEmailModel {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private Integer id;
	@Column(name="admin_Email")
	private String adminEmail;
	
	// getter and setter
	public Integer getId() {
		return id;
	}
	public String getAdminEmail() {
		return adminEmail;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setAdminEmail(String adminEmail) {
		this.adminEmail = adminEmail;
	}
	
	

}
