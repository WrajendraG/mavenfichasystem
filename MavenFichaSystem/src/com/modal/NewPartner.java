package com.modal;

public class NewPartner {
private String newPartner;
private String userName;
private String creditLimit;

//getter setter

public String getNewPartner() {
	return newPartner;
}
public String getUserName() {
	return userName;
}
public String getCreditLimit() {
	return creditLimit;
}
public void setNewPartner(String newPartner) {
	this.newPartner = newPartner;
}
public void setUserName(String userName) {
	this.userName = userName;
}
public void setCreditLimit(String creditLimit) {
	this.creditLimit = creditLimit;
}
}
