/*
 * Copyright @2015 Alteatec Pvt Ltd.
 */
package com.servlet;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Random;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Hex;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.util.Constants;

public class MigrationFileUploader {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new games file uploader.
	 *
	 * @see HttpServlet#HttpServlet()
	 */
	public MigrationFileUploader() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * Gets the string cell value.
	 *
	 * @param cell
	 *            the cell
	 * @return the string cell value
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */

	public String getNumberStringCellValue(Cell cell) {
		String returnValue = "";
		switch (cell.getCellType()) {
		case Cell.CELL_TYPE_NUMERIC:
			Double temp = cell.getNumericCellValue();
			Integer temp1 = temp.intValue();
			returnValue = temp1.toString();
			break;
		case Cell.CELL_TYPE_STRING:
			returnValue = cell.getStringCellValue();
			break;
		}
		return returnValue;
	}
	
	public String getDateCellValue(Cell cell) {
		String reportDate = "";
		if (HSSFDateUtil.isCellDateFormatted(cell)){
			Date cellValue = cell.getDateCellValue();
			DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
			reportDate = df.format(cellValue);
			//System.out.println(reportDate);
		}
		return reportDate;
	}
	public String getStringCellValue(Cell cell) {
		String returnValue = "";
		switch (cell.getCellType()) {
		case Cell.CELL_TYPE_BOOLEAN:
			Boolean tempBoolean = cell.getBooleanCellValue();
			returnValue = tempBoolean.toString();
			break;
		case Cell.CELL_TYPE_NUMERIC:
			Double temp = cell.getNumericCellValue();
			returnValue = temp.toString();
			break;
		case Cell.CELL_TYPE_STRING:
			returnValue = cell.getStringCellValue();
			break;
		}
		return returnValue;
	}
	
	/**
	 * Gets the boolean cell value.
	 *
	 * @param cell
	 *            the cell
	 * @return the boolean cell value
	 */
	public Boolean getBooleanCellValue(Cell cell) {
		Boolean returnValue = false;
		switch (cell.getCellType()) {
		case Cell.CELL_TYPE_BOOLEAN:
			returnValue = cell.getBooleanCellValue();
			break;
		case Cell.CELL_TYPE_STRING:
			String temp = cell.getStringCellValue();
			if (temp.equalsIgnoreCase("true")){
				returnValue = true;
			} else {
				returnValue = false;
			}
			break;
		}
		return returnValue;
	}

	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	protected void importFichas() throws IOException {
		
		String returnResult = "";

			/*SessionFactory sessionFactory = (SessionFactory) getServletContext()
					.getAttribute(Constants.SessionAttributes.DAO);*/
			String rowType = "";

			try {
				 if(Constants.BETMAIS_SKIN.equalsIgnoreCase("betmais")){
					 File newFile = new File("C:\\Fichas.xlsx");
						if (newFile.isFile()) {
							
							String fileName = newFile.getName();
							InputStream content =  new FileInputStream(newFile);

							HashMap<String, Object> rowMap = new HashMap<String, Object>();
							try {
								Workbook wb = WorkbookFactory.create(content);
								// Get first/desired sheet from the workbook
								Sheet workSheet = wb.getSheetAt(0);
								// Iterate through each rows from first sheet
								Iterator<Row> rowIterator = workSheet.iterator();
								
								Configuration config = new Configuration().configure();
								SessionFactory sessionFactory = config.buildSessionFactory();
								Session hibernateSession = sessionFactory.getCurrentSession();
								hibernateSession.beginTransaction();
								
								byte[] salt = new byte[64];
								Random random = new Random();
								while (rowIterator.hasNext()) {
									
									
									Row row = rowIterator.next();
									if (row.getRowNum() != 0) {
										// For each row, iterate through each
										// columns
										Iterator<Cell> cellIterator = row
												.cellIterator();
										rowMap = new HashMap<String, Object>();
										
										for(int cn=0; cn<row.getLastCellNum(); cn++) {
										//while (cellIterator.hasNext()) {
											Cell cell = row.getCell(cn, Row.CREATE_NULL_AS_BLANK);
											// for row of Game
											
											if (cell.getColumnIndex() == 0) {
												rowMap.put("ID",
														getNumberStringCellValue(cell));
												rowType = getStringCellValue(cell);
											} else if (cell.getColumnIndex() == 1) {
												rowMap.put("Name",
														getStringCellValue(cell));
											} else if (cell.getColumnIndex() == 2) {
												rowMap.put("Login",
														getStringCellValue(cell));
											} else if (cell.getColumnIndex() == 3) {
												rowMap.put("Country",
														getStringCellValue(cell));
											} else if (cell.getColumnIndex() == 4) {
												rowMap.put("Category",
														getStringCellValue(cell));
											} else if (cell.getColumnIndex() == 5) {
												//getDateCellValue(cell);
												rowMap.put("RegistrationDate",
														getDateCellValue(cell));
											} else if (cell.getColumnIndex() == 6) {
												rowMap.put("Currency",
														getStringCellValue(cell));
											} else if (cell.getColumnIndex() == 7) {
												rowMap.put("Balance",
														getStringCellValue(cell));
											} else if (cell.getColumnIndex() == 8) {
												rowMap.put("BonusAmount",
														getStringCellValue(cell));
											} else if (cell.getColumnIndex() == 9) {
												rowMap.put("WinLimit",
														getStringCellValue(cell));
											} else if (cell.getColumnIndex() == 10) {
												rowMap.put("ActualAmount",
														getStringCellValue(cell));
											} else if (cell.getColumnIndex() == 11) {
												rowMap.put("Profit",
														getStringCellValue(cell));
											} else if (cell.getColumnIndex() == 12) {
												rowMap.put("PartnerGroup",
														getStringCellValue(cell));
											} else if (cell.getColumnIndex() == 13) {
												rowMap.put("City",
														getStringCellValue(cell));
											} else if (cell.getColumnIndex() == 15) {
												rowMap.put("AffiliateID",
														getStringCellValue(cell));
											} else if (cell.getColumnIndex() == 16) {
												rowMap.put("Blocked",
														getStringCellValue(cell));
											} else if (cell.getColumnIndex() == 17) {
												rowMap.put("Email",
														getStringCellValue(cell));
											} else if (cell.getColumnIndex() == 18) {
												rowMap.put("UnplayedAmount",
												getStringCellValue(cell));
											} else if (cell.getColumnIndex() == 19) {
												rowMap.put("Phone",
														getNumberStringCellValue(cell));
											}else if (cell.getColumnIndex() == 20) {
												rowMap.put("BetShop",
														getStringCellValue(cell));
											} else if (cell.getColumnIndex() == 21) {
												rowMap.put("VerificationCode",
														getStringCellValue(cell));
											} else if (cell.getColumnIndex() == 22) {
												rowMap.put("Checked",
														getStringCellValue(cell));
											}else if (cell.getColumnIndex() == 23) {
												rowMap.put("PromoCode",
														getStringCellValue(cell));
												
											}else if (cell.getColumnIndex() == 24) {
												rowMap.put("Bank",
														getStringCellValue(cell));
												
											} else if (cell.getColumnIndex() == 25) {
												rowMap.put("AccHolder",
														getStringCellValue(cell));
												
											}else if (cell.getColumnIndex() == 26) {
												rowMap.put("AccNumber",
														getStringCellValue(cell));
												
											}else if (cell.getColumnIndex() == 27) {
												rowMap.put("HostName",
														getStringCellValue(cell));
												
											}else if (cell.getColumnIndex() == 28) {
												rowMap.put("Memo",
														getStringCellValue(cell));
												
											}else if (cell.getColumnIndex() == 29) {
												rowMap.put("MaxDailyDeposit",
														getStringCellValue(cell));
												
											}else if (cell.getColumnIndex() == 30) {
												rowMap.put("MaxWeekDeposit",
														getStringCellValue(cell));
												
											}else if (cell.getColumnIndex() == 31) {
												rowMap.put("MaxMonthDeposit",
														getStringCellValue(cell));
												
											}else if (cell.getColumnIndex() == 32) {
												rowMap.put("MaxDepositDate",
														getStringCellValue(cell));
												
											}else if (cell.getColumnIndex() == 33) {
												rowMap.put("BirthDate",
														getDateCellValue(cell));
												
											}else if (cell.getColumnIndex() == 34) {
												rowMap.put("BTag",
														getStringCellValue(cell));
												
											}else if (cell.getColumnIndex() == 35) {
												rowMap.put("ExludeDate",
														getDateCellValue(cell));
												
											}else if (cell.getColumnIndex() == 36) {
												rowMap.put("LastCountry",
														getStringCellValue(cell));
												
											}else if (cell.getColumnIndex() == 37) {
												rowMap.put("Bonus",
														getStringCellValue(cell));
												
											}else if (cell.getColumnIndex() == 38) {
												rowMap.put("LastLoginDate",
														getDateCellValue(cell));
												
											}else if (cell.getColumnIndex() == 39) {
												rowMap.put("DepositDate",
														getDateCellValue(cell));
												
											}else if (cell.getColumnIndex() == 40) {
												rowMap.put("FirstName",
														getStringCellValue(cell));
												
											}else if (cell.getColumnIndex() == 41) {
												rowMap.put("Points",
														getStringCellValue(cell));
												
											}else if (cell.getColumnIndex() == 42) {
												rowMap.put("SMS",
														getStringCellValue(cell));
												
											}else if (cell.getColumnIndex() == 43) {
												rowMap.put("EmailPromo",
														getStringCellValue(cell));
												
											}else if (cell.getColumnIndex() == 44) {
												rowMap.put("DocNumber",
														getStringCellValue(cell));
												
											}else if (cell.getColumnIndex() == 45) {
												rowMap.put("CameFrom",
														getStringCellValue(cell));
												
											} else if (cell.getColumnIndex() == 14) {
												rowMap.put("IP", getStringCellValue(cell));
											}else if (cell.getColumnIndex() == 46) {
												rowMap.put("Address", getStringCellValue(cell));
											}
										}
										
										// Send this map to DB
										try{
											random.nextBytes(salt);
											
											String password = createRandomPassword();
											System.out.println((String)rowMap.get("Login")+"="+password);
											password = createPasswordHash(password, salt);
										
											hibernateSession = sessionFactory.getCurrentSession();
											
											
											SQLQuery query = hibernateSession.createSQLQuery("INSERT INTO new_cms_schema.business (business_firstname, business_lastname, business_username, business_email, business_password, business_allowshared, business_status, business_phone, business_language, business_sscnum, business_address, business_country, business_town, business_postcode, created_on, updated_on, casino_manager_id, registration_source, bp_activation_code, bp_activated_status, business_salt, commission_percentage, is_agent) "
																					+ " VALUES(:firstName, :lastName, :userName, :email, :password, :active, :status, :phone, :language, :socialSecurityNumber, :address, :country, :city , :postCode, :createdOn, :updatedOn, :casinoManagerId, :registrationSource, :activationCode, :activationStatus, :salt, :commissionPercentage, :isAgent)");
											query.setParameter("firstName", rowMap.get("Name"));
											query.setParameter("lastName", "NA");
											query.setParameter("userName", rowMap.get("Login"));
											query.setParameter("email", rowMap.get("Email"));
											
											query.setParameter("password", password);
											query.setParameter("active", new Boolean(true));
											query.setParameter("status", 1);
											query.setParameter("phone", rowMap.get("Phone"));
											query.setParameter("language", "Portuguese");
											query.setParameter("socialSecurityNumber", "");
											query.setParameter("address", rowMap.get("Address"));
											query.setParameter("country", rowMap.get("Country"));
											query.setParameter("city", rowMap.get("City"));
											query.setParameter("postCode", "12345678");
											query.setParameter("createdOn", new Date());
											query.setParameter("updatedOn",new Date());
											query.setParameter("casinoManagerId", 1);
											query.setParameter("registrationSource", 1);
											query.setParameter("activationCode", "32d0157f-c58b-4f87-8ac8-880ef9e1ca86");
											query.setParameter("activationStatus", new Boolean(true));
											query.setParameter("salt", Hex.encodeHexString(salt));
											query.setParameter("commissionPercentage", 5);
											query.setParameter("isAgent", new Boolean(false));
											
											query.executeUpdate();
										} catch (Exception e){
											e.printStackTrace();
											System.out.println("-----Exception------");
										}
									}
								}
								
								hibernateSession.getTransaction().commit();
								//Close the current session factory and unbind it from the thread.
								sessionFactory.close();
							} catch (InvalidFormatException e) {
								e.printStackTrace();
								System.out.println("---EXCEPTION FOR ROW--");
								System.out.println(rowMap);
							}
							returnResult += "FILE " + fileName
									+ " IS SUCCESSFULLY UPLOADED!";
						}

					}
				 
			} catch (Exception e) {
				throw new IOException("Parsing file upload failed.", e);
			}

	}
	
	private String createRandomPassword() {
		int noOfCAPSAlpha = 1;
        int noOfDigits = 1;
        int noOfSplChars = 1;
        int minLen = 8;
        int maxLen = 8;

        char[] pswd = RandomPasswordGenerator.generatePswd(minLen, maxLen,
                noOfCAPSAlpha, noOfDigits, noOfSplChars);
        
        return new String(pswd);
	}
	
	private String createPasswordHash(String password, byte[] salt) {
		byte[] hash;
		try {
			MessageDigest digest = MessageDigest.getInstance("SHA-512");
			digest.update(salt);
			digest.update(password.getBytes());
			hash = digest.digest();
		} catch (NoSuchAlgorithmException e) {
			System.out.println(e);
			System.out.println("Error generating secure password");
			throw new UnsupportedOperationException(e);
		}
		return Hex.encodeHexString(hash);
	}
	
	public static void main(String[] args) {
		MigrationFileUploader newMigration = new MigrationFileUploader();
		try {
			newMigration.importFichas();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}