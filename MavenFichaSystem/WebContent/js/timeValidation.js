/*  ............................Validate Function.....................*/
	
	$.validator.addMethod("time24", function(value, element) {	
		/* return(/^([01]?[0-9]|2[0-3])(:[0-5][0-9]){2}$/.test(value)); //HH:MM:SS */			
		var parts = value.split(':');	 
		if(parts.length == 1){				
			if(parseInt(parts[0])>0 && parseInt(parts[0])<10){				
				value="0"+parseInt(value)+":00";	
			}else
				value=parseInt(value)+":00";			
		}else{
			var hh = parseInt(parts[0]);var mm=parseInt(parts[1]);
			if(hh>0 && hh<10){				
				hh="0"+parseInt(hh)+":";	
			}else
				hh=parseInt(hh)+":";
			
			if(mm>0 && mm<10 &&!isNaN(parseInt(mm))){				
				mm="0"+parseInt(mm);	
			}else{
				mm="00";
			}
			value=hh+mm;			
		}		
		return(/^([01]?[0-9]|2[0-3])(:[0-5][0-9])$/.test(value)); //HH:MM
   /*  if (!/^\d{2}:\d{2}:\d{2}$/.test(value)) return false;
    var parts = value.split(':');
    if (parts[0] > 23 || parts[1] > 59 || parts[2] > 59) return false;
    return true; */
	}, "Invalid time format.");	

	$.validator.addMethod("time12", function(value, element) {			
		var parts = value.split(':');	 
		if(parts.length == 1){				
			if(parseInt(parts[0])>0 && parseInt(parts[0])<10){				
				value="0"+parseInt(value)+":00";	
			}else
				value=parseInt(value)+":00";			
		}else{
			var hh = parseInt(parts[0]);var mm=parseInt(parts[1]);
			if(hh>0 && hh<10){				
				hh="0"+parseInt(hh)+":";	
			}else
				hh=parseInt(hh)+":";		
			if(mm>0 && mm<10 &&!isNaN(parseInt(mm))){				
				mm="0"+parseInt(mm);	
			}else{
				mm="00";
			}
			value=hh+mm;			
		}		
		$("#"+element.id).val(value); 
		return(/^([0][0-9]|1[0-2])(:[0-5][0-9])$/.test(value)); //HH:MM
	}, "Invalid time format.");