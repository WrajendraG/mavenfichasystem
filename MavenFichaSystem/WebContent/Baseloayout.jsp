<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Casino Manager</title>
<link type="text/css" href="<%=request.getContextPath()%>/css/jquery-ui.css" rel="stylesheet">
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/bootstrap.min.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/navbar-fixed-side.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/main.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/font-awesome.min.css">


<script type="text/javascript" language="javascript" src="<%=request.getContextPath()%>/js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-ui.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/checkAvailable.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.validate.min.js"></script>
<script type="text/javascript" language="javascript" src="<%=request.getContextPath()%>/js/bootstrap-hover-dropdown.js"></script>
<script type="text/javascript" language="javascript" src="<%=request.getContextPath()%>/js/jquery.pickmeup.js"></script>

</head>
<body>

	<%
	String indexPage = (String)session.getAttribute("indexPage");
	if(!"login".equals(indexPage)){
		
	
%>

<div class="container-fluid">
  <div class="row">
    <div class="col-sm-3 col-lg-2">
      <nav class="navbar navbar-default navbar-fixed-side">
        <div class="container">

              <div class="navbar-header">
                <button class="navbar-toggle" data-target=".navbar-collapse" data-toggle="collapse">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
                
              </div>
              
              <div class="collapse navbar-collapse">
              

       <ul class="nav navbar-nav">
       
       <li><a class="navbar-brand" style="width: 100%;" href="menudashboard.html" id="menudashboard"> <span class="fa-stack fa-lg pull-left"> <i class="fa fa-rocket fa-stack-1x "></i>
					</span><b> <spring:message code="label.casino.manager"/></b></a></li>
				
				<div class="clearfix"></div>
				<li><a href="menudashboard.html" id="menudashboard"> <span style="margin-left: 10px;" class="fa-stack fa-lg pull-left"> <i class="fa fa-dashboard fa-stack-1x "></i>
					</span><b> <spring:message code="label.dashboard"/></b></a></li>

				<li><a href="businesspartner.html" id="bussinesspartner"> <span style="margin-left: 10px;" class="fa-stack fa-lg pull-left"> <i class="fa fa-users fa-stack-1x "></i>
					</span> <b><spring:message code="label.business.partner"/></b></a></li>
				<li><a href="depositcasino.html" id="depositcasino"> <span style="margin-left: 10px;" class="fa-stack fa-lg pull-left"> <i class="fa fa-cc-visa fa-stack-1x "></i>
					</span><b><spring:message code="label.deposit.to.casino"/></b></a></li>
				<li><a href="casinomanager.html" id="casinomanager"> <span style="margin-left: 10px;" class="fa-stack fa-lg pull-left"> <i class="fa fa-user fa-stack-1x "></i>
					</span><b><spring:message code="label.casino.manager"/></b></a></li>
				<li><a href="payment.html" id="paymenthref"> <span style="margin-left: 10px;" class="fa-stack fa-lg pull-left"> <i class="fa fa-credit-card-alt fa-stack-1x "></i>
					</span><b><spring:message code="label.payment.type"/></b></a></li>
					
			   <li><a href="payback.html" id="paybackhref"> <span style="margin-left: 10px;" class="fa-stack fa-lg pull-left"> <i class="fa fa-credit-card-alt fa-stack-1x "></i>
					</span><b><spring:message code="label.pay.back"/></b></a></li>                   
					 
					
				<li><a href="currency.html" id="currencyhref"> <span style="margin-left: 10px;" class="fa-stack fa-lg pull-left"> <i class="fa fa-btc fa-stack-1x "></i>
					</span><b><spring:message code="label.currencies"/></b></a></li>
				
				<li class="dropdown">
                    <a href="#" class="" data-toggle="dropdown"> 
                        <span style="margin-left: 10px;" class="fa-stack fa-lg pull-left"><i class="fa fa-fw fa-plus"></i></span><b><spring:message code="label.players"/></b>
                    </a>
					<ul class="dropdown-menu">
						<li style="margin-left: 40px;"><a href="playersForBusinessPartner.html" id="playersForBusinessPartnerHref"><b><spring:message code="label.players.for.business.partner"/></b></a></li>
						<li style="margin-left: 40px;"><a href="playersForAgents.html" id="playersForAgentsHref"><b><spring:message code="label.players.for.agents"/></b></a></li>
					</ul>
					</li>
					</li>
				<li class="dropdown">
				
                    <a href="#" class="" data-toggle="dropdown"> 
                        <span style="margin-left: 10px;" class="fa-stack fa-lg pull-left"><i class="fa fa-fw fa-plus"></i></span><b><spring:message code="label.reports"/></b>
                    </a>
					<ul class="dropdown-menu">

						<li style="margin-left: 40px;"><a href="approvedAmounts.html" id="approvedAmountsHref"><b><spring:message code="label.approved.amounts"/></b></a></li>

						<li style="margin-left: 40px;"><a href="approvedCredits.html" id="approvedCreditsHref"><b><spring:message code="label.approved.credits"/></b></a></li>
					</ul>
					
					</li>
					
					<li><a href="adminEmail.html" id="adminEmail"> <span class="fa-stack fa-lg pull-left"> <i class="fa fa-email fa-stack-1x "></i>
					</span><b><spring:message code="label.admin.email"/></b></a></li>
					
					<!-- <li><a href="newPartner.html" id="newPartner"> <span class="fa-stack fa-lg pull-left"> <i class="fa fa-email fa-stack-1x "></i>
					</span><b> New Partner</b></a></li>
					
					<li><a href="depositWithdrawl.html" id="depositWithdrawl"> <span class="fa-stack fa-lg pull-left"> <i class="fa fa-email fa-stack-1x "></i>
					</span><b>Deposit Withdrawl</b></a></li>
					
                    <li><a href="creditStatus.html" id="creditStatus"> <span class="fa-stack fa-lg pull-left"> <i class="fa fa-email fa-stack-1x "></i>
					</span><b>Credit Status</b></a></li> -->
                    					
					
				<li><a href="logout.html" id="logout"> <span class="fa-stack fa-lg pull-left"> <i class="fa fa-sign-out fa-stack-1x "></i>
					</span><b><spring:message code="label.logout"/></b></a></li>
			</ul>


              </div>
        
        </div>
      </nav>
    </div>
    <div class="col-sm-9 col-lg-10">
     <div class="col-md-10">
		<tiles:insertAttribute name="body" />
	</div>
    </div>
  </div>
</div>

	<% } else {%>
	<div class="col-md-12">
		<tiles:insertAttribute name="body" />
	</div>
	<% 
}
	session.removeAttribute("indexPage");
%>



</body>
</html>