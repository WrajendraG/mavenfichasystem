<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script type="text/javascript">

function submitForm()
{
	//alert("insert button clicked");
   document.getElementById("adminEmailInsert").submit();
}

$("#document")
.ready(function() {
			
			
		});

</script>
</head>
<body>
<form:form action="insertAdminEmail.html" method="post" commandName="adminEmailCommandName" id="adminEmailInsert">

<div class="panel panel-default" div id="content">
			<div class="panel-heading">
				<center>
					<h4><spring:message code="label.admin.email"/></h4>
				</center>
			</div>

			
			<div class="panel-body">
			<div class="row">
					<div class="col-sm-2"></div>
					<div class="col-sm-2">
						<b><spring:message code="label.admin.email"/>:</b>
					</div>
					<div class="col-sm-4">
						<form:input type="text" path="adminEmail" id="adminEmail" class="form-control" />
					</div>
				</div>
				<div class="row">
					<div class="col-sm-4"></div>
					<div class="col-sm-4 errorMsg">
						<form:errors path="adminEmail" />
					</div>
				</div>
				<br>
			
			</div>
			<div class="btn-group btn-group-justified" id="buttonDiv"
				style="margin-bottom: 15px;">
				<center>
					<input name="logindeposit" type="button" value="<spring:message code="label.set.admin.email"/>"	class="btn btn-success" id="creat_bal" onclick="submitForm();"/> 
					<%-- <input name="updatedeposit" type="submit" value="<spring:message code="label.update"/>"	class="btn btn-warning" id="update_bal" />   --%>
				</center>
			</div>
			
			</div>


</form:form>
</body>
</html>