<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>	
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script type="text/javascript">
function submitForm()
{
  document.getElementById("updateCurrency").submit(); 
}
$("#document").ready(function() {
	
	var getVar= location.search.replace('?', '').split('=');	
	 var getvar2 = getVar.toString();
	 var queryString = new Array();
	 queryString = getvar2.split(",");
	 var currencyId = queryString[1];
		var id = currencyId;
		$.getJSON('currencyallvalues.html', {
			id : id
		}).done(function(data) {
			$("#id").val(data[0].currency_id);
			$("#name").val(data[0].currency_name);
		});

	
	 $("#back").click(function(){
		 alert("back button clicked");
		 window.location.href = document.getElementById("currencyhref").href;
	}); 
});
</script>
</head>
<body>
<form:form method="post" action="updateCurrency.html" commandName="curr" id="updateCurrency">
      		<div class="panel panel-default" div id="content">
			<div class="panel-heading">
				<center>
					<h4><spring:message code="label.currency.name"/></h4>
				</center>
			</div>
			<div class="panel-body">
				<div class="row">
					<br>

					<div class="col-sm-2"></div>
					<div class="col-sm-2"></div>
					<div class="col-sm-4">
						<font size=2 color="red">${message} </font>
					</div>
					<div class="col-sm-4"></div>
				</div>
				<br>


				<form:input type="hidden" path="id" id="id" class="form-control" />


				<div class="row">
					<div class="col-sm-2"></div>
					<div class="col-sm-2 required-field">
						<b><spring:message code="label.name"/>:</b>
					</div>
					<div class="col-sm-4">
						<form:input type="text" path="name" id="name" class="form-control" maxlength="3"/>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-4"></div>
					<div class="col-sm-4 errorMsg">
						<form:errors path="name"></form:errors>
					</div>
				</div>
				<br>


			</div>
			<div class="btn-group btn-group-justified"
				style="margin-bottom: 15px;">
				<center>
					<input name="updatePayment" type="button" value="<spring:message code="label.update"/>"	class="btn btn-warning" id="updatePayment" onclick="submitForm();"/> 
					<input name="back"	type="button" value="<spring:message code="label.back"/>" class="btn btn-primary" id="back" />
				</center>
			</div>
		</div> 
		
</form:form>
</body>
</html>