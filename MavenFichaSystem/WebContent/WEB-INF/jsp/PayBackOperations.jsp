<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>	
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="tag" uri="/WEB-INF/customTaglib.tld"%>
<%@ taglib prefix="tg" tagdir="/WEB-INF/tags" %>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script type="text/javascript">

$("#document").ready(function() {
	    var username = "${username}"
		$("#user").val(username);
		$("#username").append("" + username);
		
		$("#back").click(function() {	
			window.location.href = document.getElementById("paybackhref").href;
		}); 
		
		
});


</script>
</head>
<body>
<div id="col-md-12">
	
	
	<h3><spring:message code="label.pay.back"/></h3>
		
	<form:form action="payBackCrudOperations.html" method="post" commandName="payBackCrudOperation">	
		<div class="row">
		
		<div class="panel panel-default" id="content">
			<div class="panel-heading">
				<center><h4><spring:message code="label.pay.back"/></h4></center>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-sm-2"></div>
					<div class="col-sm-2"></div>
					<div class="col-sm-4">
						<div id="errorMessageBusinessPartner" style="font-size:12px; color:red" class="col-sm-12 text-center">${message}</div>
					</div>
					<div class="col-sm-4"></div>
				</div>	
				<div class="row">
					<div class="col-sm-2"></div>
					<div class="col-sm-2">
						<b>User:</b>
					</div>
					<div class="col-sm-4">
						<div id="username"></div>
					</div>
					<div class="col-sm-4"></div>
				</div>	
			</div>
				
				<br>


				<div class="row">
					<div class="col-sm-2"></div>
					<div class="col-sm-2">
						<b><spring:message code="label.business.partner"/>:</b>
					</div>
					<div class="col-sm-4">
						<form:select path="businessPartnerId" id="busniesspartner"
							class="form-control">
							<form:option value="" label="Select type"></form:option>
							<form:options items="${depositebusinesspartner}"></form:options>
						</form:select>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-4"></div>
					<div class="col-sm-4 errorMsg">
						<form:errors path="businessPartnerId"></form:errors>
					</div>
				</div>
				<br>


				<div class="row">
					<div class="col-sm-2"></div>
					<div class="col-sm-2">
						<b><spring:message code="label.currency"/>:</b>
					</div>
					<div class="col-sm-4">
						<form:select path="currencyId" id="currency" class="form-control">
							<form:option value="" label="Select type"></form:option>
							<form:options items="${depositcurrency}"></form:options>
						</form:select>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-4"></div>
					<div class="col-sm-4 errorMsg">
						<form:errors path="currencyId" />
					</div>
				</div>
				<br>


				<div class="row">
					<div class="col-sm-2"></div>
					<div class="col-sm-2">
						<b><spring:message code="label.payment.type"/>:</b>
					</div>
					<div class="col-sm-4">
						<form:select path="paymentId" id="payment" class="form-control">
							<form:option value="" label="Select type"></form:option>
							<form:options items="${depositpaymentvalues}"></form:options>
						</form:select>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-4"></div>
					<div class="col-sm-4 errorMsg">
						<form:errors path="paymentId" />
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-sm-2"></div>
					<div class="col-sm-2">
						<b><spring:message code="label.pay.back"/>:</b>
					</div>
					<div class="col-sm-4">
						<form:select path="payBack" id="payback" class="form-control">
							<form:option value="" label="Select type"></form:option>
							<form:option value="2" label="Credit Payback"></form:option>
							
						</form:select>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-4"></div>
					<div class="col-sm-4 errorMsg">
						<form:errors path="payBack" />
					</div>
				</div>
				<br>

				<div class="row">
					<div class="col-sm-2"></div>
					<div class="col-sm-2">
						<b><spring:message code="label.amount"/>:</b>
					</div>
					<div class="col-sm-4">
						<form:input type="text" path="amountStr" id="amt" class="form-control" maxlength="12" />
					</div>
				</div>
				<div class="row">
					<div class="col-sm-4"></div>
					<div class="col-sm-4 errorMsg">
						<form:errors path="amountStr" />
					</div>
				</div>
				<br>
</div>
</div>
		<center>
		<input name="insert" type="submit" value="<spring:message code="label.create"/>"	class="btn btn-success" id="insert" /> 
		<input type="button" value="<spring:message code="label.back"/>" class="btn btn-primary" id="back" />
		</center>
		
		
</form:form>
</body>
</html>