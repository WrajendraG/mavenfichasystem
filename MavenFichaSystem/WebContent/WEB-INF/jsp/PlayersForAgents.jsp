<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
      <%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="tag" uri="/WEB-INF/customTaglib.tld"%>
<%@ taglib prefix="tg" tagdir="/WEB-INF/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script type="text/javascript">
$("#document").ready(
		function() {
			
			$('#srch-term').bind('keyup', function(e) {

			    if ( e.keyCode === 13 ) { 
			    	var playerUserName=$("#srch-term").val();
					if(playerUserName!="")
					{
						window.location.href = document.getElementById("playersForAgentsHref").href+"?"+$.param({'username':playerUserName});
					}
				else
					{
						window.location.href = document.getElementById("playersForAgentsHref").href;
					}

			    }

			});
			
			
                     $("#search").click(function() {
	                 var playerUserName=$("#srch-term").val();
	
	                  if(playerUserName!="")
		              {
	            		window.location.href = document.getElementById("playersForAgentsHref").href+"?"+$.param({'username':playerUserName});
		              }
	                 else
		              {
			          window.location.href = document.getElementById("playersForAgentsHref").href;
		              }


});
		});

</script>
</head>
<body>
<h3><spring:message code="label.players.for.agents"/></h3>
			
		<div class="row">
			<div class="col-sm-3 col-md-3 pull-right" id="tablediv1">
				
					<div class="input-group">
						<input type="text" class="form-control" placeholder="<spring:message code="label.search.by.user.name"/>"
							name="srch-term" id="srch-term" maxlength="50">
						<div class="input-group-btn">
							<button style="height: 34px;" class="btn btn-info" type="button" id="search">
								<i class="glyphicon glyphicon-search"></i>
							</button>
						</div>
					</div>
			</div>
		</div>
		
		<c:url value="/playersForAgents.html?username=${searchKey}" var="pagedLink">
			<c:param name="action" value="list"/>
			<c:param name="p" value="~"/>
		</c:url>			
		
		<div id="tablediv" class="table-responsive" style="margin-top: 15px; width: 100%; height: 100%;">
			<table class= 'table table-hover table-striped table-bordered'  table id='tablec'>
			<tr class='success' align ='left'><th><spring:message code="label.agent"/></th><th><spring:message code="label.sub.agent"/></th><th><spring:message code="label.first.name"/></th><th><spring:message code="label.last.name"/></th><th><spring:message code="label.user.name"/></th><th><spring:message code="label.address"/></th><th><spring:message code="label.country"/></th><th><spring:message code="label.city"/></th><th><spring:message code="label.post.code"/></th></tr>
			<c:forEach items="${pagedListHolder.pageList}" var="item">
				<tr align='left' class='info'>
					<td>${item.mainagentusername}</td>
					<td>${item.subagentusername}</td>
					<td>${item.players_firstname}</td>
					<td>${item.players_lastname}</td>
					<td>${item.players_username}</td>
					<td>${item.players_address}</td>
					<td>${item.players_country}</td>
					<td>${item.city}</td>
					<td>${item.players_postcode}</td>
				</tr>
			</c:forEach>
			</table>		
		</div>
		<div id="page-nav"><tg:paging pagedListHolder="${pagedListHolder}" pagedLink="${pagedLink}"/></div>	
</body>
</html>