<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>	
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script type="text/javascript">

function submitForm()
{
  document.getElementById("insertBusinessPartner").submit(); 
}
$("#document").ready(function() {
	
	 $("#back").click(function(){
		 window.location.href = document.getElementById("bussinesspartner").href;
	}); 
});
</script>
</head>
<body>
<form:form action="insertBusinessPartner.html" method="post" commandName="business" id="insertBusinessPartner">
<div class="panel panel-default" id="content">	
			<div class="panel-heading">
				<center>
					<h4><spring:message code="label.new"/> <spring:message code="label.business.partner"/></h4>
				</center>
			</div>
			<div class="panel-body">

				<div class="col-sm-2"></div>
				<div class="col-sm-2"></div>
				<div class="col-sm-4">
					<div id="errorMessageBusinessPartner" style="font-size:12px; color:red" class="col-sm-12 text-center"></div>
				</div>
				<div class="col-sm-4"></div>
			</div>
			<br>

			<form:input type="hidden" path="id" id="id" />
			<form:input type="hidden" path="casinoManagerId" id="casinoManagerId" />
			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.first.name"/>:</b>
				</div>
				<div class="col-sm-4">
					<form:input type="text" path="firstname" id="firstname"
						class="form-control" maxlength="40" />
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4 errorMsg">
					<form:errors path="firstname"></form:errors>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.last.name"/>:</b>
				</div>
				<div class="col-sm-4">
					<form:input type="text" path="lastname" id="lastname"
						class="form-control" maxlength="40" />
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4 errorMsg">
					<form:errors path="lastname"></form:errors>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.user.name"/>:</b>
				</div>
				<div class="col-sm-4">
					<form:input type="text" path="username" id="username"
						class="form-control" maxlength="40" />
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4 errorMsg">
					<form:errors path="username"></form:errors>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.email"/>:</b>
				</div>
				<div class="col-sm-4">
					<form:input type="text" path="email" id="email"
						class="form-control" maxlength="40" />
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4 errorMsg">
					<form:errors path="email"></form:errors>
				</div>
			</div>
			<br>

			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.password"/>:</b>
				</div>
				<div class="col-sm-4">
					<form:input type="password" path="password" id="password"
						class="form-control" minlength="8" maxlength="16" />
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4 errorMsg">
					<form:errors path="password"></form:errors>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4">
					<spring:message code="label.password.policy"/>
				</div>
				<div class="col-sm-4"></div>
			</div>
			<br>
			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.password"/> <spring:message code="label.confirmation"/>:</b>
				</div>
				<div class="col-sm-4">
					<form:input type="password" path="conpassword" id="conpassword"
						class="form-control" minlength="8" maxlength="16" />
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4 errorMsg">
					<form:errors path="conpassword"></form:errors>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.active"/>:</b>
				</div>
				<div class="col-sm-4">
					<div class="checkbox" style="margin: 0px 0px 0px 20px;">
						<form:checkbox path="allowShared" name="allowShared"
							id="allowShared" />
					</div>
				</div>
				<div class="col-sm-4"></div>
			</div>
            <br>
			
			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.agent"/>:</b>
				</div>
				<div class="col-sm-4">
					<div class="checkbox" style="margin: 0px 0px 0px 20px;">
						<form:checkbox path="agent" name="agent"
							id="agent" />
					</div>
				</div>
			</div>
			<br>
			
				<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.affiliate.id"/>:</b>
				</div>
				<div class="col-sm-4">
					<form:input type="text" path="affiliateId" id="affiliateId"
						class="form-control" maxlength="40" />
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4 errorMsg">
					<form:errors path="affiliateId"></form:errors>
				</div>
			</div>
				
			<br>
			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.status"/>:</b>
				</div>
				<div class="col-sm-4">
					<form:select path="status" id="status" class="form-control">
						<form:option value="" label="Select type"></form:option>
						<form:options items="${array1}"></form:options>
					</form:select>
				</div>

			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4 errorMsg">
					<form:errors path="status"></form:errors>
				</div>
			</div>
			<br>

			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.phone.number"/>:</b>
				</div>
				<div class="col-sm-4">
					<form:input type="text" path="phone" id="phone"
						class="form-control" maxlength="12" minlength="6" />
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4 errorMsg">
					<form:errors path="phone"></form:errors>
				</div>
			</div>
			<br>


			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.language"/>:</b>
				</div>
				<div class="col-sm-4">
					<form:input type="text" path="language" id="language"
						class="form-control" maxlength="10" />
				</div>
				<div class="col-sm-4"></div>
			</div>
			<br>

			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.social.security.number"/>:</b>
				</div>
				<div class="col-sm-4">
					<form:input type="text" path="sscnum" id="sscnum"
						class="form-control" maxlength="40" />
				</div>
				<div class="col-sm-4"></div>
			</div>
			<br>


			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.address"/>:</b>
				</div>
				<div class="col-sm-4">
					<form:input type="text" path="address" id="address"
						class="form-control" maxlength="50" />
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4 errorMsg">
					<form:errors path="address"></form:errors>
				</div>
			</div>
			<br>


			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.country"/>:</b>
				</div>
				<div class="col-sm-4">
					<form:select path="country" id="country" class="form-control">
						<form:option value="" label="Select Country"></form:option>
						<form:options items="${countryList}"></form:options>
					</form:select>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4 errorMsg">
					<form:errors path="country"></form:errors>
				</div>
			</div>
			<br>

			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.city"/>:</b>
				</div>
				<div class="col-sm-4">
					<form:input type="text" path="town" id="town" class="form-control"
						maxlength="40" />
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4 errorMsg">
					<form:errors path="town"></form:errors>
				</div>
			</div>
			<br>

			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.post.code"/>:</b>
				</div>
				<div class="col-sm-4">
					<form:input type="text" path="postcode" id="postcode"
						class="form-control" maxlength="40" />
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4 errorMsg">
					<form:errors path="postcode"></form:errors>
				</div>
			</div>
			<br>


          <div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.commission.percentage"/>:</b>
				</div>
				<div class="col-sm-4">
					<form:input type="text" path="commissionPercentageStr" id="commissionPercentageStr"
						class="form-control" maxlength="6" />
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4 errorMsg">
					<form:errors path="commissionPercentageStr"></form:errors>
				</div>
			</div>
			<br>
		</div>
	<div class="btn-group btn-group-justified"
				style="margin-bottom: 15px;">
				<center>

					<input name="create_busniess" type="button" value="<spring:message code="label.create"/>"	class="btn btn-success" id="create_busniess" onclick="submitForm();"/> 
					<input name="back"	type="button" value="<spring:message code="label.back"/>" class="btn btn-primary" id="back" />
				</center>
				
				<%-- <input name="logindeposit" type="button" value="<spring:message code="label.create"/>"	class="btn btn-success" id="creat_bal" onclick="submitForm();"/> 
					<input name="updatedeposit" type="submit" value="<spring:message code="label.update"/>"	class="btn btn-warning" id="update_bal" />  
					<input name="back"	type="button" value="<spring:message code="label.back"/>" class="btn btn-primary" id="back" />
				</center> --%>
				
			</div>

</form:form>
</body>
</html>