<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>	

    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script type="text/javascript">
function submitForm()
{
  //alert("update button clicked");	
  document.getElementById("depositCasinoUpdateOperation").submit(); 
}

$("#document")
.ready(
		function() {
			
			 var getVar= location.search.replace('?', '').split('=');	
			 var getvar2 = getVar.toString();
			 var queryString = new Array();
			 queryString = getvar2.split(",");
			 var businessPartnerId = queryString[1];
			//alert(" id =  "+businessPartnerId);
			var username = "${username}"
				
				$("#user").val(username);
				$("#username").append("" + username);
				
				 $.getJSON('deposittocasinovaluesallvalues.html', {
						id : businessPartnerId
					}).done(function(data) {
						
                        $("#id").val(businessPartnerId);
						$("#busniesspartner").val(data[0].business_partner_id);
						$("#status").val(data[0].status);
						$("#currency").val(data[0].currency + '');
						$("#payment").val(data[0].payment + '');
						$("#amt").val(data[0].amount);
						$("#depositType").val(data[0].deposit_or_credit);
						

					 	if(data[0].status != 0){
							$("#busniesspartner").attr("disabled", true);
							$("#status").attr("disabled", true);
							$("#currency").attr("disabled", true);
							$("#payment").attr("disabled", true);
							$("#amt").attr("disabled", true);
							$("#depositType").attr("disabled", true);
							$("#update_bal").hide();
						}else{
							$("#busniesspartner").attr("disabled", false);
							$("#status").attr("disabled", false);
							$("#currency").attr("disabled", false);
							$("#payment").attr("disabled", false);
							$("#amt").attr("disabled", false);
							$("#depositType").attr("disabled", false);
						} 
				
					});
			
			$("#back").click(function() {	
				
				window.location.href = document.getElementById("depositcasino").href;
			});
		});
</script>
</head>
<body>
 <form:form method="post" action="updateDepositToCasino.html" commandName="d1" id="depositCasinoUpdateOperation">  

<div class="panel panel-default" div id="content">
			<div class="panel-heading">
				<center>
					<h4><spring:message code="label.deposit.to.casino"/></h4>
				</center>
			</div>

			
			<div class="panel-body">

				<form:input type="hidden" path="id" id="id" class="form-control" />


				<div class="row">
					<div class="col-sm-2"></div>
					<div class="col-sm-2">
						<b>User:</b>
					</div>
					<div class="col-sm-4">
						<div id="username"></div>
					</div>
					<div class="col-sm-4"></div>
				</div>
				
				<br>


				<div class="row">
					<div class="col-sm-2"></div>
					<div class="col-sm-2">
						<b><spring:message code="label.business.partner"/>:</b>
					</div>
					<div class="col-sm-4">
						<form:select path="businessPartnerId" id="busniesspartner"
							class="form-control">
							<form:option value="" label="Select type"></form:option>
							<form:options items="${depositebusinesspartner}"></form:options>
						</form:select>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-4"></div>
					<div class="col-sm-4 errorMsg">
						<form:errors path="businessPartnerId"></form:errors>
					</div>
				</div>
				<br>

				<div class="row">
					<div class="col-sm-2"></div>
					<div class="col-sm-2">
						<b><spring:message code="label.status"/>:</b>
					</div>
					<div class="col-sm-4">
						<form:select path="statusId" id="status" class="form-control">
							<form:option value="" label="Select type"></form:option>
							<form:options items="${deposittostatus}"></form:options>
						</form:select>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-4"></div>
					<div class="col-sm-4 errorMsg">
						<form:errors path="statusId" />
					</div>
				</div>
				<br>

				<div class="row">
					<div class="col-sm-2"></div>
					<div class="col-sm-2">
						<b><spring:message code="label.currency"/>:</b>
					</div>
					<div class="col-sm-4">
						<form:select path="currencyId" id="currency" class="form-control">
							<form:option value="" label="Select type"></form:option>
							<form:options items="${depositcurrency}"></form:options>
						</form:select>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-4"></div>
					<div class="col-sm-4 errorMsg">
						<form:errors path="currencyId" />
					</div>
				</div>
				<br>


				<div class="row">
					<div class="col-sm-2"></div>
					<div class="col-sm-2">
						<b><spring:message code="label.payment.type"/>:</b>
					</div>
					<div class="col-sm-4">
						<form:select path="paymentId" id="payment" class="form-control">
							<form:option value="" label="Select type"></form:option>
							<form:options items="${depositpaymentvalues}"></form:options>
						</form:select>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-4"></div>
					<div class="col-sm-4 errorMsg">
						<form:errors path="paymentId" />
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-sm-2"></div>
					<div class="col-sm-2">
						<b><spring:message code="label.deposit"/> <spring:message code="label.type"/>:</b>
					</div>
					<div class="col-sm-4">
						<form:select path="depositType" id="depositType" class="form-control">
							<form:option value="" label="Select type"></form:option>
							<form:option value="0" label="Deposit"></form:option>
							<form:option value="1" label="Credit"></form:option>
						</form:select>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-4"></div>
					<div class="col-sm-4 errorMsg">
						<form:errors path="depositType" />
					</div>
				</div>
				<br>

				<div class="row">
					<div class="col-sm-2"></div>
					<div class="col-sm-2">
						<b><spring:message code="label.amount"/>:</b>
					</div>
					<div class="col-sm-4">
						<form:input type="text" path="amountStr" id="amt" class="form-control" maxlength="12" />
					</div>
				</div>
				<div class="row">
					<div class="col-sm-4"></div>
					<div class="col-sm-4 errorMsg">
						<form:errors path="amountStr" />
					</div>
				</div>
				<br>

			</div>
           
			<div class="btn-group btn-group-justified" id="buttonDiv"
				style="margin-bottom: 15px;">
				<center>
					 <input name="updatedeposit" type="button" value="<spring:message code="label.update"/>"	class="btn btn-warning" id="update_bal" onclick="submitForm();"/>  
					<input name="back"	type="button" value="<spring:message code="label.back"/>" class="btn btn-primary" id="back" /> 
				</center>
			</div>
		</div>



</form:form>

</body>
</html>