<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>	
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="tag" uri="/WEB-INF/customTaglib.tld"%>
<%@ taglib prefix="tg" tagdir="/WEB-INF/tags" %>
<script type="text/javascript">

    var checkbo=[];
    var balcheckbox=[];
    var select = function($this,id){
     
    };

    $("#document").ready(function(){

    	$("#approveButton").click(function(){
    		
    		var count = 0;
            for(var i=0;i<balcheckbox.length;i++)
            {
              if($('#chkbalances'+balcheckbox[i]).prop('checked')) {
                if(count == 0){
                  arrValues=balcheckbox[i];
                }else{
                  arrValues=arrValues+","+balcheckbox[i];
                }

                count++;
              }
              else {

              }
            }
			if(count > 0){
				$("#approveModal").css({"display":"block"});
			}else{
				$("#approveModal").css({"display":"none"});
			}
    		
       });
    	
    	
    	$("#cancel").click(function(){

    		$("#approveModal").css({"display":"none"});
       });

      $("#updateBalances").click(function(){
    	  $("#approveModal").css({"display":"none"});  
        var arrValues="";
        var count = 0;
        for(var i=0;i<balcheckbox.length;i++)
        {
          if($('#chkbalances'+balcheckbox[i]).prop('checked')) {
            if(count == 0){
              arrValues=balcheckbox[i];
            }else{
              arrValues=arrValues+","+balcheckbox[i];
            }

            count++;
          }
          else {

          }
        }
        var approved=1;
        var depositType = $("#depositType").val();
        
		$.getJSON('dashboardBalancesUpdate.html',{balancesToBeApproved:arrValues,approved:approved, depositType:depositType}).done(function(data){
          
          //location.reload();
          window.location.href = document.getElementById("menudashboard").href;

        });
        
      });

      //

      $("#rejectBalances").click(function(){
        var arrValues="";
        var count = 0;
        for(var i=0;i<balcheckbox.length;i++)
        {
          if($('#chkbalances'+balcheckbox[i]).prop('checked')) {
            if(count == 0){
              arrValues=balcheckbox[i];
            }else{
              arrValues=arrValues+","+balcheckbox[i];
            }

            count++;
          }
          else {

          }
        }

        var approved=2;
        $.getJSON('dashboardBalancesUpdate.html',{balancesToBeApproved:arrValues,approved:approved}).done(function(data){
         
          //location.reload();
          window.location.href = document.getElementById("menudashboard").href;
        });
        
      });

      $("#updateBPartner").click(function(){
        var arrValues="";
        var count = 0;
        for(var i=0;i<checkbo.length;i++)
        {
          if($('#chk'+checkbo[i]).prop('checked')) {
            if(count == 0){
              arrValues=checkbo[i];
            }else{
              arrValues=arrValues+","+checkbo[i];
            }

            count++;
          }
          else {

          }
        }

       
        var approved=1;
        $.getJSON('dashboardValuesUpdate.html',{businessPartnerToBeApproved:arrValues,approved:approved}).done(function(data){
          
          //location.reload();
          window.location.href = document.getElementById("menudashboard").href;
        });
      });


      $("#rejectBPartner").click(function(){
        var arrValues="";
        var count = 0;
        for(var i=0;i<checkbo.length;i++)
        {
          if($('#chk'+checkbo[i]).prop('checked')) {
            if(count == 0){
              arrValues=checkbo[i];
            }else{
              arrValues=arrValues+","+checkbo[i];
            }
            count++;
          }
          else {

          }
        }

        var approved=2;
        $.getJSON('dashboardValuesUpdate.html',{businessPartnerToBeApproved:arrValues,approved:approved}).done(function(data){
          //location.reload();
          window.location.href = document.getElementById("menudashboard").href;
        });
      });


      $.getJSON('dashboard.html').done(function(data){
        if(parseInt(data.length)==parseInt(0))
        {
         
        }
        else
        {
          $('#businessPartner').html("");
          $('#businessPartner').append( "<table class= 'table table-hover table-striped table-bordered'  table id='tablec'>");
          var table = $('#businessPartner').children();
          table.append("<tr class='success' align ='center'><th>A/B</th><th><spring:message code="label.first.name"/></th><th><spring:message code="label.last.name"/></th><th><spring:message code="label.user.name"/></th><th><spring:message code="label.email"/></th><th><spring:message code="label.active"/></th><th><spring:message code="label.status"/></th><th><spring:message code="label.country"/></th><th><spring:message code="label.approve"/></th></tr>");
          for(var i = 0; i < data.length; i++)
          {
            checkbo.push(data[i].business_id);
            var row = $("<tr align='left' class='info'></tr>");

            	 var agentOrBP="";
				 if(data[i].is_agent == true){
					 agentOrBP = "A";
		          }else if(data[i].is_agent == false){
		        	  agentOrBP = "B";
		          }
            var agent_or_bp = $("<td>"+agentOrBP+"</td>");
            var business_firstname = $("<td>"+data[i].business_firstname+"</td>");
            var business_lastname = $("<td>"+data[i].business_lastname+"</td>");
            var business_username = $("<td>"+data[i].business_username+"</td>");
            var business_email = $("<td>"+data[i].business_email+"</td>");
            var business_allowshared = $("<td>"+data[i].business_allowshared+"</td>");
            //var business_status = $("<td>"+data[i].business_status+"</td>");
            var status="";
            if(data[i].business_status == 0){
              status = "Pending";
            }else if(data[i].business_status == 1){
              status = "Confirmed";
            }else if(data[i].business_status == 2){
              status = "Rejected";
            }
            var business_status = $("<td>"+status+"</td>");
            var business_country = $("<td>"+data[i].business_country+"</td>");
            var approvedCheck=$("<td><input type='checkbox' id=chk"+data[i].business_id+"><br></td>");
            table.append(row);
            
            row.append(agent_or_bp);
            row.append(business_firstname);
            row.append(business_lastname);
            row.append(business_username);
            row.append(business_email);
            row.append(business_allowshared);
            row.append(business_status);
            row.append(business_country);
            row.append(approvedCheck);
          }
        }
      });


      $.getJSON('dashboardBalancesValue.html').done(function(data){
        if(parseInt(data.length)==parseInt(0))
        {
          
        }
        else
        {
          $('#balances').html("");
          $('#balances').append( "<table class= 'table table-hover table-striped table-bordered'  table id='tablec'>");
          var table = $('#balances').children();
          table.append("<tr class='success' align ='center'><th>A/B</th><th><spring:message code="label.business.partner"/></th><th><spring:message code="label.amount"/></th><th><spring:message code="label.currency"/></th><th><spring:message code="label.casino.manager"/></th><th><spring:message code="label.status"/></th><th><spring:message code="label.approve"/></th></tr>");
          for(var i = 0; i < data.length; i++)
          {
            balcheckbox.push(data[i].balances_id);
            var row = $("<tr align='left' class='info'></tr>");
	       	var agentOrBP="";
			 if(data[i].is_agent == true){
				 agentOrBP = "A";
	          }else if(data[i].is_agent == false){
	        	  agentOrBP = "B";
	          }
   			 var agent_or_bp = $("<td>"+agentOrBP+"</td>");
            var businessPartner = $("<td>"+data[i].business_firstname+"</td>");
            var amount = $("<td>"+data[i].amount+"</td>");
            var currency = $("<td>"+data[i].currency_name+"</td>");
            var casinoManager = $("<td>"+data[i].casino_username+"</td>");
            var status="";
            if(data[i].status == 0){
              status = "Pending";
            }else if(data[i].status == 1){
              status = "Confirmed";
            }else if(data[i].status == 2){
              status = "Rejected";
            }
            var balStatus = $("<td>"+status+"</td>");
            var approvedCheck=$("<td><input type='checkbox' id=chkbalances"+data[i].balances_id+"><br></td>");
            table.append(row);
            
            row.append(agent_or_bp);
            row.append(businessPartner);
            row.append(amount);
            row.append(currency);
            row.append(casinoManager);
            row.append(balStatus);
            row.append(approvedCheck);
          }
        }

      });



    });
 </script>
        

<div class="col-md-12">
<form:form action="dashi.html" method="post" commandName="dash">
<div class="row">
	 	<span class="pull-left"><h3><spring:message code="label.heading.dashboard"/></h3></span>
	 	<span class="pull-right" style="margin-top: 12px;">  
	 	
			<div class="pull-left"><h4><spring:message code="label.language"/> : </h4></div>
			<div class="pull-right">
				<select onChange="window.document.location.href=this.options[this.selectedIndex].value;" name="languageList" class="form-control" style="margin-top: 2px; margin-left: 5px;">
				  	<option value="" >Select Language</option>
				  	<option value="menudashboard.html?lang=en" >English</option>
				  	<option value="menudashboard.html?lang=pt" >Portuguese</option>
				  	<option value="menudashboard.html?lang=de" >German</option>
			  		<option value="menudashboard.html?lang=tr" >Turkish</option>
			  		<option value="menudashboard.html?lang=nl" >Dutch</option>
				</select>
			</div>
	 	</span>
	
	</div>
  <div class="row">
    <div class="panel panel-default">
      <div class="panel-heading"><h4><spring:message code="label.pending"/> <spring:message code="label.business.partners"/></h4></div>
      <div class="panel-body">
        <div id="businessPartner" class="table-responsive" style=" margin-top: 15px; width:100%;height:100%;"></div>
      </div>
    </div>
  </div>
  <div class="row pull-right">
    <input name="update" type="button" value="<spring:message code="label.approve"/>" class="btn btn-success" id="updateBPartner"/>
    <input name="reject" type="button" value="<spring:message code="label.reject"/>" class="btn btn-danger" id="rejectBPartner"/>
  </div>
  <div class="clearfix"></div>
  <div class="row" style="margin-top: 20px;">
    <div class="panel panel-default">
      <div class="panel-heading"><h4><spring:message code="label.pending"/> <spring:message code="label.deposits"/></h4></div>
      <div class="panel-body">
        <div id="balances" class="table-responsive" style=" margin-top: 15px; width:100%;height:100%;"></div>
      </div>
    </div>
  </div>
  <div class="row pull-right">
    <input type="button" value="Approve" class="btn btn-success" id="approveButton"/>
    <input name="reject" type="button" value="Reject" class="btn btn-danger" id="rejectBalances"/>
         <div id="approveModal" class="modal" role="dialog">
	        <div class="modal-dialog modal-sm">
	            <div class="modal-content">
	                <div class="modal-header">
	                    <h4 class="modal-title"><spring:message code="label.confirmation"/></h4>
	                </div>
	                <div class="modal-body" style="height: 125px">
	                    <p><spring:message code="label.please.select.approval.type"/> : </p>
	                    <form>
		                    <div class="form-group">
				                <select class="form-control" id="depositType" style="width: 150px">
								  <option value="0">Deposit</option>
								  <option value="1">Credit</option>
								</select>
							</div>
						</form>
	                </div>
	                <div class="modal-footer">
	                    <button type="button" id="updateBalances" class="btn btn-success" data-dismiss="modal"><spring:message code="label.confirm"/></button>
	                    <button type="button" id="cancel" class="btn btn-primary" data-dismiss="modal"><spring:message code="label.cancel"/></button>
	                </div>
	            </div>
	        </div>
    	</div>
  </div>
</form:form>
</div>
