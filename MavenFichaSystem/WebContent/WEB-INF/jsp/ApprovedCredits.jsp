<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>	
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="tag" uri="/WEB-INF/customTaglib.tld"%>
<%@ taglib prefix="tg" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:useBean id="pagedListHolder" scope="request" type="org.springframework.beans.support.PagedListHolder"/>

<script type="text/javascript">
$("#document").ready(function(){

   
    $('#srch-term').bind('keyup', function(e) {

	    if ( e.keyCode === 13 ) {
	    	var userNameApprovedCredits=$("#srch-term").val();
			
			if(userNameApprovedCredits!="")
			{
				window.location.href = document.getElementById("approvedCreditsHref").href+"?"+$.param({'userNameApprovedCredits':userNameApprovedCredits});
			}
			else
			{
				window.location.href = document.getElementById("approvedCreditsHref").href;
			}
	    	
	    }
    });
    
    
	$("#search").click(function() {
		
		var userNameApprovedCredits=$("#srch-term").val();
		
		if(userNameApprovedCredits!="")
		{
			window.location.href = document.getElementById("approvedCreditsHref").href+"?"+$.param({'userNameApprovedCredits':userNameApprovedCredits});
		}
		
		else
		{
			window.location.href = document.getElementById("approvedCreditsHref").href;
		}
	});
    
    
    

  });
   
 </script>
        
<div id="col-md-12">



<div class="row"><h3><spring:message code="label.approved.credits"/></h3></div>

<div class="row">
			<div class="col-sm-3 col-md-3 pull-right" id="tablediv1">
					<div class="input-group">
						<input type="text" class="form-control" placeholder="<spring:message code="label.search.by.user.name"/>"
							name="srch-term" id="srch-term" maxlength="50">
						<div class="input-group-btn">
							<button style="height: 34px;" class="btn btn-info" type="button" id="search">
								<i class="glyphicon glyphicon-search"></i>
							</button>
						</div>
					</div>
			</div>
		</div>

 <br>
  <div class="row">
    <div class="panel panel-default">
      <div class="panel-heading"><h4><spring:message code="label.approved.amount.credits"/></h4></div>
      <div class="panel-body">
        <c:url value="/approvedCredits.html?userNameApprovedCredits=${searchKey}" var="pagedLink">
			<c:param name="action" value="list"/>
			<c:param name="p" value="~"/>
		</c:url>
        <div id="approvedCredits" class="table-responsive" style=" margin-top: 15px; width:100%;height:100%;">
			<table class= 'table table-hover table-striped table-bordered'  table id='tablec'>
				<tr class='success' align ='center'><th><spring:message code="label.business.partner"/> <spring:message code="label.name"/></th><th><spring:message code="label.user.name"/></th><th><spring:message code="label.currency"/></th><th><spring:message code="label.amount"/></th><th><spring:message code="label.approved.by"/></th></tr>
				<fmt:setLocale value="en_US" scope="session"/>
				<c:forEach items="${pagedListHolder.pageList}" var="item">
					<tr align='left' class='info'>
					<td>${item.buss_partner_name}</td>
					<td>${item.business_username}</td>
					<td>${item.currency_name}</td>
					<td><fmt:formatNumber type="number" maxFractionDigits="2" value="${item.amount}" /></td>
					<td>${item.casino_manager_name}</td>
					</tr>
				</c:forEach>
			</table>	        
        </div>
		<tg:paging pagedListHolder="${pagedListHolder}" pagedLink="${pagedLink}"/>
      </div>
    </div>
  </div>
</div> 

