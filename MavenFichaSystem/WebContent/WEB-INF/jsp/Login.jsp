<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<style type="text/css">
</style>

</head>
<body>

	<form:form action="validate.html" method="post" commandName="go">
	
		<div class="row">
		<div class="col-md-12">
				<h1 class="text-center login-title">Sign in to continue to
					CASINO MANGER</h1>
				<div class="account-wall">
					<img class="profile-img"
						src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=120"
						alt="">

					<div class="row" style="margin-top: 10px;">
						<div class="col-md-4"></div>
						<div class="col-md-4">
							<div class="row">
								<div class="col-md-2"></div>
								<div class="col-md-8">
								
								<form:input type="text" path="username"
									class="form-control text-center"
									placeholder="Username" id="username" />
							
								
								</div>
								<div class="col-md-2"></div>
							
							</div>	
						</div>
						<div class="col-md-4"></div>
					</div>
					<div class="row" style="margin-top: 10px;">
						<div class="col-md-4"></div>
						<div class="col-md-4">
						<div class="row">
								<div class="col-md-2"></div>
								<div class="col-md-8">
								
								<form:input type="password" path="password"
								class="form-control text-center"
								placeholder="Password" id="password"/>
							
								
								</div>
								<div class="col-md-2"></div>
							
							</div>
							
						</div>

						<div class="col-md-4"></div>
					</div>
					<center>
						<div class="errorMsg">

							<% String loginMessage = (String) session
											.getAttribute("loginMessage");
							
									if (loginMessage != null) {
										out.println(loginMessage);
									}
									session.removeAttribute("loginMessage"); %>

							<form:errors path="*" />
						</div>
					</center>
					<div class="row" style="margin-top: 10px;">
						<div class="col-md-4"></div>
						<div class="col-md-4">
						<div class="row">
								<div class="col-md-2"></div>
								<div class="col-md-8">
								
								<button name="loginbutton" type="submit" value="Login" id="ins"
								class="btn btn-lg btn-primary btn-block">Sign in</button>
							
								
								</div>
								<div class="col-md-2"></div>
							
							</div>
							
						</div>
						<div class="col-md-4"></div>
					</div>
					<div class="row" style="margin-top: 10px;">
						<div class="col-md-4"></div>
						<div class="col-md-4">
						<div class="row">
								<div class="col-md-2"></div>
								<div class="col-md-8">
								
								<button type="reset" value="clear" id="reset"
								class="btn btn-lg btn-danger btn-block">Reset</button>
							
								
								</div>
								<div class="col-md-2"></div>
							
							</div>
							
						</div>
					</div>

					<span class="clearfix"></span>

				</div>
			</div>
		</div>
	</form:form>
		<%-- <form:form action="forgotPasswordPage.html" method="post" commandName="forgotPasswordPage">
                        <div class="row" style="margin-top: 5px;">
						<div class="col-md-4"></div>
						<div class="col-md-4">
						<div class="row">
								<div class="col-md-2"></div>
								<div class="col-md-8">
								
								<button type="submit" value="Forgot Password" name="forgotPassword" id="forgotPassword"
								class="btn btn-lg btn-primary btn-block">Forgot Password</button>
							
								
								</div>
								<div class="col-md-2"></div>
							
							</div>
							
						</div>
						<div class="col-md-4"></div>
					</div>


		</form:form> --%>

</body>
</html>