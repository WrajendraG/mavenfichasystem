<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>	
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="tag" uri="/WEB-INF/customTaglib.tld"%>
<%@ taglib prefix="tg" tagdir="/WEB-INF/tags" %>
<jsp:useBean id="pagedListHolder" scope="request" type="org.springframework.beans.support.PagedListHolder"/>
<style type="text/css">
#content {
	display: none;
}

#creat_currency {
	display: none;
}

#delete_currency {
	display: none;
}

#update_currency {
	display: none;
}

#back {
	display: none;
}
</style>


<script type="text/javascript">
	function reset() {

		/*  $("#id").val(""); */
		$("#name").val("");
	}

	var currencyIdToBeDeleted ="";
	var selectdelete = function($this, id) {

		currencyIdToBeDeleted = id;
		$("#deleteModal").css({"display":"block"});

	};

	var select = function($this, currency_id) {

		window.location = 'currencyUpdate.html?id=' + currency_id;
	/* 	var id = currency_id;
		$.getJSON('currencyallvalues.html', {
			id : id
		}).done(function(data) {
			$("#id").val(data[0].currency_id);
			$("#name").val(data[0].currency_name);
		});

		$("#tablediv").hide();
		
		$("#tablediv1").hide();
		$("#content").show();
		$("#back").show();
		$("#update_currency").show();
		$("#delete_currency").show();
		$("#new").hide();
		$("#creat_currency").hide();
		$("#page-nav").hide();
 */
	};
	
	$("#document")
			.ready(
					function() {

						$("#confirm").click(function(){
				    		
							$("#deleteModal").css({"display":"none"});

							$.getJSON('deleteCurrencyId.html', {
								id : currencyIdToBeDeleted
							}).done(
									function(data) {
										

										window.location.href = document
												.getElementById("currencyhref").href;
							});
				    		
				       });

				    	$("#cancel").click(function(){
				    		currencyIdToBeDeleted = "";
				    		$("#deleteModal").css({"display":"none"});
				       });
				    	
				    	
						var errors = "${isValidationErrorPresentCurrency}";
						var errorsForUpdate= "${isValidationErrorPresentCurrencyUpdate}"; 
						
						if (errors == 'Y') {
							$("#new").hide();
							$("#tablediv").hide();
							$("#tablediv1").hide();
							$("#content").show();
							$("#creat_currency").show();
							$("#back").show();
							$("#update_currency").hide();
							$("#delete_currency").hide();
							$("#page-nav").hide();
							$("#back")
									.click(
											function() {
												window.location.href = document
														.getElementById("currencyhref").href;
											});

						} 
						
						
						else if(errorsForUpdate =='Y')
							{
							$("#new").hide();
							$("#tablediv").hide();
							$("#tablediv1").hide();
							$("#content").show();
							$("#creat_currency").hide();
							$("#back").show();
							$("#update_currency").show();
							$("#delete_currency").hide();
							$("#page-nav").hide();
							$("#back")
									.click(
											function() {
												window.location.href = document
														.getElementById("currencyhref").href;
											});

							}
						
						else {

							$("#new").click(function() {
								window.location = 'currencyInsert.html?';
								/* reset();
								$("#new").hide();
								$("#tablediv").hide();
								$("#tablediv1").hide();
								$("#content").show();
								$("#creat_currency").show();
								$("#back").show();
								$("#update_currency").hide();
								$("#delete_currency").hide();
								$("#page-nav").hide(); */
								 
							});

							$("#back").click(function() {

								$("#tablediv").show();
								$("#tablediv1").show();
								$("#content").hide();
								$("#creat_bal").hide();
								$("#back").hide();
								$("#update_bal").hide();
								$("#delete_bal").hide();
								$("#new").show();
								$("#page-nav").show();

							});

							
							//Search function for currency
							
							$('#srch-term').bind('keyup', function(e) {

							    if ( e.keyCode === 13 ) {
							    	var currencyName=$("#srch-term").val();
									
									if(currencyName!="")
										{
											window.location.href = window.location.href = document.getElementById("currencyhref").href +"?"+$.param({'name':currencyName})
										}
									else
										{
											window.location.href = document.getElementById("currencyhref").href;
										}

							    	
							    }
							});
							
							
							$("#search").click(function() {
								
								var currencyName=$("#srch-term").val();
								
								if(currencyName!="")
									{
										window.location.href = window.location.href = document.getElementById("currencyhref").href +"?"+$.param({'name':currencyName})
									}
								else
									{
										window.location.href = document.getElementById("currencyhref").href;
									}


							});
							
						}
					});
</script>
        
 <div id="col-md-12">
	
		
			<h3><spring:message code="label.currency"/></h3>
		
		
		<div class="row">
			<div class="col-sm-3 col-md-3 pull-right" id="tablediv1">
				
					<div class="input-group">
						<input type="text" class="form-control" placeholder="<spring:message code="label.search.by.currency.name"/>"
							name="srch-term" id="srch-term" maxlength="50">
						<div class="input-group-btn">
							<button style="height: 34px;" class="btn btn-info" type="button" id="search">
								<i class="glyphicon glyphicon-search"></i>
							</button>
						</div>
					</div>
				
			</div>

		</div>
		<form:form method="post" action="curropertion.html" commandName="curr">
		<c:url value="/currency.html?name=${searchKey}" var="pagedLink">
			<c:param name="action" value="list"/>
			<c:param name="p" value="~"/>
		</c:url>
		<div id="tablediv" class="table-responsive"	style="margin-top: 15px; width: 100%; height: 100%;">
			<table class= 'table table-hover table-striped table-bordered'  table id='tablec'>
			<tr class='success' align ='left'><th><spring:message code="label.currency.name"/></th><th><spring:message code="label.update"/></th><th><spring:message code="label.delete"/></th></tr>
			<c:forEach items="${pagedListHolder.pageList}" var="item">
				<tr align='left' class='info'>
					<td>${item.currency_name}</td>
					<td><button name='updateButton' value='' type='button' class='btn btn-primary' onclick='select(this,${item.currency_id})'><spring:message code="label.update"/></button></td>
																
					<td><button name='deleteButton' value='' type='button' class='btn btn-danger' data-toggle='modal' data-target=''#deleteModal' onclick='selectdelete(this,${item.currency_id})'><spring:message code="label.delete"/></button></td>																

				</tr>
			</c:forEach>
			</table>
		</div>
		<div id="page-nav"><tg:paging pagedListHolder="${pagedListHolder}" pagedLink="${pagedLink}"/></div>	
			
		<div id="deleteModal" class="modal" role="dialog">
	        <div class="modal-dialog modal-sm">
	            <div class="modal-content">
	                <div class="modal-header">
	                    <h4 class="modal-title"><spring:message code="label.confirmation"/></h4>
	                </div>
	                <div class="modal-body" style="height: 150px">
	                    <p><spring:message code="label.delete.confirmation"/></p>
	                    
	                </div>
	                <div class="modal-footer">
	                    <button type="button" id="confirm" class="btn btn-success" data-dismiss="modal" value=""><spring:message code="label.update"/></button>
	                    <button type="button" id="cancel" class="btn btn-primary" data-dismiss="modal" value=""><spring:message code="label.cancel"/></button>
	                </div>
	            </div>
	        </div>
    	</div>

<%--
		--%>
		<div class="btn-group btn-group-justified"
			style="margin-bottom: 15px;">
			<center>
				<input name="new" type="button" value="<spring:message code="label.new"/>" class="btn btn-success"
					id="new" />
			</center>
		</div>


	</form:form>
	</div>