<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>	
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script type="text/javascript">
function submitForm()
{
  document.getElementById("insertCasinoManager").submit(); 
}

$("#document").ready(function(){
	
	 $("#back").click(function(){
		 window.location.href = document.getElementById("casinomanager").href;
	}); 	
});
</script>
</head>
<body>
<form:form method="post" action="insertCasinoManager.html" commandName="ncasino" id="insertCasinoManager">
<div class="panel panel-default" id="content">
			<div class="panel-heading">
				<center><h4><spring:message code="label.casino.manager"/></h4></center>
			</div>
			<div class="panel-body">

				<div class="col-sm-2"></div>
				<div class="col-sm-2"></div>
				<div class="col-sm-4">
					<div id="errorMessageCasinoManager" style="font-size:12px; color:red" class="col-sm-12 text-center"></div>
				</div>
				<div class="col-sm-4"></div>
			</div>
			<br>

			<form:input type="hidden" path="id" id="id" class="form-control" />
			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.first.name"/>:</b>
				</div>
				<div class="col-sm-4">
					<form:input type="text" path="firstname" id="firstname"
						class="form-control" maxlength="40" />
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4 errorMsg">
					<form:errors path="firstname"></form:errors>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.last.name"/>:</b>
				</div>
				<div class="col-sm-4">
					<form:input type="text" path="lastname" id="lastname"
						class="form-control" maxlength="40" />
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4 errorMsg">
					<form:errors path="lastname"></form:errors>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.user.name"/>:</b>
				</div>
				<div class="col-sm-4">
					<form:input type="text" path="username" id="username"
						class="form-control" maxlength="40" />
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4 errorMsg">
					<form:errors path="username"></form:errors>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.email"/>:</b>
				</div>
				<div class="col-sm-4">
					<form:input type="text" path="email" id="email"
						class="form-control" maxlength="40" />
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4 errorMsg">
					<form:errors path="email"></form:errors>
				</div>
			</div>
			<br>

			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.password"/>:</b>
				</div>
				<div class="col-sm-4">
					<form:input type="password" path="password" id="password"
						class="form-control" minlength="8" maxlength="16" />
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4 errorMsg">
					<form:errors path="password"></form:errors>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4">
					<spring:message code="label.password.policy"/>
				</div>
				<div class="col-sm-4"></div>
			</div>

			<br>
			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.password"/> <spring:message code="label.confirmation"/>:</b>
				</div>
				<div class="col-sm-4">
					<form:input type="password" path="conpassword" id="conpassword"
						class="form-control" minlength="8" maxlength="16" />
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4 errorMsg">
					<form:errors path="conpassword"></form:errors>
				</div>
			</div>

			<br>
			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.address"/>:</b>
				</div>
				<div class="col-sm-4">
					<form:input type="text" path="address" id="address"
						class="form-control" maxlength="40" />
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4 errorMsg">
					<form:errors path="address"></form:errors>
				</div>
			</div>
			<br>


			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.country"/>:</b>
				</div>
				<div class="col-sm-4">
					<form:select path="country" id="country" class="form-control">
						<form:option value="" label="Select Country"></form:option>
						<form:options items="${countryList}"></form:options>
					</form:select>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4 errorMsg">
					<form:errors path="country"></form:errors>
				</div>
			</div>
			<br>

			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.city"/>:</b>
				</div>
				<div class="col-sm-4">
					<form:input type="text" path="city" id="city" class="form-control"
						maxlength="40" />
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4 errorMsg">
					<form:errors path="city"></form:errors>
				</div>
			</div>
			<br>

			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b><spring:message code="label.post.code"/>:</b>
				</div>
				<div class="col-sm-4">
					<form:input type="text" path="postcode" id="postcode"
						class="form-control" maxlength="40" />
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4 errorMsg">
					<form:errors path="postcode"></form:errors>
				</div>
			</div>
			<br>



			<div class="btn-group btn-group-justified" style="margin-bottom: 15px;">
				<center>
					<input name="logincasinomanager" type="button" value="<spring:message code="label.create"/>" class="btn btn-success" id="insert" onclick="submitForm();"/> 
					<%-- <input name="updatecasinomanager" type="submit" value="<spring:message code="label.update"/>" class="btn btn-warning" id="update" />  --%>
					<input type="button" value="<spring:message code="label.back"/>" class="btn btn-primary" id="back" />

				</center>
			</div>
		</div>


</form:form>
</body>
</html>