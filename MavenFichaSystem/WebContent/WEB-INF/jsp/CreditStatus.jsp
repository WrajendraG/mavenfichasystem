<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     	<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>	
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<form:form action="newPartner.html" method="post" commandName="newPartner">
<div class="panel panel-default" div id="content">
			<div class="panel-heading">
				<center>
					<h4>Account Information</h4>
				</center>
			</div>
			<div class="panel-body">
			</div>
			</div>
			
			<div class="btn-group btn-group-justified"
				style="margin-bottom: 15px;">
				<center>

					<button name="back" type="submit" class="btn btn-success" id="back"></button>
					<button name="print" type="submit" class="btn btn-warning" id="print" ></button>
					<button name="close" type="button" class="btn btn-primary" id="close" ></button>
				</center>
			</div>
</body>
</html>