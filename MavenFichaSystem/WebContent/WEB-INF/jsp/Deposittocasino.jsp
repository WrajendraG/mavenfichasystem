<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>	
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="tag" uri="/WEB-INF/customTaglib.tld"%>
<%@ taglib prefix="tg" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:useBean id="pagedListHolder" scope="request" type="org.springframework.beans.support.PagedListHolder"/>

<style type="text/css">
#creat_bal {
	display: none;
}

#content {
	display: none;
}

#delete_bal {
	display: none;
}

#update_bal {
	display: none;
}

#back {
	display: none;
}
</style>




<script type="text/javascript">
	function reset() {
		$("#busniesspartner").val("");
		$("#status").val("");
		$("#currency").val("");
		$("#payment").val("");
		$("#amt").val("");
		$("#depositType").val("");

	}
	var depositToCasinoIdToBeDeleted = "";
	var selectdelete = function($this, id, status) {
		
		if(status == 1){

			$("#deleteWarningModal").css({"display":"block"});
			return;
		}
		
		depositToCasinoIdToBeDeleted = id;
		$("#deleteModal").css({"display":"block"});
		
	};

	var select = function($this, businessPartnerId) {

		
		
		
		
		window.location = 'depositToCasinoUpdate.html?id=' + businessPartnerId;	
		/* $.getJSON('deposittocasinovaluesallvalues.html', {
			id : id
		}).done(function(data) {

			$("#busniesspartner").val(data[0].business_partner_id);
			$("#status").val(data[0].status);
			$("#currency").val(data[0].currency + '');
			$("#payment").val(data[0].payment + '');
			$("#amt").val(data[0].amount);
			$("#depositType").val(data[0].deposit_or_credit);
			

			if(data[0].status != 0){
				$("#busniesspartner").attr("disabled", true);
				$("#status").attr("disabled", true);
				$("#currency").attr("disabled", true);
				$("#payment").attr("disabled", true);
				$("#amt").attr("disabled", true);
				$("#depositType").attr("disabled", true);
			}else{
				$("#busniesspartner").attr("disabled", false);
				$("#status").attr("disabled", false);
				$("#currency").attr("disabled", false);
				$("#payment").attr("disabled", false);
				$("#amt").attr("disabled", false);
				$("#depositType").attr("disabled", false);
			}
			
			$("#tablediv").hide();
			
			$("#tablediv1").hide();
			$("#new").hide();
			$("#content").show();
			if(data[0].status != 0){
				$("#update_bal").hide();
				$("#delete_bal").hide();
			}else{
				$("#update_bal").show();
				$("#delete_bal").show();
			}
			
			
			$("#back").show();
			$("#page-nav").hide();

		});
 */

	};

	$("#document")
			.ready(
					function() {
						
						$("#confirm").click(function(){
				    		
							$("#deleteModal").css({"display":"none"});

							$.getJSON('deleteCasinoValuesId.html', {
								id : depositToCasinoIdToBeDeleted
							}).done(
									function(data) {
										

										window.location.href = document
												.getElementById("depositcasino").href;
							});
				    		
				       });

				    	$("#cancel").click(function(){
				    		depositToCasinoIdToBeDeleted = "";
				    		$("#deleteModal").css({"display":"none"});
				       });
				    	
				    	$("#deleteWarningConfirm").click(function(){
				    		depositToCasinoIdToBeDeleted = "";
				    		$("#deleteWarningModal").css({"display":"none"});
				       });

						var errors = "${isValidationErrorPresentDeposit}";
						var errorsForUpdate = "${isValidationErrorPresentDepositForUpdate}";
						if (errors == 'Y') {
							$("#tablediv").hide();
							$("#tablediv1").hide();
							$("#content").show();
							$("#creat_bal").show();
							$("#back").show();
							$("#update_bal").hide();
							$("#delete_bal").hide();
							$("#new").hide();
							$("#page-nav").hide();
							$("#back")
									.click(
											function() {
												window.location.href = document
														.getElementById("depositcasino").href;
											});
						} else if (errorsForUpdate == 'Y') {
							$("#tablediv").hide();
							$("#tablediv1").hide();
							$("#content").show();
							$("#creat_bal").hide();
							$("#back").show();
							$("#update_bal").show();
							$("#delete_bal").hide();
							$("#new").hide();
							$("#page-nav").hide();
							$("#back")
									.click(
											function() {
												window.location.href = document
														.getElementById("depositcasino").href;
											});
						} else {

							$("#new").click(function() {
								
								 window.location = 'depositToCasinoInsert.html?';	
								
								
								/* 
								Earlier code 
								
								reset();
								
								$("#busniesspartner").attr("disabled", false);
								$("#status").attr("disabled", false);
								$("#currency").attr("disabled", false);
								$("#payment").attr("disabled", false);
								$("#amt").attr("disabled", false);
								$("#depositType").attr("disabled", false);
								
								$("#tablediv").hide();
								$("#tablediv1").hide();
								$("#content").show();
								$("#creat_bal").show();
								$("#back").show();
								$("#update_bal").hide();
								$("#delete_bal").hide();
								$("#new").hide();
								$("#page-nav").hide(); */

							});

							$("#back").click(function() {

								$("#tablediv").show();
								$("#tablediv1").show();
								$("#content").hide();
								$("#creat_bal").hide();
								$("#back").hide();
								$("#update_bal").hide();
								$("#delete_bal").hide();
								$("#new").show();
								$("#page-nav").show();

							});

							var username = "${username}"
							
							$("#user").val(username);
							$("#username").append("" + username);

							$('#srch-term').bind('keyup', function(e) {

							    if ( e.keyCode === 13 ) {
							    	var businessUserName=$("#srch-term").val();
									
									
									if(businessUserName!="")
										{
											window.location.href = document.getElementById("depositcasino").href+"?"+$.param({'businessUserName':businessUserName});
										}
									else
										{
											window.location.href = document.getElementById("depositcasino").href;
										}

							    	
							    }
							});
							
							
							$("#search").click(function() {
								var businessUserName=$("#srch-term").val();
								
								
								if(businessUserName!="")
									{
										window.location.href = document.getElementById("depositcasino").href+"?"+$.param({'businessUserName':businessUserName});
									}
								else
									{
										window.location.href = document.getElementById("depositcasino").href;
									}


							});
						}
					});
</script>    
 <div id="col-md-12">
	
	
			<h3><spring:message code="label.heading.deposit.to.casino"/></h3>
		
		
		<div class="row">
			<div class="col-sm-3 col-md-3 pull-right" id="tablediv1">
				
					<div class="input-group">
						<input type="text" class="form-control" placeholder="<spring:message code="label.search.by.user.name"/>"
							name="srch-term" id="srch-term" maxlength="50">
						<div class="input-group-btn">
							<button style="height: 34px;" class="btn btn-info" type="button" id="search">
								<i class="glyphicon glyphicon-search"></i>
							</button>
						</div>
					</div>
				
			</div>

		</div>
		
	<%-- 	<form:form method="post" action="deposittocasinooper.html" commandName="d1"> --%>
		<c:url value="/depositcasino.html?businessUserName=${searchKey}" var="pagedLink">
			<c:param name="action" value="list"/>
			<c:param name="p" value="~"/>
		</c:url>
		<div id="tablediv" class="table-responsive" style="margin-top: 15px; width: 100%; height: 100%;">
			<table class= 'table table-hover table-striped table-bordered'  table id='tablec'>
				<tr class='success' align ='left'><th><spring:message code="label.casino.manager"/></th><th><spring:message code="label.status"/></th><th><spring:message code="label.user.name"/></th><th><spring:message code="label.business.partner"/></th><th><spring:message code="label.payment"/></th><th><spring:message code="label.currency"/></th><th><spring:message code="label.amount"/></th><th><spring:message code="label.date.time"/></th><th><spring:message code="label.update"/></th><th><spring:message code="label.delete"/></th></tr>
				<fmt:setLocale value="en_US" scope="session"/>
				<c:forEach items="${pagedListHolder.pageList}" var="item">
					<tr align='left' class='info'>
					<td>${item.casino_username}</td>
					<td>
						<c:if test="${item.status == 0}">Pending</c:if>
						<c:if test="${item.status == 1}">Confirmed</c:if>
						<c:if test="${item.status == 2}">Rejected</c:if>
					</td>
					<td>${item.business_username}</td>
					<td>${item.business_firstname}</td>
					<td>${item.payment_name}</td>
					<td>${item.currency_name}</td>
					<td><fmt:formatNumber type="number" maxFractionDigits="2" value="${item.amount}" /></td>
					<td>${item.created_on}</td>
				
					<td><button name='updateButton' value='Modify' type='button' class='btn btn-primary' onclick='select(this,${item.balances_id})'><spring:message code="label.modify"/></button></td>
					<td><button name='deleteButton' value='Delete' type='button' class='btn btn-danger' data-toggle='modal' data-target=''#deleteModal'  onclick='selectdelete(this,${item.balances_id})'><spring:message code="label.delete"/></button></td>
					</tr>
				</c:forEach>
			</table>
		
		</div>
		<div id="page-nav"><tg:paging pagedListHolder="${pagedListHolder}" pagedLink="${pagedLink}"/></div>
		
		<div id="deleteModal" class="modal" role="dialog">
	        <div class="modal-dialog modal-sm">
	            <div class="modal-content">
	                <div class="modal-header">
	                    <h4 class="modal-title">Confirmation</h4>
	                </div>
	                <div class="modal-body" style="height: 150px">
	                    <p><spring:message code="label.delete.confirmation"/></p>
	                    
	                </div>
	                <div class="modal-footer">
	                    <button type="button" id="confirm" class="btn btn-success" data-dismiss="modal"><spring:message code="label.confirm"/></button>
	                    <button type="button" id="cancel" class="btn btn-primary" data-dismiss="modal"><spring:message code="label.cancel"/></button>
	                </div>
	            </div>
	        </div>
    	</div>
    	
    	<div id="deleteWarningModal" class="modal" role="dialog">
	        <div class="modal-dialog modal-sm">
	            <div class="modal-content">
	                <div class="modal-header">
	                    <h4 class="modal-title">Warning</h4>
	                </div>
	                <div class="modal-body" style="height: 150px">
	                    <p><spring:message code="label.delete.warning"/></p>
	                    
	                </div>
	                <div class="modal-footer">
	                    <button type="button" id="deleteWarningConfirm" class="btn btn-success" data-dismiss="modal"><spring:message code="label.ok"/></button>
	                </div>
	            </div>
	        </div>
    	</div>
		<!-- from here -->	

		
<!-- till here cut  -->
		<div class="btn-group btn-group-justified" id="buttonDiv"
			style="margin-bottom: 15px;">
			<center>
				<input name="new" type="button" value="<spring:message code="label.new"/>" class="btn btn-success"	id="new" />
			</center>
		</div>



	<%-- </form:form> --%>
	</div>