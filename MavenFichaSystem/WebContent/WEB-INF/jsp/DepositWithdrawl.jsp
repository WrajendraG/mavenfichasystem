<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     	<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>	
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<form:form action="depositWithdrawl.html" method="post" commandName="depositWithdrawl">

<div class="panel panel-default" div id="content">
			<div class="panel-heading">
				<center>
					<h4>Deposit Withdrawl</h4>
				</center>
			</div>
			<div class="panel-body">


<div class="row">
					<div class="row">
					<div class="col-sm-2"></div>
					<div class="col-sm-2 required-field">
						<b>User Name:</b>
					</div>
					<div class="col-sm-4">
						<form:input type="text" path="userName" id="userName" class="form-control" />
					</div>
				</div>
				
				<div class="row">
					<div class="col-sm-4"></div>
					<div class="col-sm-4 errorMsg">
						<form:errors path="userName"></form:errors>
					</div>
				</div>
				<br>
				
				<div class="col-sm-2"></div>
					<div class="col-sm-2 required-field">
						<b>Amount:</b>
					</div>
					<div class="col-sm-4">
						<form:input type="text" path="amount" id="amount" class="form-control"/>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-4"></div>
					<div class="col-sm-4 errorMsg">
						<form:errors path="amount"></form:errors>
					</div>
				</div>
				</div>
				</div>
				
</form:form>
</body>
</html>