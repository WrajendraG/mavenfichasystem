<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="tag" uri="/WEB-INF/customTaglib.tld"%>
<%@ taglib prefix="tg" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:useBean id="pagedListHolder" scope="request" type="org.springframework.beans.support.PagedListHolder"/>



<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script type="text/javascript">
$("#document").ready(function() {
			$("#new").click(function() {
				window.location ='payBackOperations.html?'
			});
			
			$('#srch-term').bind('keyup', function(e) {

			    if ( e.keyCode === 13 ) { 
			    	var userName=$("#srch-term").val();
					if(userName!="")
					{
						window.location.href = window.location.pathname+"?"+$.param({'username':userName});
					}
				else
					{
						window.location.href = document.getElementById("payBackDisplay").href;
					}

			    }

			});
			
			
                     $("#search").click(function() {
	                 var userName=$("#srch-term").val();
	
	                  if(userName!="")
		              {
	            		window.location.href = document.getElementById("payBackDisplay").href+"?"+$.param({'username':userName});
		              }
	                 else
		              {
			          window.location.href = document.getElementById("payBackDisplay").href;
		              }
                    
                     });
			
			
		});
</script>
</head>
<body>
<h3><spring:message code="label.pay.back"/></h3>
	<div class="row">
			<div class="col-sm-3 col-md-3 pull-right" id="tablediv1">
				
					<div class="input-group">
						<input type="text" class="form-control" placeholder="<spring:message code="label.search.by.user.name"/>"
							name="srch-term" id="srch-term" maxlength="50">
						<div class="input-group-btn">
							<button style="height: 34px;" class="btn btn-info" type="button" id="search">
								<i class="glyphicon glyphicon-search"></i>
							</button>
						</div>
					</div>
			</div>
		</div>
<form:form method="post" action="payBackDisplay.html" commandName="payBackDisplay">
		<c:url value="/payback.html?name=${searchKey}" var="pagedLink">
			<c:param name="action" value="list"/>
			<c:param name="p" value="~"/>
		</c:url>
<div id="tablediv" class="table-responsive" style="margin-top: 15px; width: 100%; height: 100%;">
			<table class= 'table table-hover table-striped table-bordered'  table id='tablec'>
				<tr class='success' align ='left'><th><spring:message code="label.casino.manager"/></th><th><spring:message code="label.business.partner"/></th><th><spring:message code="label.payment"/></th><th><spring:message code="label.currency.name"/></th><th><spring:message code="label.amount"/></th><th>Created On</th></tr>
				<fmt:setLocale value="en_US" scope="session"/>
				<c:forEach items="${pagedListHolder.pageList}" var="item">
					<tr align='left' class='info'>
					<td>${item.casino_username}</td>
					<td>${item.business_firstname}</td>
					<td>${item.payment_name}</td>
					<td>${item.currency_name}</td>
					<td><fmt:formatNumber type="number" maxFractionDigits="2" value="${item.amount}" /></td>
					<td>${item.created_on}</td>
					</tr>
				</c:forEach>
			</table>
		</div>
<div id="page-nav"><tg:paging pagedListHolder="${pagedListHolder}" pagedLink="${pagedLink}"/></div>
<center>
<input name="new" type="button" value="<spring:message code="label.new"/>" class="btn btn-success"	id="new" />
</center>
</form:form>
</body>
</html>