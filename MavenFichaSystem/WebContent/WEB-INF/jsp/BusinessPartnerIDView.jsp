<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ taglib prefix="tag" uri="/WEB-INF/customTaglib.tld"%>
<%@ taglib prefix="tg" tagdir="/WEB-INF/tags" %>
<jsp:useBean id="pagedListHolder" scope="request" type="org.springframework.beans.support.PagedListHolder"/>

<script type="text/javascript">

$("#document").ready(function(){
	
	 var getVar= location.search.replace('?', '').split('=');
		
	 var getvar2 = getVar.toString();
	 var queryString = new Array();
	 queryString = getvar2.split(",");
	 var businessPartnerId = queryString[1];
	
    $.getJSON('businessPartnerIdPersonalInformation.html',{businessPartnerId:businessPartnerId}).done(function(data){
    	$("#businessPartnerPersonalInfo").append("<b><spring:message code="label.first.name"/> : "+data[0].business_firstname +"</b><br><b><spring:message code="label.last.name"/> : "+data[0].business_lastname+"</b><br><b><spring:message code="label.user.name"/> : "+data[0].business_username+"</b><br><b><spring:message code="label.email"/> : "+data[0].business_email+"</b></center></div>"); 
	});
 
	
    $("#back").click(function() {
    	window.location.href = document.getElementById("bussinesspartner").href;
    });
    
	
	//
	
	
	
});
</script>

<h3><spring:message code="label.business.partner.details"/></h3>
	<div id="businessPartnerCurrencyInfo" class="table-responsive" style=" margin-top: 15px; width:100%; height:100%;">
	     	<c:url value="/BusinessPartnerIDView.html" var="pagedLink">
				<c:param name="action" value="list"/>
				<c:param name="p" value="~"/>
			</c:url>
			
	     	<table class= 'table table-hover table-striped table-bordered'  table id='tablec'>
				<tr class='success' align ='left'><th><spring:message code="label.currency"/></th><th><spring:message code="label.commission"/></th><th><spring:message code="label.total.balance"/></th><th><spring:message code="label.credit.balance"/></th><th><spring:message code="label.cash.balance"/></th></tr>
				
				<c:forEach items="${pagedListHolder.pageList}" var="item">
					<tr align='left' class='info'>
					<fmt:setLocale value="en_US" scope="session"/>
						<td>${item.currency_name}</td>
						<td><fmt:formatNumber type="number" maxFractionDigits="2" value="${item.total_commission_amount}" /></td>
						<td><fmt:formatNumber type="number" maxFractionDigits="2" value="${item.total_amount}" /></td>
						<td><fmt:formatNumber type="number" maxFractionDigits="2" value="${item.total_credit_amount}" /></td>
						<td><fmt:formatNumber type="number" maxFractionDigits="2" value="${item.total_balance}" /></td>
					</tr>
				</c:forEach>
			</table>
	</div>
     <div id="page-nav"><tg:paging pagedListHolder="${pagedListHolder}" pagedLink="${pagedLink}"/></div>
<br><br>
<div id="businessPartnerPersonalInfo" class="table-responsive" style=" margin-top: 15px; width:100%; height:100%;"></div>
<br><br>
<center><input name="back" type="button" value="<spring:message code="label.back"/>" class="btn btn-primary" id="back" /></center>
