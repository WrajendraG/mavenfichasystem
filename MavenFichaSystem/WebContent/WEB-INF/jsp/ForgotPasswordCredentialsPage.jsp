<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<form:form action="casinoManagerCredential.html" method="post" commandName="casinoManagerCredential">
<div class="panel panel-default" id="content">
			<div class="panel-heading">
				<center><h4></h4></center>
			</div>
			<div id="errorMessageResetPassword" style="font-size:12px; color:red" class="col-sm-12 text-center">${message}</div>
			<div class="panel-body">
			
			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-2">
					<b>User Name:</b>
				</div>
				<div class="col-sm-4">
					<form:input type="text" path="userName" id="userName"
						class="form-control" maxlength="40" />
				</div>
				<div class="col-sm-4">
				<font size=2 color="red"><form:errors path="userName"></form:errors></font>
				</div>
			</div>
			<br>
			<center>
			<input name="forgotPassword" type="submit" value="Ok" class="btn btn-primary" id="ok" /> 
					<input type="button" value="Back" class="btn btn-primary" id="back" />
			</center>
			</div>
			</div>
			</form:form>
</body>
</html>