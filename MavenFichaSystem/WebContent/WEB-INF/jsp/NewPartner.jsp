<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    	<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>	
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<form:form action="newPartner.html" method="post" commandName="newPartner">
<div class="panel panel-default" div id="content">
			<div class="panel-heading">
				<center>
					<h4><%-- <spring:message code="label.currency.name"/> --%>New Partner</h4>
				</center>
			</div>
			<div class="panel-body">
				<div class="row">
					<br>

					<div class="col-sm-2"></div>
					<div class="col-sm-2"></div>
					<div class="col-sm-4">
						<font size=2 color="red">${message} </font>
					</div>
					<div class="col-sm-4"></div>
				</div>
				<br>

				<%-- <form:input type="hidden" path="id" id="id" class="form-control" /> --%>


				<div class="row">
					<div class="col-sm-2"></div>
					<div class="col-sm-2 required-field">
						<b>New Partner:</b>
					</div>
					<div class="col-sm-4">
						<form:input type="text" path="newPartner" id="newPartner" class="form-control" maxlength="3"/>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-4"></div>
					<div class="col-sm-4 errorMsg">
						<form:errors path="newPartner"></form:errors>
					</div>
				</div>
				<br>
					<div class="row">
					<div class="col-sm-2"></div>
					<div class="col-sm-2 required-field">
						<b>User Name:</b>
					</div>
					<div class="col-sm-4">
						<form:input type="text" path="userName" id="userName" class="form-control" />
					</div>
				</div>
				<div class="row">
					<div class="col-sm-4"></div>
					<div class="col-sm-4 errorMsg">
						<form:errors path="userName"></form:errors>
					</div>
				</div>
				<br>
				
				<div class="row">
					<div class="col-sm-2"></div>
					<div class="col-sm-2 required-field">
						<b>Credit Limit:</b>
					</div>
					<div class="col-sm-4">
						<form:input type="text" path="creditLimit" id="creditLimit" class="form-control" />
					</div>
				</div>
				<div class="row">
					<div class="col-sm-4"></div>
					<div class="col-sm-4 errorMsg">
						<form:errors path="creditLimit"></form:errors>
					</div>
				</div>
				<br>


			</div>
			<div class="btn-group btn-group-justified"
				style="margin-bottom: 15px;">
				<center>	
					<input name="back"	type="button" value="Submit" class="btn btn-primary" id="back" />
				</center>
			</div>
		</div> 
		

</form:form>
</body>
</html>